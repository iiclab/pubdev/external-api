#include <iostream>
#include <fstream>
#include <sstream>
#include <unordered_map>
#include <memory>

#include <Windows.h>

#include <Scenario/ExternalApi/ExternalApiLoadManager.h>
#include <Scenario/ExternalApi/Structure.h>
#include <Scenario/ExternalApi/Array.h>

using namespace scenario;

std::vector<std::string> Split( const std::string& input, const std::string& splitIndicator );
std::string Trim( const std::string& input );
Value Parse( const std::string& input );
std::vector<Value> Parse( const std::vector<std::string>& input );

int main()
{
	std::ifstream scriptStream("script.txt");

	ExternalApiLoadManager& externalApiLoadManager = ExternalApiLoadManager::GetInstance();

	//External Api Loader 로드 하기
	const std::string assignIndicator("<=");
	for (std::string line; std::getline(scriptStream, line) && !line.empty(); )
	{
		auto splited = Split(line, assignIndicator);
		if(splited.size() != 2)
		{
			std::cout << "[" << line << "]" << std::endl;
			std::cout << "External Api Loader 입력이 잘못됨" << std::endl;
			continue;
		}

		if(!externalApiLoadManager.Register(Trim(splited[1]), Trim(splited[0])))
		{
			std::cout << "[" << line << "]" << std::endl;
			std::cout << "External Api Loader 등록 실패" << std::endl;
			continue;
		}
	}
	
	//External Api 로드 하기
	std::unordered_map<std::string, std::shared_ptr<ExternalApi>> externalApiMap;
	for (std::string line; std::getline(scriptStream, line) && !line.empty(); )
	{
		std::string name;
		std::string uri;

		std::string::size_type position = line.find(assignIndicator);
		if(position == std::string::npos)
		{
			std::cout << "[" << line << "]" << std::endl;
			std::cout << "External Api의 변수 명이 빠짐" << std::endl;
			continue;
		}

		name = Trim(line.substr(0, position));
		uri = Trim(line.substr(position + assignIndicator.size())); 

		auto externalApi = externalApiLoadManager.Load(Url(uri));
		if(externalApi.get() == nullptr)
		{
			std::cout << "[" << line << "]" << std::endl;
			std::cout << "External Api 로드 실패" << std::endl;
			continue;
		}

		if(externalApiMap.find(name) != externalApiMap.end())
		{
			std::cout << "[" << line << "]" << std::endl;
			std::cout << "동일한 변수 명이 이미 있음" << std::endl;
			continue;
		}
		
		externalApiMap.insert(std::make_pair(name, externalApi));
	}

	//함수 로드
	for (std::string line; std::getline(scriptStream, line) && !line.empty(); )
	{
		std::string name;
		std::string function;

		std::string::size_type position = line.find(assignIndicator);
		if(position == std::string::npos)
		{
			std::cout << "[" << line << "]" << std::endl;
			std::cout << "External Api의 변수 명이 빠짐" << std::endl;
			continue;
		}

		name = Trim(line.substr(0, position));
		function = Trim(line.substr(position + assignIndicator.size())); 

		auto itor = externalApiMap.find(name);
		if(itor == externalApiMap.end())
		{
			std::cout << "[" << line << "]" << std::endl;
			std::cout << "존재하지 않는 External Api의 변수 " << std::endl;
			continue;
		}

		auto externalApi = itor->second;

		FunctionType functionType;
		if(!FunctionType::Generate(functionType, function, *externalApi))
		{
			std::cout << "[" << line << "]" << std::endl;
			std::cout << "입력된 함수 타입이 정상적이지 않음" << std::endl;
			continue;
		}
				
		if (function.substr(0, 6) == "struct")
		{
			// 구조체 타입 추가
			Structure structure(functionType.name);
			auto& fields = functionType.parameters;
			for (auto fieldItor = fields.begin(), end = fields.end()
				; fieldItor != end; ++fieldItor)
			{
				structure.AddField(fieldItor->name, fieldItor->type);
			}

			externalApi->RegisterType(structure.GetName(), Value(structure));
		}
		else
		{
			if(!externalApi->Load(functionType))
			{
				std::cout << "[" << line << "]" << std::endl;
				std::cout << "함수 로드 실패" << std::endl;
				continue;
			}
		}		
	}

	//함수 호출
	for (std::string line; std::getline(scriptStream, line) && !line.empty(); )
	{
		getchar();

		std::string::size_type apiNameEndPosition = line.find('.');
		std::string::size_type parameterStartPosition = line.find('(');
		std::string::size_type parameterEndPosition = line.rfind(')');

		if (apiNameEndPosition == std::string::npos 
			|| parameterStartPosition == std::string::npos
			|| parameterEndPosition == std::string::npos)
		{
			std::cout << "[" << line << "]" << std::endl;
			std::cout << "문법 오류" << std::endl;
			continue;
		}

		if(apiNameEndPosition > parameterStartPosition)
		{
			std::cout << "[" << line << "]" << std::endl;
			std::cout << "External Api 변수 명을 찾을 수 없습니다." << std::endl;
			continue;
		}

		if(parameterStartPosition > parameterEndPosition)
		{
			std::cout << "[" << line << "]" << std::endl;
			std::cout << "함수 입력을 찾을 수 없습니다." << std::endl;
			continue;
		}

		std::string apiName = Trim(line.substr(0, apiNameEndPosition));
		std::string functionName 
			= Trim(line.substr(apiNameEndPosition + 1
			, parameterStartPosition - apiNameEndPosition - 1));
		auto rawParameterData = line.substr(parameterStartPosition + 1
			, parameterEndPosition - parameterStartPosition - 1);

		std::vector<scenario::Value> parameterList;
		
		for (size_t position = 0; position < rawParameterData.size();)
		{
			std::string parameter;
			size_t tokenPosition = rawParameterData.find_first_of(",{}", position);
			if (tokenPosition != rawParameterData.npos && rawParameterData.at(tokenPosition) == '{')
			{
				size_t endBracePosition = rawParameterData.find('}', tokenPosition);
				if (endBracePosition != rawParameterData.npos)
				{
					parameter = rawParameterData.substr(tokenPosition, endBracePosition - tokenPosition + 1);
				}
			}
			else
			{
				parameter = rawParameterData.substr(position, tokenPosition - position);
			}			

			parameterList.push_back(Parse(Trim(parameter)));
			position += parameter.size() + 1;
		}
		
		auto itor = externalApiMap.find(apiName);
		if(itor == externalApiMap.end())
		{
			std::cout << "[" << line << "]" << std::endl;
			std::cout << "존재하지 않는 External Api의 변수 " << std::endl;
			continue;
		}

		std::cout << functionName << " -> ";

		Value result;
		if(itor->second->Call(functionName, result, parameterList) == false)
		{
			std::cout << "[" << line << "]" << std::endl;
			std::cout << "함수 호출 실패" << std::endl;
		}
		else
		{
			std::cout << result << std::endl;
		}		
	}
		
	getchar();

	return 0;
}

std::vector<std::string> Split( const std::string& input, const std::string& splitIndicator )
{
	std::vector<std::string> result;

	size_t previousIndicator = 0;
	for (size_t position = input.find(splitIndicator)
		; position != input.npos
		; previousIndicator = position + splitIndicator.size(), position = input.find(splitIndicator, previousIndicator))
	{
		size_t subStringSzie = position - previousIndicator;

		if (subStringSzie != 0)
			result.push_back(input.substr(previousIndicator, subStringSzie));
	}

	if (previousIndicator < input.size())
	{
		result.push_back(input.substr(previousIndicator, input.size() - previousIndicator));
	}

	return result;
}

std::string Trim( const std::string& input )
{
	if (input.size() == 0)
		return input;

	std::size_t beg = input.find_first_not_of(" \a\b\f\n\r\t\v");
	std::size_t end = input.find_last_not_of(" \a\b\f\n\r\t\v");
	if(beg == std::string::npos) // No non-spaces
		return "";

	return std::string(input, beg, end - beg + 1);
}

Value Parse( const std::string& input )
{
	if (input.empty())
		return Value(Value::Void);

	std::string rawData = Trim(input);

	// 문자열이나 배열일 가능성있을 경우
	if (rawData.size() >= 2)
	{
		auto& startToken = *rawData.begin();
		auto& lastToken = *rawData.rbegin();

		if(startToken == '\"' && lastToken == '\"')
		{
			return Value(rawData.substr(1, rawData.size() - 2));
		}
		else if(startToken == '{' && lastToken == '}')
		{
			auto arrayRawData 
				= Split(rawData.substr(1, rawData.size() - 2), ",");
			Value result(Value::Array);

			// {} 안에 아무것도 없을 경우 크기가 0인 배열 반환
			if(arrayRawData.size() == 1 && arrayRawData[0].empty())
				return result;

			auto& resultArray = result.AsArray();
			for (auto itor = arrayRawData.begin()
				; itor != arrayRawData.end(); ++itor)
			{
				resultArray.push_back(Parse(*itor));
			}
			return result;
		}		
	}

	if(!_stricmp(rawData.c_str(), "true"))
	{
		return Value(true);   
	}
	else if(!_stricmp(rawData.c_str(), "false"))
	{
		return Value(false);
	}
	else if(rawData.find('.') != rawData.npos)
	{
		std::stringstream stream;
		stream << rawData;

		Value result(Value::Float64);
		stream >> result.AsFloat64();

		if (stream.fail() || stream.bad())
			return Value(Value::Void);
		return result;
	}
	else
	{
		std::stringstream stream;
		stream << rawData;

		Value result(Value::Int32);
		stream >> result.AsInt32();

		if (stream.fail() || stream.bad())
			return Value(Value::Void);
		return result;
	}
}

std::vector<Value> Parse( const std::vector<std::string>& input )
{
	std::vector<Value> result;

	for (auto itor = input.begin(); itor != input.end(); ++itor)
	{
		result.push_back(Parse(*itor));
	}

	return result;
}