#ifndef __ROS_VALUE_TYPE_TRAIT_H__
#define __ROS_VALUE_TYPE_TRAIT_H__

#include <Scenario/ExternalApi/Value.h>

#include <std_msgs/Int8.h>
#include <std_msgs/UInt8.h>
#include <std_msgs/Int16.h>
#include <std_msgs/UInt16.h>
#include <std_msgs/Int32.h>
#include <std_msgs/UInt32.h>
#include <std_msgs/Int64.h>
#include <std_msgs/UInt64.h>
#include <std_msgs/Float32.h>
#include <std_msgs/Float64.h>
#include <std_msgs/Bool.h>
#include <std_msgs/String.h>

template<int valueType>
struct RosTypeMapper
{
	static std::string Get();
	static std::string GetMessageType();
};

#define ROS_TYPE_MAPPER_SPECIALIZER(_VALUE_TYPE_, _ROS_TYPE_, _ROS_MESSAGE_TYPE_) \
	template<> \
struct RosTypeMapper<_VALUE_TYPE_> \
{ \
	static std::string GetType() { return _ROS_TYPE_; } \
	static std::string GetMessageType() { return _ROS_MESSAGE_TYPE_; } \
};

ROS_TYPE_MAPPER_SPECIALIZER(scenario::Value::Void, "", "");
ROS_TYPE_MAPPER_SPECIALIZER(scenario::Value::Int8, "int8", std_msgs::Int8::__s_getDataType());
ROS_TYPE_MAPPER_SPECIALIZER(scenario::Value::Uint8, "uint8", std_msgs::UInt8::__s_getDataType());
ROS_TYPE_MAPPER_SPECIALIZER(scenario::Value::Int16, "int16", std_msgs::Int16::__s_getDataType());
ROS_TYPE_MAPPER_SPECIALIZER(scenario::Value::Uint16, "uint16", std_msgs::UInt16::__s_getDataType());
ROS_TYPE_MAPPER_SPECIALIZER(scenario::Value::Int32, "int32", std_msgs::Int32::__s_getDataType());
ROS_TYPE_MAPPER_SPECIALIZER(scenario::Value::Uint32, "uint32", std_msgs::UInt32::__s_getDataType());
ROS_TYPE_MAPPER_SPECIALIZER(scenario::Value::Int64, "int64", std_msgs::Int64::__s_getDataType());
ROS_TYPE_MAPPER_SPECIALIZER(scenario::Value::Uint64, "uint64", std_msgs::UInt64::__s_getDataType());
ROS_TYPE_MAPPER_SPECIALIZER(scenario::Value::Float32, "float32", std_msgs::Float32::__s_getDataType());
ROS_TYPE_MAPPER_SPECIALIZER(scenario::Value::Float64, "float64", std_msgs::Float64::__s_getDataType());
ROS_TYPE_MAPPER_SPECIALIZER(scenario::Value::Bool, "bool", std_msgs::Bool::__s_getDataType());
ROS_TYPE_MAPPER_SPECIALIZER(scenario::Value::String, "string", std_msgs::String::__s_getDataType());

#undef ROS_TYPE_MAPPER_SPECIALIZER

#endif