#include "ValueSubscriptionCallbackHelper.h"

#include <Scenario/ExternalApi/Value.h>

#include "RosTopicExternalApi.h"
#include "RosMessageGenerator.h"

ValueSubscriptionCallbackHelper::ValueSubscriptionCallbackHelper( 
	const scenario::Value& value, boost::function<void(scenario::Value const&)>& callback )
	: mMessageType(value), mCallback(callback)
{}

ValueSubscriptionCallbackHelper::~ValueSubscriptionCallbackHelper()
{
}

ros::VoidConstPtr ValueSubscriptionCallbackHelper::deserialize( const ros::SubscriptionCallbackHelperDeserializeParams& params )
{
	if(params.buffer == nullptr)
		return ros::VoidConstPtr();

	ros::serialization::IStream stream(params.buffer, params.length);
	
	boost::shared_ptr<scenario::Value> pResult(new scenario::Value(mMessageType));
	if (!RosTopicExternalApi::Deserialize(*pResult, stream))
		return ros::VoidConstPtr();
	
	return ros::VoidConstPtr(pResult);
}

void ValueSubscriptionCallbackHelper::call( ros::SubscriptionCallbackHelperCallParams& params )
{
	auto& parameter = boost::shared_static_cast<const scenario::Value
		, const void>(params.event.getMessage());
	
	mCallback(*parameter);
}

const std::type_info& ValueSubscriptionCallbackHelper::getTypeInfo()
{	
	return typeid(scenario::Value);
}

bool ValueSubscriptionCallbackHelper::isConst()
{
	return false;
}
