#include <Scenario/ExternalApi/ExternalApi.h>
#include <Scenario/ExternalApi/Url.h>

#include <boost/thread/mutex.hpp>

#include "RosNodeManager.h"
#include "RosTopicExternalApi.h"
#include "RosServiceExternalApi.h"

extern "C"
{
	__declspec(dllexport) scenario::ExternalApi* GetExternalApi( const char* uri_ );
	__declspec(dllexport) void ReleaseExternalApi(scenario::ExternalApi* pExternalApi);
}

scenario::ExternalApi* GetExternalApi( const char* url_ )
{
	scenario::Url url(url_);

	if (!url.IsValid())
		return nullptr;

	if (url.GetScheme() != "ros")
		return nullptr;

	if (url.GetPath().size() == 0)
		return false;	

	std::string rosMasterIp = url.GetHostName().empty() ? "127.0.0.1" : url.GetHostName();
	int rosMasterPort = url.GetPortNumber() < 0 ? 11311 : url.GetPortNumber();
	
	static boost::shared_ptr<RosNodeManager> pManager;

	if (pManager.get() == nullptr)
	{
		static boost::mutex managerMutex;

		boost::mutex::scoped_lock lock(managerMutex);

		if (pManager.get() == nullptr)
		{
			RosNodeManager* pTempManager = new RosNodeManager(rosMasterIp, rosMasterPort);
			if(!pTempManager->Enable())
			{
				delete pTempManager;
				return nullptr;
			}

			pManager.reset(pTempManager);
		}
	}
		
	const auto& path = url.GetPath();
	const std::string& type = *path.rbegin();
	std::string namespace_ = "/";
	for (std::vector<std::string>::const_iterator itor = path.begin()
		, end = --path.end(); itor != end; ++itor )
		namespace_.append(*itor).append("/");
	
	if (_stricmp(type.c_str(), "topic") == 0)
	{
		return new RosTopicExternalApi(*pManager, namespace_);
	}
	else if(_stricmp(type.c_str(), "service") == 0)
	{
		return new RosServiceExternalApi(*pManager, namespace_);
	}
	else
	{
		return nullptr;
	}
}

void ReleaseExternalApi(scenario::ExternalApi* pExternalApi)
{
	delete pExternalApi;
}
