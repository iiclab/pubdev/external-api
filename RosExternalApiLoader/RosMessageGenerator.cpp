#include "RosMessageGenerator.h"

#include <iomanip>
#include <sstream>
#include <array>
#include <algorithm>

#include "md5.h"
#include "RosValueTypeTrait.h"

using scenario::Value;

bool RosMessageGenerator::CalculateMd5( std::array<uint8_t, 16>& result
	, const std::string& data )
{
	if(data.size() == 0)
		return false;

	md5_state_t state;

	md5_init(&state);
	md5_append(&state, (const md5_byte_t*)data.c_str(), data.size());
	md5_finish(&state, result.data());

	return true;
}

bool RosMessageGenerator::CalculateMd5( std::array<uint8_t, 16>& result
	, const std::vector<std::string>& datas )
{
	if(datas.size() == 0)
		return false;

	md5_state_t state;

	md5_init(&state);

	for (auto itor = datas.cbegin(), end = datas.cend(); itor != end; ++itor)
	{
		md5_append(&state, (const md5_byte_t*)itor->c_str(), itor->size());
	}

	md5_finish(&state, result.data());

	return true;
}

std::string RosMessageGenerator::CalculateSpec( const Value& type
	, const std::string& name )
{
	std::string result;

	switch (type.GetType())
	{
	case Value::Struct:
		{
			std::string structSpec = CalculateSpec(type.AsStructure());		
			std::array<uint8_t, 16> digset;
			assert(CalculateMd5(digset, structSpec));
			result = ToString(digset);
		}
		break;

	case Value::Array:
		result = CalculateSpec(type.AsArray().GetDefaultType(), "").append("[]");
		break;

	default:
		result = GetTypeName(type);
		break;
	}

	if (name.size() != 0)
		result.append(" ").append(name);	

	return result;
}

std::string RosMessageGenerator::CalculateSpec(
	const scenario::Structure& structure)
{
	std::string spec;
	for (auto itor  = structure.begin(), end = structure.end()
		; itor != end; ++itor)
		spec.append(CalculateSpec(itor->second, itor->first)).append("\n");
	
	// 마지막 '\n' 제거
	if (spec.size() != 0)
		spec.erase(spec.size() - 1);

	return spec;
}

std::string RosMessageGenerator::ToString( const std::array<uint8_t, 16>& md5 )
{
	std::stringstream stream;

	for (auto itor = md5.begin(), end = md5.end(); itor != end; ++itor)
		stream << std::setfill('0') << std::setw(2)
		<< std::hex << static_cast<unsigned int>(*itor);

	return stream.str();
}

bool RosMessageGenerator::IsPrimitiveType( const Value& value )
{	
	switch (value.GetType())
	{
	case Value::Void:
	case Value::Int8:
	case Value::Uint8:
	case Value::Int16:
	case Value::Uint16:
	case Value::Int32:
	case Value::Uint32:
	case Value::Int64:
	case Value::Uint64:
	case Value::Float32:
	case Value::Float64:
	case Value::Bool:
	case Value::String:
		return true;
		break;
	case Value::Array:
		return IsPrimitiveType(value.AsArray().GetDefaultType());
		break;
	case Value::Struct:
	default:
		return false;
		break;
	}
}

size_t RosMessageGenerator::CalculateSize( const Value& value )
{
	switch (value.GetType())
	{
	case Value::Int8:
		return ros::serialization::serializationLength(value.AsInt8());
		break;
	case Value::Uint8:
		return ros::serialization::serializationLength(value.AsUint8());
		break;
	case Value::Int16:
		return ros::serialization::serializationLength(value.AsInt16());
		break;
	case Value::Uint16:
		return ros::serialization::serializationLength(value.AsUint16());
		break;
	case Value::Int32:
		return ros::serialization::serializationLength(value.AsInt32());
		break;
	case Value::Uint32:
		return ros::serialization::serializationLength(value.AsUint32());
		break;
	case Value::Int64:
		return ros::serialization::serializationLength(value.AsInt64());
		break;
	case Value::Uint64:
		return ros::serialization::serializationLength(value.AsUint64());
		break;
	case Value::Float32:
		return ros::serialization::serializationLength(value.AsFloat32());
		break;
	case Value::Float64:
		return ros::serialization::serializationLength(value.AsFloat64());
		break;
	case Value::Bool:
		return ros::serialization::serializationLength(value.AsBool());
		break;
	case Value::String:
		return ros::serialization::serializationLength(value.AsString());
		break;
	case Value::Array:
		{
			auto& array = value.AsArray();
			size_t size = sizeof(uint32_t);
			for (auto itor = array.begin(), end = array.end()
				; itor !=  end; ++itor)
			{
				size += CalculateSize(*itor);
			}
			return size;
		}
		break;
	case Value::Struct:
		{
			auto& structure = value.AsStructure();
			size_t size = 0;
			for (auto itor = structure.begin(), end = structure.end()
				; itor !=  end; ++itor)
			{
				size += CalculateSize(itor->second);
			}
			return size;
		}		
		break;
	case Value::Void:
	default:
		return 0;
		break;
	}
}

std::string RosMessageGenerator::GetTypeName( const Value& type )
{
	switch (type.GetType())
	{
	case Value::Void:
		return RosTypeMapper<Value::Void>::GetType(); break;
	case Value::Int8:
		return RosTypeMapper<Value::Int8>::GetType(); break;
	case Value::Uint8:
		return RosTypeMapper<Value::Uint8>::GetType(); break;
	case Value::Int16:
		return RosTypeMapper<Value::Int16>::GetType(); break;
	case Value::Uint16:
		return RosTypeMapper<Value::Uint16>::GetType(); break;
	case Value::Int32:
		return RosTypeMapper<Value::Int32>::GetType(); break;
	case Value::Uint32:
		return RosTypeMapper<Value::Uint32>::GetType(); break;
	case Value::Int64:
		return RosTypeMapper<Value::Int64>::GetType(); break;
	case Value::Uint64:
		return RosTypeMapper<Value::Uint64>::GetType(); break;
	case Value::Float32:
		return RosTypeMapper<Value::Float32>::GetType(); break;
	case Value::Float64:
		return RosTypeMapper<Value::Float64>::GetType(); break;
	case Value::Bool:
		return RosTypeMapper<Value::Bool>::GetType(); break;
	case Value::String:
		return RosTypeMapper<Value::String>::GetType(); break;
	case Value::Struct: 
		{
			std::string result  = type.AsStructure().GetName();
			std::replace(result.begin(), result.end(), '.', '/');
			return result;
		}
		break;
	case Value::Array:
		return GetTypeName(type.AsArray().GetDefaultType()).append("[]"); break;
	default:
		return "";
		break;
	}
}

std::string RosMessageGenerator::GetMessageDefinition(
	const scenario::Structure& structure )
{
	std::list<Value> dependentTypes;
	std::string definition;
	for (auto itor = structure.begin(), end = structure.end()
		; itor != end; ++itor)
	{
		definition.append(GetTypeName(itor->second));
		definition.append(" ").append(itor->first).append("\n");

		GetDependency(dependentTypes, itor->second);
	}			

	std::string dependency;

	for (auto itor = dependentTypes.begin(), end = dependentTypes.end()
		; itor != end; ++itor)
	{
		dependency.append("\n================================================================================\n");
		dependency.append("MSG: ").append(GetTypeName(*itor)).append("\n");
		dependency.append(GetMessageDefinition(itor->AsStructure()));
		dependency.erase(dependency.end() - 1);
	}

	definition.append(dependency);
	definition.append("\n");

	return definition;
}

Value RosMessageGenerator::Generate( const Value& type )
{
	Value::Type messageType = type.GetType();

	if (messageType == Value::Struct)
	{
		return type;
	}
	else if(messageType == Value::Array)
	{
		// 배열에 대한 처리 방법을 생각해 봐야함
		return Value();
	}
	else 
	{
		std::string typeName;

		switch (messageType)
		{
		case Value::Int8: 
			typeName = RosTypeMapper<Value::Int8>::GetMessageType(); break;
		case Value::Uint8: 
			typeName = RosTypeMapper<Value::Uint8>::GetMessageType(); break;
		case Value::Int16:
			typeName = RosTypeMapper<Value::Int16>::GetMessageType(); break;
		case Value::Uint16:
			typeName = RosTypeMapper<Value::Uint16>::GetMessageType(); break;
		case Value::Int32:
			typeName = RosTypeMapper<Value::Int32>::GetMessageType(); break;
		case Value::Uint32:
			typeName = RosTypeMapper<Value::Uint32>::GetMessageType(); break;
		case Value::Int64:
			typeName = RosTypeMapper<Value::Int64>::GetMessageType(); break;
		case Value::Uint64:
			typeName = RosTypeMapper<Value::Uint64>::GetMessageType(); break;
		case Value::Float32:
			typeName = RosTypeMapper<Value::Float32>::GetMessageType(); break;
		case Value::Float64:
			typeName = RosTypeMapper<Value::Float64>::GetMessageType(); break;
		case Value::Bool:
			typeName = RosTypeMapper<Value::Bool>::GetMessageType(); break;
		case Value::String:
			typeName = RosTypeMapper<Value::String>::GetMessageType(); break;
		default: return Value(); break;
		}

		// 표준 메시지 타입으로 변경
		std::replace(typeName.begin(), typeName.end(), '/', '.');
		
		scenario::Structure message(typeName);
		message.SetName(typeName);
		message.AddField("data", type);
		return Value(message);
	}
}

void RosMessageGenerator::GetDependency( std::list<scenario::Value>& output
	, const scenario::Value& type )
{
	switch (type.GetType())
	{
	case Value::Struct:
		output.push_back(type);
		break;
	case Value::Array:
		GetDependency(output, type.AsArray().GetDefaultType());
		break;
	}
}
