#ifndef __ROS_NODE_MANAGER_H__
#define __ROS_NODE_MANAGER_H__

#include <string>

namespace ros
{
	class NodeHandle;
	class AsyncSpinner;
}

class RosNodeManager
{
public:
	RosNodeManager(const std::string& masterIp, int masterPort = 11311);
	~RosNodeManager();

private:
	// for non copyable
	RosNodeManager(const RosNodeManager&);
	RosNodeManager& operator=(const RosNodeManager&);
	
public:
	bool Enable();
	bool Disable();

	inline bool IsEnabled()
	{
		return mIsEnabled;
	}

	inline ros::NodeHandle& GetNodeHandle()
	{
		return *mpNodeHandle;
	}

private:
	bool GetLocalIp(std::string& output);
	
private:
	const std::string mMasterIp;
	const int mMasterPort;
	bool mIsEnabled;
	ros::AsyncSpinner* mpAsyncSpinner;
	ros::NodeHandle* mpNodeHandle;
};

#endif