#ifndef __ROS_MESSAGE_GENERATOR_H__
#define __ROS_MESSAGE_GENERATOR_H__

#include <string>
#include <vector>
#include <array>
#include <cstdint>

#include "Scenario/ExternalApi/ExternalApi.h"

class RosMessageGenerator
{
public:
	static bool CalculateMd5(std::array<uint8_t, 16>& result, const std::string& data);
	static bool CalculateMd5(std::array<uint8_t, 16>& result, const std::vector<std::string>& datas);

	static std::string CalculateSpec(const scenario::Structure& structure);

	static std::string ToString(const std::array<uint8_t, 16>& md5);

	static bool IsPrimitiveType(const scenario::Value& value);

	static size_t CalculateSize(const scenario::Value& value);

	static std::string GetTypeName(const scenario::Value& type);
	static std::string GetMessageDefinition(const scenario::Structure& structure);

	static scenario::Value Generate(const scenario::Value& type);

private:
	static std::string CalculateSpec(const scenario::Value& type, const std::string& name);
	static void GetDependency(std::list<scenario::Value>& output, const scenario::Value& type);
};

#endif