#ifndef __ROS_SERVICE_EXTERNAL_API_H__
#define __ROS_SERVICE_EXTERNAL_API_H__

#include <unordered_map>

#include <Scenario/ExternalApi/ExternalApi.h>

#include <boost/thread/shared_mutex.hpp>

class RosNodeManager;

namespace ros
{
	namespace serialization
	{
		struct OStream;
		struct IStream;
	}

	class ServiceClient;
}

class RosServiceExternalApi : public scenario::ExternalApi
{
private:
	struct ServiceSet;

public:
	RosServiceExternalApi(RosNodeManager& manager, const std::string& namespace_);
	virtual ~RosServiceExternalApi();

public:
	virtual bool Load(const scenario::FunctionType& functionType);
	virtual bool Call(const std::string& functionName, scenario::Value& output, const std::vector<scenario::Value>& input);

private:
	std::string CreateServiceClient(ros::ServiceClient& output, const scenario::FunctionType& functionType, const std::string& namespace_ = "");
	bool Serialize(ros::serialization::OStream& stream, const scenario::Value& value);
	bool Deserialize(scenario::Value& output, ros::serialization::IStream& stream);

private:
	RosNodeManager& mRosNodeManager;
	const std::string mNamespace;
	std::unordered_map<std::string, ServiceSet> mRosServiceMap;
	boost::shared_mutex mRosServiceMapMutex;
};


#endif