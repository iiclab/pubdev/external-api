#include "RosTopicExternalApi.h"

#include <boost/thread/locks.hpp>
#include <boost/noncopyable.hpp>
#include <boost/bind.hpp>

#include <ros/topic_manager.h>

#include "RosNodeManager.h"
#include "RosMessageGenerator.h"
#include "ValueSubscriptionCallbackHelper.h"

class RosTopicMessageDispatcher : private boost::noncopyable
{
public:
	RosTopicMessageDispatcher()
		: mpValue(nullptr)
	{
	}

	~RosTopicMessageDispatcher()
	{
		delete mpValue;
	}

public:
	void OnReceived(scenario::Value const& message)
	{
		boost::unique_lock<boost::shared_mutex> writerLock(mValueMutex);

		if(mpValue == nullptr)
			mpValue = new scenario::Value(message);
		else
			*mpValue = message;
	}

	bool GetMessage(scenario::Value& output)
	{
		boost::shared_lock<boost::shared_mutex> readerLock(mValueMutex);

		if(mpValue == nullptr)
			return false;

		output = *mpValue;
		return true;
	}

private:
	scenario::Value* mpValue;
	boost::shared_mutex mValueMutex; 
};

struct RosTopicExternalApi::TopicSet
{
	scenario::Value type;
	ros::Publisher publihser;
	ros::Subscriber subscriber;
	boost::shared_ptr<RosTopicMessageDispatcher> dispatcher;
};

RosTopicExternalApi::RosTopicExternalApi(RosNodeManager& manager
	, const std::string& namespace_)
	: mRosNodeManager(manager), mNamespace(namespace_)
{
}

RosTopicExternalApi::~RosTopicExternalApi()
{
}

bool RosTopicExternalApi::Load( const scenario::FunctionType& functionType )
{
	boost::upgrade_lock<boost::shared_mutex> upgradeLock(mRosTopicMapMutex);

	if (mRosTopicMap.count(functionType.name) != 0)
		return false;

	if (functionType.returnType.GetType() == scenario::Value::Void
		|| functionType.parameters.size() != 1
		|| functionType.returnType.GetType() 
			!= functionType.parameters[0].type.GetType())
		return false;

	boost::upgrade_to_unique_lock<boost::shared_mutex> writerLock(upgradeLock);
	
	ros::Publisher publihser;
	if (!CreateRosPublisher(publihser, functionType.name
		, functionType.returnType, mNamespace))
		return false;

	auto dispatcher 
		= boost::shared_ptr<RosTopicMessageDispatcher>(new RosTopicMessageDispatcher);
	ros::Subscriber subscriber;
	if (!CreateRosSubscriber(subscriber, functionType.name
		, functionType.returnType, dispatcher, mNamespace))
	{
		publihser.shutdown();
		return false;
	}

	TopicSet topicSet = {functionType.returnType, publihser, subscriber, dispatcher};
	mRosTopicMap[functionType.name] = topicSet;
	return true;
}

bool RosTopicExternalApi::Call( const std::string& functionName
	, scenario::Value& output, const std::vector<scenario::Value>& input )
{
	boost::shared_lock<boost::shared_mutex> readerLock(mRosTopicMapMutex);

	auto topicSetItor = mRosTopicMap.find(functionName);
	if (topicSetItor == mRosTopicMap.end())
		return false;
	
	TopicSet& topicSet = topicSetItor->second;
	
	scenario::Value value;

	if (input.size() == 0)
	{
		//getter
		if (!topicSet.dispatcher->GetMessage(value))
			return false;
		value.ConvertTo(topicSet.type);
	}
	else
	{
		//setter
		value = input[0];
		value.ConvertTo(topicSet.type);
		if (!PublishTopic(topicSet.publihser, value))
			return false;
	}
	
	output = value;

	return true;
}

bool RosTopicExternalApi::CreateRosPublisher( ros::Publisher& output
	, const std::string& name, const scenario::Value& type
	, const std::string& namesapce_)
{	
	const scenario::Value message = RosMessageGenerator::Generate(type);
	if (message.GetType() != scenario::Value::Struct)
		return false;

	std::string typeSpec = RosMessageGenerator::CalculateSpec(message.AsStructure());

	std::array<uint8_t, 16> md5sum;
	if (!RosMessageGenerator::CalculateMd5(md5sum, typeSpec))
		return false;

	ros::NodeHandle& nodeHandle = mRosNodeManager.GetNodeHandle();

	const int queueSize = 10;

	ros::AdvertiseOptions options;
	options.topic = std::string(namesapce_).append(name);
	options.queue_size = queueSize;
	options.connect_cb = ros::SubscriberStatusCallback();
	options.disconnect_cb = ros::SubscriberStatusCallback();
	options.md5sum = RosMessageGenerator::ToString(md5sum);
	options.datatype = RosMessageGenerator::GetTypeName(message);
	options.message_definition 
		= RosMessageGenerator::GetMessageDefinition(message.AsStructure());
	options.has_header = false;

	output = nodeHandle.advertise(options);

	if (!output)
		return false;
	return true;
}

bool RosTopicExternalApi::CreateRosSubscriber( ros::Subscriber& output
	, const std::string& name, const scenario::Value& type
	, boost::shared_ptr<RosTopicMessageDispatcher>& dispatcher
	, const std::string& namesapce_)
{
	const scenario::Value message = RosMessageGenerator::Generate(type);
	if (message.GetType() != scenario::Value::Struct)
		return false;

	std::string typeSpec = RosMessageGenerator::CalculateSpec(message.AsStructure());

	std::array<uint8_t, 16> md5sum;
	if (!RosMessageGenerator::CalculateMd5(md5sum, typeSpec))
		return false;

	ros::NodeHandle& nodeHandle = mRosNodeManager.GetNodeHandle();
	
	const int queueSize = 10;
		
	ros::SubscribeOptions options;
	options.topic = std::string(namesapce_).append(name);
	options.queue_size = queueSize;
	options.md5sum = RosMessageGenerator::ToString(md5sum);
	options.datatype = RosMessageGenerator::GetTypeName(message);		
	options.tracked_object = dispatcher;
	options.transport_hints = ros::TransportHints();

	 boost::function<void(scenario::Value const&)> callback 
		 = boost::bind(&RosTopicMessageDispatcher::OnReceived, dispatcher, ::_1);
	 
	options.helper = ros::SubscriptionCallbackHelperPtr(
		new ValueSubscriptionCallbackHelper(type, callback));

	output = nodeHandle.subscribe(options);

	if(!output)
		return false;
	return true;
}

bool RosTopicExternalApi::PublishTopic( ros::Publisher& publihser
	, const scenario::Value& value )
{
	ros::SerializedMessage message;
	
	ros::TopicManager::instance()->publish(publihser.getTopic()
		, boost::bind(&RosTopicExternalApi::Serialize, boost::ref(value)), message);
	return true;
}

ros::SerializedMessage RosTopicExternalApi::Serialize( const scenario::Value& value )
{
	ros::SerializedMessage message;
	size_t messageSize = RosMessageGenerator::CalculateSize(value);
		
	message.num_bytes = messageSize + 4;
	message.buf.reset(new uint8_t[message.num_bytes]);

	ros::serialization::OStream messageStream(message.buf.get(), message.num_bytes);
	messageStream.next(message.num_bytes - 4);

	message.message_start = messageStream.getData();

	assert(Serialize(messageStream, value));

	return message;
}

bool RosTopicExternalApi::Serialize( ros::serialization::OStream& stream
	, const scenario::Value& value )
{
	try
	{
		switch (value.GetType())
		{
		case scenario::Value::Void:	break;
		case scenario::Value::Int8: stream.next(value.AsInt8()); break;
		case scenario::Value::Uint8: stream.next(value.AsUint8()); break;
		case scenario::Value::Int16: stream.next(value.AsInt16()); break;
		case scenario::Value::Uint16: stream.next(value.AsUint16()); break;
		case scenario::Value::Int32: stream.next(value.AsInt32()); break;
		case scenario::Value::Uint32: stream.next(value.AsUint32()); break;
		case scenario::Value::Int64: stream.next(value.AsInt64()); break;
		case scenario::Value::Uint64: stream.next(value.AsUint64()); break;
		case scenario::Value::Float32: stream.next(value.AsFloat32()); break;
		case scenario::Value::Float64: stream.next(value.AsFloat64()); break;
		case scenario::Value::Bool: stream.next(value.AsBool()); break;
		case scenario::Value::String: stream.next(value.AsString()); break;
		case scenario::Value::Struct:
			{
				auto& structure = value.AsStructure();
				for (auto itor = structure.begin(), end = structure.end()
					; itor != end; ++itor)
				{
					if (!Serialize(stream, itor->second))
						return false;					
				}				
			}
			break;
		case scenario::Value::Array:
			{
				auto& array = value.AsArray();
				uint32_t size = array.size();
				stream.next(size);
				for (auto itor = array.begin(), end = array.end()
					; itor != end; ++itor)
				{
					if (!Serialize(stream, *itor))
						return false;				
				}
			}
			break;
		default:
			return false;
		}
	}
	catch(ros::serialization::StreamOverrunException& exception)
	{
		return false;
	}

	return true;
}

bool RosTopicExternalApi::Deserialize( scenario::Value& output
	, ros::serialization::IStream& stream )
{
	try
	{
		switch (output.GetType())
		{
		case scenario::Value::Void:	break;
		case scenario::Value::Int8: stream.next(output.AsInt8()); break;
		case scenario::Value::Uint8: stream.next(output.AsUint8()); break;
		case scenario::Value::Int16: stream.next(output.AsInt16()); break;
		case scenario::Value::Uint16: stream.next(output.AsUint16()); break;
		case scenario::Value::Int32: stream.next(output.AsInt32()); break;
		case scenario::Value::Uint32: stream.next(output.AsUint32()); break;
		case scenario::Value::Int64: stream.next(output.AsInt64()); break;
		case scenario::Value::Uint64: stream.next(output.AsUint64()); break;
		case scenario::Value::Float32: stream.next(output.AsFloat32()); break;
		case scenario::Value::Float64: stream.next(output.AsFloat64()); break;
		case scenario::Value::Bool: stream.next(output.AsBool()); break;
		case scenario::Value::String: stream.next(output.AsString()); break;
		case scenario::Value::Struct:
			{
				auto& structure = output.AsStructure();
				for (auto itor = structure.begin(), end = structure.end()
					; itor != end; ++itor)
				{
					if (!Deserialize(itor->second, stream))
						return false;					
				}				
			}
			break;
		case scenario::Value::Array:
			{
				auto& array = output.AsArray();
				uint32_t size = 0;

				stream.next(size);
				array.resize(size);
				array.Normalize();

				for (auto itor = array.begin(), end = array.end()
					; itor != end; ++itor)
				{
					if (!Deserialize(*itor, stream))
						return false;				
				}
			}
			break;
		default:
			return false;
		}
	}
	catch(ros::serialization::StreamOverrunException& exception)
	{
		return false;
	}

	return true;
}
