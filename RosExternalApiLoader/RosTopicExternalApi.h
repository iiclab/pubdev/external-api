#ifndef __ROS_TOPIC_EXTERNAL_API_H__
#define __ROS_TOPIC_EXTERNAL_API_H__

#include <unordered_map>

#include <Scenario/ExternalApi/ExternalApi.h>

#include <ros/ros.h>

#include <boost/thread/shared_mutex.hpp>

class RosNodeManager;
class RosTopicMessageDispatcher;

class RosTopicExternalApi : public scenario::ExternalApi
{
private:
	struct TopicSet;

public:
	RosTopicExternalApi(RosNodeManager& manager, const std::string& namespace_);
	virtual ~RosTopicExternalApi();

public:
	virtual bool Load(const scenario::FunctionType& functionType);
	virtual bool Call(const std::string& functionName, scenario::Value& output, const std::vector<scenario::Value>& input);

public:
	static ros::SerializedMessage Serialize(const scenario::Value& value); 
	static bool Serialize(ros::serialization::OStream& stream, const scenario::Value& value);
	static bool Deserialize(scenario::Value& output, ros::serialization::IStream& stream);

private:
	bool CreateRosPublisher(ros::Publisher& output, const std::string& name, const scenario::Value& type, const std::string& namesapce_ = "");
	bool CreateRosSubscriber(ros::Subscriber& output, const std::string& name, const scenario::Value& type, boost::shared_ptr<RosTopicMessageDispatcher>& dispatcher, const std::string& namesapce_ = ""); 
	bool PublishTopic(ros::Publisher& publihser, const scenario::Value& value);
		
private:
	RosNodeManager& mRosNodeManager;
	const std::string mNamespace;
	std::unordered_map<std::string, TopicSet> mRosTopicMap;
	boost::shared_mutex mRosTopicMapMutex;
};

#endif