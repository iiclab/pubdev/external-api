#include "RosServiceExternalApi.h"

#include <boost/thread/locks.hpp>
#include <boost/noncopyable.hpp>

#include <ros/ros.h>
#include <std_msgs/Int8.h>
#include <std_msgs/UInt8.h>
#include <std_msgs/Int16.h>
#include <std_msgs/UInt16.h>
#include <std_msgs/Int32.h>
#include <std_msgs/UInt32.h>
#include <std_msgs/Int64.h>
#include <std_msgs/UInt64.h>
#include <std_msgs/Float32.h>
#include <std_msgs/Float64.h>
#include <std_msgs/Bool.h>
#include <std_msgs/String.h>

#include "RosNodeManager.h"
#include "RosMessageGenerator.h"

struct RosServiceExternalApi::ServiceSet
{
	scenario::FunctionType type;
	std::string md5;
	ros::ServiceClient client;
};

RosServiceExternalApi::RosServiceExternalApi( RosNodeManager& manager
	, const std::string& namespace_)
	: mRosNodeManager(manager), mNamespace(namespace_)
{
}

RosServiceExternalApi::~RosServiceExternalApi()
{
}

bool RosServiceExternalApi::Load( const scenario::FunctionType& functionType )
{
	boost::upgrade_lock<boost::shared_mutex> upgradeLock(mRosServiceMapMutex);

	if (mRosServiceMap.count(functionType.name) != 0)
		return false;
		
	boost::upgrade_to_unique_lock<boost::shared_mutex> writerLock(upgradeLock);

	ros::ServiceClient client;
	std::string md5 = CreateServiceClient(client, functionType, mNamespace);
	if (md5.empty())
		return false;

	ServiceSet serviceSet = {functionType, md5, client};
	mRosServiceMap[functionType.name] = serviceSet;
	return true;
}

bool RosServiceExternalApi::Call( const std::string& functionName
	, scenario::Value& output, const std::vector<scenario::Value>& input )
{
	boost::shared_lock<boost::shared_mutex> readerLock(mRosServiceMapMutex);

	auto serviceSetItor = mRosServiceMap.find(functionName);
	if (serviceSetItor == mRosServiceMap.end())
		return false;

	scenario::FunctionType& functionType = serviceSetItor->second.type;
	ros::ServiceClient& client = serviceSetItor->second.client;

	ros::SerializedMessage request;
	ros::SerializedMessage response;

	std::vector<scenario::Value> requestDatas;
	requestDatas.resize(functionType.parameters.size());

	size_t requestSize = 0;
	for (size_t i = 0, end = functionType.parameters.size(); i < end; i++)
	{
		const scenario::Value& originalType = functionType.parameters[i].type;

		if (i < input.size())
		{
			requestDatas[i] = input[i];
			requestDatas[i].ConvertTo(originalType);
		}
		else
		{
			requestDatas[i] = originalType;
		}

		requestSize += RosMessageGenerator::CalculateSize(requestDatas[i]);
	}
	
	request.num_bytes = requestSize + 4;
	request.buf.reset(new uint8_t[request.num_bytes]);
	ros::serialization::OStream requestStream(request.buf.get(), request.num_bytes);

	requestStream.next(request.num_bytes - 4);

	for (auto itor = requestDatas.begin(), end = requestDatas.end()
		; itor != end; ++itor)
	{
		if (!Serialize(requestStream, *itor))
			return false;		
	}	

	if (!client.call(request, response, serviceSetItor->second.md5))
		return false;

	ros::serialization::IStream responseStream(response.message_start
		, response.num_bytes - (response.message_start - response.buf.get()));

	output = scenario::Value(functionType.returnType);
	if (!Deserialize(output, responseStream))
		return false;

	return true;
}

std::string RosServiceExternalApi::CreateServiceClient( ros::ServiceClient& output
	, const scenario::FunctionType& functionType, const std::string& namespace_)
{
	const scenario::Value response
		= RosMessageGenerator::Generate(functionType.returnType);

	std::string responseSpec;

	switch (response.GetType())
	{
	case scenario::Value::Struct:
		responseSpec = RosMessageGenerator::CalculateSpec(response.AsStructure());
		break;
	case scenario::Value::Void:
		responseSpec = "";
		break;
	default:
		return ""; break;
	}
		
	scenario::Structure request;
	for (auto itor  = functionType.parameters.cbegin()
		, end = functionType.parameters.cend(); itor != end; ++itor)
		request.push_back(std::make_pair(itor->name, itor->type));

	std::string requestSpec
		= RosMessageGenerator::CalculateSpec(request);

	std::vector<std::string> serviceSpec;
	serviceSpec.push_back(requestSpec);
	serviceSpec.push_back(responseSpec);

	std::array<uint8_t, 16> md5sum;
	if (!RosMessageGenerator::CalculateMd5(md5sum, serviceSpec))
		return "";
	
	ros::ServiceClientOptions option(std::string(namespace_)
		.append(functionType.name), RosMessageGenerator::ToString(md5sum)
		, false, ros::M_string());

	ros::NodeHandle& nodeHandle = mRosNodeManager.GetNodeHandle();
	output = nodeHandle.serviceClient(option);
	
	return option.md5sum;
}

bool RosServiceExternalApi::Serialize( ros::serialization::OStream& stream
	, const scenario::Value& value )
{
	try
	{
		switch (value.GetType())
		{
		case scenario::Value::Void:	break;
		case scenario::Value::Int8: stream.next(value.AsInt8()); break;
		case scenario::Value::Uint8: stream.next(value.AsUint8()); break;
		case scenario::Value::Int16: stream.next(value.AsInt16()); break;
		case scenario::Value::Uint16: stream.next(value.AsUint16()); break;
		case scenario::Value::Int32: stream.next(value.AsInt32()); break;
		case scenario::Value::Uint32: stream.next(value.AsUint32()); break;
		case scenario::Value::Int64: stream.next(value.AsInt64()); break;
		case scenario::Value::Uint64: stream.next(value.AsUint64()); break;
		case scenario::Value::Float32: stream.next(value.AsFloat32()); break;
		case scenario::Value::Float64: stream.next(value.AsFloat64()); break;
		case scenario::Value::Bool: stream.next(value.AsBool()); break;
		case scenario::Value::String: stream.next(value.AsString()); break;
		case scenario::Value::Struct:
			{
				auto& structure = value.AsStructure();
				for (auto itor = structure.begin(), end = structure.end()
					; itor != end; ++itor)
				{
					if (!Serialize(stream, itor->second))
						return false;					
				}				
			}
			break;
		case scenario::Value::Array:
			{
				auto& array = value.AsArray();
				uint32_t size = array.size();
				stream.next(size);
				for (auto itor = array.begin(), end = array.end()
					; itor != end; ++itor)
				{
					if (!Serialize(stream, *itor))
						return false;				
				}
			}
			break;
		default:
			return false;
		}
	}
	catch(ros::serialization::StreamOverrunException& exception)
	{
		return false;
	}

	return true;
}

bool RosServiceExternalApi::Deserialize( scenario::Value& output
	, ros::serialization::IStream& stream )
{
	try
	{
		switch (output.GetType())
		{
		case scenario::Value::Void:	break;
		case scenario::Value::Int8: stream.next(output.AsInt8()); break;
		case scenario::Value::Uint8: stream.next(output.AsUint8()); break;
		case scenario::Value::Int16: stream.next(output.AsInt16()); break;
		case scenario::Value::Uint16: stream.next(output.AsUint16()); break;
		case scenario::Value::Int32: stream.next(output.AsInt32()); break;
		case scenario::Value::Uint32: stream.next(output.AsUint32()); break;
		case scenario::Value::Int64: stream.next(output.AsInt64()); break;
		case scenario::Value::Uint64: stream.next(output.AsUint64()); break;
		case scenario::Value::Float32: stream.next(output.AsFloat32()); break;
		case scenario::Value::Float64: stream.next(output.AsFloat64()); break;
		case scenario::Value::Bool: stream.next(output.AsBool()); break;
		case scenario::Value::String: stream.next(output.AsString()); break;
		case scenario::Value::Struct:
			{
				auto& structure = output.AsStructure();
				for (auto itor = structure.begin(), end = structure.end()
					; itor != end; ++itor)
				{
					if (!Deserialize(itor->second, stream))
						return false;					
				}				
			}
			break;
		case scenario::Value::Array:
			{
				auto& array = output.AsArray();
				uint32_t size = 0;

				stream.next(size);
				array.resize(size);
				array.Normalize();

				for (auto itor = array.begin(), end = array.end()
					; itor != end; ++itor)
				{
					if (!Deserialize(*itor, stream))
						return false;				
				}
			}
			break;
		default:
			return false;
		}
	}
	catch(ros::serialization::StreamOverrunException& exception)
	{
		return false;
	}

	return true;
}
