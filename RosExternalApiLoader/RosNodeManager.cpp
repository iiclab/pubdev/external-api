#include "RosNodeManager.h"

#include <map>
#include <string>
#include <functional>

#include <boost/asio.hpp>
#include <ros/spinner.h>
#include <boost/thread.hpp>

#include <ros/ros.h>


RosNodeManager::RosNodeManager(const std::string& masterIp, int masterPort)
	: mMasterIp(masterIp), mMasterPort(masterPort), mIsEnabled(false)
	, mpAsyncSpinner(nullptr), mpNodeHandle(nullptr)
{
}

RosNodeManager::~RosNodeManager()
{
	Disable();
}

bool RosNodeManager::Enable()
{
	if (mIsEnabled)
		return true;

	std::map<std::string, std::string> rosParameter;
	{	
		std::string myIp;
		if (!GetLocalIp(myIp))
			return false;

		std::stringstream stream;
		stream << "http://" << mMasterIp << ":" << mMasterPort;

		rosParameter["__master"] = stream.str();
		rosParameter["__ip"] = myIp;		
	}

	try
	{
		ros::init(rosParameter, "RosExternalApi", ros::init_options::AnonymousName);
	}
	catch(ros::InvalidNodeNameException& exception)
	{
		std::cout << exception.what() << std::endl;
		return false;
	}

	mpNodeHandle = new ros::NodeHandle();
	mpAsyncSpinner = new ros::AsyncSpinner(0);

	mpAsyncSpinner->start();

	mIsEnabled = true;
	return true;
}

bool RosNodeManager::Disable()
{
	if (!mIsEnabled)
		return true;

	ros::requestShutdown();
	
	mpAsyncSpinner->stop();

	delete mpAsyncSpinner;
	mpAsyncSpinner = nullptr;

	delete mpNodeHandle;
	mpNodeHandle = nullptr;

	mIsEnabled = false;
	return true;
}

bool RosNodeManager::GetLocalIp( std::string& output )
{
	using namespace boost::asio::ip;
	typedef tcp Protocol;

	boost::system::error_code errorCdoe;
	boost::asio::io_service ioService;

	Protocol::resolver resolver(ioService);
	Protocol::resolver::query query(mMasterIp, "");
	auto endpointItor = resolver.resolve(query, errorCdoe);

	if (errorCdoe)
		return false;

	if (endpointItor == Protocol::resolver_iterator())
		return false;
	
	Protocol::socket socket(ioService);			
	socket.connect(Protocol::endpoint(endpointItor->endpoint().address(), mMasterPort), errorCdoe);

	if (errorCdoe)
		return false;

	auto localEnpoint = socket.local_endpoint(errorCdoe);

	if (errorCdoe)
		return false;
	
	output = localEnpoint.address().to_string();

	socket.close();

	return true;
}
