#ifndef __VALUE_SUBSCRIPTION_CALLBACK_HELPER_H__

#include <boost/function.hpp>

#include <ros/subscription_callback_helper.h>

#include <Scenario/ExternalApi/Value.h>

class ValueSubscriptionCallbackHelper : public ros::SubscriptionCallbackHelper
{
public:
	ValueSubscriptionCallbackHelper(const scenario::Value& value, boost::function<void(scenario::Value const&)>& callback);

public:
	virtual ~ValueSubscriptionCallbackHelper();

public:
	virtual ros::VoidConstPtr deserialize(const ros::SubscriptionCallbackHelperDeserializeParams& params);
	virtual void call(ros::SubscriptionCallbackHelperCallParams& params);
	virtual const std::type_info& getTypeInfo();
	virtual bool isConst();

private:
	const scenario::Value mMessageType;
	boost::function<void(scenario::Value const&)> mCallback;
};

#endif // !__VALUE_SUBSCRIPTION_CALLBACK_HELPER_H__
