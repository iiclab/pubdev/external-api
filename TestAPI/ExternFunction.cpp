#include <cstdint>
#include <iostream>

#include <WinSock2.h>

struct Person
{
	double a, b;
	char* name;
	int age;
};

extern "C"
{
	__declspec(dllexport) void Print(const char* message);
	__declspec(dllexport) void _stdcall PrintStringArray(const char** stringArray, int count);
	__declspec(dllexport) void PrintPerson(Person person);
	__declspec(dllexport) bool Execute(const char* programName);
	__declspec(dllexport) uint64_t Factorial(uint32_t number);
	__declspec(dllexport) double Power(float base, float radix);
	__declspec(dllexport) const char* GetIpAddress();
};

void Print( const char* message )
{
	std::cout << message << std::endl;
}

void _stdcall PrintStringArray( const char** stringArray, int count)
{
	for (int i = 0; i < count; i++)
	{
		std::cout << stringArray[i] << std::endl;
	}	
}

void PrintPerson( Person person )
{
	std::cout << "Person : " << sizeof(Person) << "=";
	std::cout << person.name << ":" << person.age;
	std::cout << ", " << person.a << ", " << person.b;
	std::cout << std::endl;
}

bool Execute( const char* programName )
{
	return WinExec(programName, SW_SHOWNORMAL) > 31;
}

uint64_t Factorial( uint32_t number )
{
	uint64_t result = 1;

	for (auto temp = number; temp > 1; temp--)
	{
		result *= temp;
	}

	return result;
}

double Power( float base, float radix )
{
	return pow(base, radix);
}

const char* GetIpAddress()
{
	static char ipAddress[1024] = {'\0'};
	char hostName[256] = {'\0'};

	WSADATA wsadata;

	ipAddress[0] = '\0';
	if(WSAStartup(MAKEWORD(2, 2), &wsadata) == 0 
		&& gethostname(hostName, sizeof(hostName)) != SOCKET_ERROR) 
	{
		HOSTENT* pHostEntry = gethostbyname(hostName);
		
		if (pHostEntry != nullptr)
		{
			for (auto ip = pHostEntry->h_addr_list; *ip != nullptr; ip++)
			{
				strcat(ipAddress, inet_ntoa(*(IN_ADDR*)*ip));
				strcat(ipAddress, "; ");
			}
		}
	}

	WSACleanup();

	return ipAddress;
}
