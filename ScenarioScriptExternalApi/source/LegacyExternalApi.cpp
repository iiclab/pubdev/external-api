#include "Scenario/ExternalApi/LegacyExternalApi.h"

#include <cassert>
#include <functional>
#include <string>
#include <array>
#include <stack>
#include <sstream>

#include "Scenario/ExternalApi/DynamicLibraryLoader.h"
#include "Scenario/ExternalApi/CdeclCaller.h"
#include "Scenario/ExternalApi/StdcallCaller.h"
#include "Scenario/Util/StringUtil.h"

using namespace scenario;

LegacyExternalApi::LegacyExternalApi(const Url& url)
	: mpLibrary(nullptr)
{	
	if (url.GetScheme() != "legacy")
		return;

	if (url.GetPath().empty())
		return;	

	std::string libraryPath;

	libraryPath.append(url.GetPath()[0]);
	for(std::vector<std::string>::const_iterator itor = ++url.GetPath().begin()
		, end = url.GetPath().end(); itor != end; ++itor)
	{
		libraryPath.append("/").append(*itor);
	}

	mpLibrary = DynamicLibraryLoader::Load(libraryPath);
}

LegacyExternalApi::~LegacyExternalApi()
{
	for (auto itor = mFunctionMap.begin()
		; itor != mFunctionMap.end(); ++itor)
	{
		delete itor->second;
		itor->second = nullptr;
	}

	delete mpLibrary;
}

bool LegacyExternalApi::Load( const FunctionType& functionType )
{
	if(mpLibrary == nullptr || functionType.name.empty())
		return false;

	auto itor = mFunctionMap.find(functionType.name);

	if(itor != mFunctionMap.end())
		return false;

	FunctionCaller* pFunctionCaller = nullptr;

	if(_stricmp(functionType.option.c_str(), "stdcall") == 0)
	{		
		auto pFunction = mpLibrary->GetFunction<void*>(functionType.name);
		if (pFunction == nullptr)
		{
			std::stringstream stream;
			stream << "_" << functionType.name << "@";

			size_t paramtersSize = 0;
			for (auto itor = functionType.parameters.begin()
				, end = functionType.parameters.end(); itor != end; ++itor)
			{
				Value::Type type = itor->type.GetType();
				size_t size = Value::GetByteSize(type);
				size = size == 0 ? sizeof(void*) : size;
				if (type == Value::Struct)
				{
					size = FunctionCaller::CalculateSize(itor->type.AsStructure());
				}

				// sizeof(void*) 의 배수로 정렬
				size += (sizeof(void*) - size % sizeof(void*)) % sizeof(void*);
				paramtersSize += size;
			}
			stream << paramtersSize;

			pFunction = mpLibrary->GetFunction<void*>(stream.str());
		}

		if(pFunction != nullptr)
			pFunctionCaller = new StdcallCaller(pFunction, functionType);
	}
	else
	{
		auto pFunction = mpLibrary->GetFunction<void*>(functionType.name);
		if(pFunction != nullptr)
			pFunctionCaller = new CdeclCaller(pFunction, functionType);
	}

	if(pFunctionCaller == nullptr)
		return false;

	mFunctionMap.insert(std::make_pair(functionType.name
		, pFunctionCaller));

	return true;
}

bool LegacyExternalApi::Call(const std::string& functionName, Value& output
							 , const std::vector<Value>& input)
{
	auto itor = mFunctionMap.find(functionName);

	if(itor == mFunctionMap.end())
		return false;

	output = itor->second->Call(input);

	return true;
}