#include "Scenario/ExternalApi/TypeManager.h"

#include <unordered_map>

#include "Scenario/ExternalApi/Value.h"
#include "Scenario/ExternalApi/TypeManagerImpl.h"
#include "Scenario/Util/StringUtil.h"

using namespace scenario;

TypeManager::TypeManager()
{
	mpImpl = new TypeManagerImpl();	
}

TypeManager::~TypeManager()
{
	delete mpImpl;
}

bool TypeManager::RegisterType( const std::string& typeName, const Value& value )
{
	return mpImpl->RegisterType(typeName, value);
}

void TypeManager::UnregisterType( const std::string& typeName )
{
	mpImpl->UnregisterType(typeName);
}

bool TypeManager::GetType( Value& output, const std::string& typeName ) const
{
	return mpImpl->GetType(output, typeName);
}

TypeManagerImpl::TypeManagerImpl()
{
	//Invalid �Ǵ� void
	mTypeMap.insert(std::make_pair("invalid", Value(Value::Void)));
	mTypeMap.insert(std::make_pair("void", Value(Value::Void)));

	//Int8
	mTypeMap.insert(std::make_pair("int8", Value(Value::Int8)));
	mTypeMap.insert(std::make_pair("int8_t", Value(Value::Int8)));
	mTypeMap.insert(std::make_pair("byte", Value(Value::Int8)));

	//uint8
	mTypeMap.insert(std::make_pair("uint8", Value(Value::Uint8)));
	mTypeMap.insert(std::make_pair("uint8_t", Value(Value::Uint8)));
	mTypeMap.insert(std::make_pair("ubyte", Value(Value::Uint8)));

	//int16
	mTypeMap.insert(std::make_pair("int16", Value(Value::Int16)));
	mTypeMap.insert(std::make_pair("int16_t", Value(Value::Int16)));
	mTypeMap.insert(std::make_pair("short", Value(Value::Int16)));

	//uint16
	mTypeMap.insert(std::make_pair("uint16", Value(Value::Uint16)));
	mTypeMap.insert(std::make_pair("uint16_t", Value(Value::Uint16)));
	mTypeMap.insert(std::make_pair("ushort", Value(Value::Uint16)));

	//int32
	mTypeMap.insert(std::make_pair("int32", Value(Value::Int32)));
	mTypeMap.insert(std::make_pair("int32_t", Value(Value::Int32)));
	mTypeMap.insert(std::make_pair("int", Value(Value::Int32)));

	//uint32
	mTypeMap.insert(std::make_pair("uint32", Value(Value::Uint32)));
	mTypeMap.insert(std::make_pair("uint32_t", Value(Value::Uint32)));
	mTypeMap.insert(std::make_pair("uint", Value(Value::Uint32)));

	//int64
	mTypeMap.insert(std::make_pair("int64", Value(Value::Int64)));
	mTypeMap.insert(std::make_pair("int64_t", Value(Value::Int64)));
	mTypeMap.insert(std::make_pair("long", Value(Value::Int64)));

	//uint64
	mTypeMap.insert(std::make_pair("uint64", Value(Value::Uint64)));
	mTypeMap.insert(std::make_pair("uint64_t", Value(Value::Uint64)));
	mTypeMap.insert(std::make_pair("ulong", Value(Value::Uint64)));

	//float32
	mTypeMap.insert(std::make_pair("float32", Value(Value::Float32)));
	mTypeMap.insert(std::make_pair("float", Value(Value::Float32)));

	//float64
	mTypeMap.insert(std::make_pair("float64", Value(Value::Float64)));
	mTypeMap.insert(std::make_pair("double", Value(Value::Float64)));

	//bool
	mTypeMap.insert(std::make_pair("bool", Value(Value::Bool)));
	mTypeMap.insert(std::make_pair("boolean", Value(Value::Bool)));

	//string
	mTypeMap.insert(std::make_pair("string", Value(Value::String)));
	mTypeMap.insert(std::make_pair("char*", Value(Value::String)));
}

bool TypeManagerImpl::RegisterType( const std::string& typeName, const Value& value )
{
	if (mTypeMap.count(typeName) != 0)
		return false;

	mTypeMap.insert(std::make_pair(typeName, value));

	return true;
}

void TypeManagerImpl::UnregisterType( const std::string& typeName )
{
	mTypeMap.erase(typeName);
}

bool TypeManagerImpl::GetType( Value& output, const std::string& typeName )
{
	size_t arrayIndicatorPosition = typeName.find("[]");
	if (arrayIndicatorPosition != typeName.npos)
	{
		std::string originalType = typeName.substr(0, arrayIndicatorPosition);
		output = Value(Value::Array);

		Value original;
		if (!GetType(original, originalType))
			return false;

		output.AsArray().SetDefaultType(original);
		return true;
	}

	auto itor = mTypeMap.find(typeName);

	if (itor != mTypeMap.end())
	{
		output = itor->second;
		return true;
	}

	return false;
}