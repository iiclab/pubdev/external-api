#include "Scenario/ExternalApi/ExternalApiLoadManager.h"

#include <memory>
#include <functional>

#include "Scenario/Util/StringUtil.h"
#include "Scenario/ExternalApi/Url.h"
#include "Scenario/ExternalApi/LegacyExternalApi.h"
#include "Scenario/ExternalApi/DynamicLibraryLoader.h"

using namespace scenario;

static ExternalApi* GetLegacyExternalApi( const char* uri_ )
{
	Url uri(uri_);
	if(!uri.IsValid())
		return nullptr;

	auto pLegacyExternalApi = new LegacyExternalApi(uri);

	if (!pLegacyExternalApi->IsValid())
	{
		delete pLegacyExternalApi;
		return nullptr;
	}

	return pLegacyExternalApi;
}

static void ReleaseLegacyExternalApi(ExternalApi* pExternalApi)
{
	delete pExternalApi;
}

static void ReleaseExternalApi(ExternalApi* pExternalApi
							   , ExternalApiReleaser releaser
							   , std::shared_ptr<DynamicLibrary> loaderHolder)
{
	releaser(pExternalApi);
	loaderHolder.reset();
}

ExternalApiLoadManager::ExternalApiLoadManager()
{
	//legacy 스킴등록
	LoaderSet legacyLoaderSet = {nullptr
		, &GetLegacyExternalApi, &ReleaseLegacyExternalApi};
	mLoaderMap.insert(std::make_pair("legacy", legacyLoaderSet));
}

ExternalApiLoadManager::~ExternalApiLoadManager()
{
	mLoaderMap.clear();
}

bool ExternalApiLoadManager::Register(const std::string& loaderPath
									  , const std::string& scheme_)
{
	std::string scheme = StringUtil::ToLower(scheme_);

	if(mLoaderMap.find(scheme) != mLoaderMap.end())
		return false;

	std::shared_ptr<DynamicLibrary> pLibrary(DynamicLibraryLoader::Load(loaderPath));
	if(pLibrary == nullptr)
		return false;

	ExternalApiLoader loader 
		= pLibrary->GetFunction<ExternalApiLoader>("GetExternalApi");
	ExternalApiReleaser releaser
		= pLibrary->GetFunction<ExternalApiReleaser>("ReleaseExternalApi");

	if(loader == nullptr || releaser == nullptr)
		return false;

	LoaderSet loaderSet = {pLibrary, loader, releaser};
	// 크리티컬 섹션, 나중에 수정해야함
	mLoaderMap.insert(std::make_pair(scheme, loaderSet)); 

	return true;
}

void ExternalApiLoadManager::Unregister(const std::string& scheme_)
{
	std::string scheme = StringUtil::ToLower(scheme_);

	auto itor = mLoaderMap.find(scheme);
	if(itor  == mLoaderMap.end())
		return;

	mLoaderMap.erase(itor);
}

std::shared_ptr<ExternalApi> ExternalApiLoadManager::Load(const Url& url)
{	
	auto itor = mLoaderMap.find(url.GetScheme());
	if(itor  == mLoaderMap.end())
		return nullptr;

	auto pExternalApi = itor->second.loader(url.ToString().c_str());
	if(pExternalApi == nullptr)
		return nullptr;

	return std::shared_ptr<ExternalApi>(pExternalApi
		, std::bind(&ReleaseExternalApi, std::placeholders::_1
		, itor->second.releaser, itor->second.pDynamicLibrary));
}

std::shared_ptr<ExternalApi> ExternalApiLoadManager::Load(const std::string& url)
{
	return Load(Url(url));
}
