#include "Scenario/ExternalApi/StdcallCaller.h"

using namespace scenario;

StdcallCaller::StdcallCaller( void* pFunction, const FunctionType& funtionType )
	: mpFunction(pFunction), mFunctionType(funtionType)
{
	assert(pFunction != nullptr);
}

StdcallCaller::~StdcallCaller()
{
}

Value StdcallCaller::Call( const std::vector<Value>& paramList )
{
	std::list<std::shared_ptr<uint8_t>> memoryHolders;
	auto stack = StackTo(paramList, mFunctionType.parameters, memoryHolders);
	std::array<StackElement, 2> returnValue;

	if (mFunctionType.returnType.GetType() == Value::Struct)
	{
		size_t structSize = CalculateSize(mFunctionType.returnType.AsStructure());
		uint8_t* pStruct = new uint8_t[structSize];
		memoryHolders.push_back(
			std::shared_ptr<uint8_t>(pStruct, std::default_delete<uint8_t[]>()));
		stack.push_front(reinterpret_cast<StackElement>(pStruct));
	}

	Call(returnValue, stack);
	return GetReturnValue(returnValue, mFunctionType.returnType);
}

void StdcallCaller::Call( std::array<StackElement, 2>& rawReturnValue
						 , std::deque<StackElement>& stack )
{
	void* pFunction = mpFunction;
	Value::Type returnType = mFunctionType.returnType.GetType();
	StackElement data;

	for (; !stack.empty(); stack.pop_back())
	{
		data = stack.back();
		__asm push data;
	}

	__asm call pFunction;

	if(returnType == Value::Float32)
	{
		float temp;
		__asm fstp temp;

		*reinterpret_cast<float*>(&rawReturnValue[0]) = temp;
	}
	else if(returnType == Value::Float64)
	{
		double temp;
		__asm fstp temp;

		*reinterpret_cast<double*>(&rawReturnValue[0]) = temp;
	}
	else
	{           
		StackElement returnValue0, returnValue1;

		__asm mov returnValue0, eax;
		__asm mov returnValue1, edx;

		rawReturnValue[0] = returnValue0;
		rawReturnValue[1] = returnValue1;
	}
}