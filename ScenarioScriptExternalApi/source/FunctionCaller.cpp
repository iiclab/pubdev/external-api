#include "Scenario/ExternalApi/FunctionCaller.h"

#include <algorithm>

#include "Scenario/ExternalApi/Array.h"
#include "Scenario/ExternalApi/Structure.h"

using namespace scenario;

std::deque<FunctionCaller::StackElement> FunctionCaller::StackTo( 
	const std::vector<Value>& parameters
	, const std::vector<FunctionType::Parameter>& parameterTypes
	, std::list<std::shared_ptr<uint8_t>>& memoryHolders )
{
	std::deque<StackElement> result;
	std::vector<uint8_t> stack;

	for (size_t i = 0, parameterCount = parameterTypes.size()
		; i < parameterCount; i++)
	{
		Value value;
		if(i < parameters.size())
		{
			value = parameters[i];
			value.ConvertTo(parameterTypes[i].type);
		}
		else
		{
			value = parameterTypes[i].type;
		}

		stack.clear();

		if (value.GetType() == Value::Struct)
		{
			const size_t bytePadding = CalculateBytePaddingSize(value.AsStructure());
			StackTo(stack, value, memoryHolders, bytePadding);
			// 구조체 마지막에 바이트패딩 추가
			stack.resize(stack.size() 
				+ (bytePadding - stack.size() % bytePadding) % bytePadding);
		}
		else
		{
			StackTo(stack, value, memoryHolders);
			// stack을 sizeof(void*) 의 배수로 정렬
			stack.resize(stack.size() 
				+ (sizeof(void*) - stack.size() % sizeof(void*)) % sizeof(void*));
		}

		result.insert(result.end(), reinterpret_cast<StackElement*>(&stack[0])
			, reinterpret_cast<StackElement*>(&stack[0] + stack.size()));
	}

	return result;
}

void scenario::FunctionCaller::StackTo( 
	std::vector<uint8_t>& stack, const Value& input
	, std::list<std::shared_ptr<uint8_t>>& memoryHolders
	, size_t bytePaddingSize)
{	
	const size_t remainBytePadding
		= bytePaddingSize == 0 ? 0 : bytePaddingSize - stack.size() % bytePaddingSize;
	size_t size = Value::GetByteSize(input.GetType());

	if (size == 0 && input.GetType() != Value::Struct)
		size = sizeof(void*);

	if (remainBytePadding < size)
		stack.insert(stack.end(), remainBytePadding, 0);

	uint8_t* pCursor = nullptr;
	switch (input.GetType())
	{
	case scenario::Value::Int8:
		pCursor = const_cast<uint8_t*>(reinterpret_cast<const uint8_t*>(&input.AsInt8()));
		stack.insert(stack.end(), pCursor, pCursor + size);
		break;
	case scenario::Value::Uint8:
		pCursor = const_cast<uint8_t*>(reinterpret_cast<const uint8_t*>(&input.AsUint8()));
		stack.insert(stack.end(), pCursor, pCursor + size);
		break;
	case scenario::Value::Int16:
		pCursor = const_cast<uint8_t*>(reinterpret_cast<const uint8_t*>(&input.AsInt16()));
		stack.insert(stack.end(), pCursor, pCursor + size);
		break;
	case scenario::Value::Uint16:
		pCursor = const_cast<uint8_t*>(reinterpret_cast<const uint8_t*>(&input.AsUint16()));
		stack.insert(stack.end(), pCursor, pCursor + size);
		break;
	case scenario::Value::Int32:
		pCursor = const_cast<uint8_t*>(reinterpret_cast<const uint8_t*>(&input.AsInt32()));
		stack.insert(stack.end(), pCursor, pCursor + size);
		break;
	case scenario::Value::Uint32:
		pCursor = const_cast<uint8_t*>(reinterpret_cast<const uint8_t*>(&input.AsUint32()));
		stack.insert(stack.end(), pCursor, pCursor + size);
		break;
	case scenario::Value::Int64:
		pCursor = const_cast<uint8_t*>(reinterpret_cast<const uint8_t*>(&input.AsInt64()));
		stack.insert(stack.end(), pCursor, pCursor + size);
		break;
	case scenario::Value::Uint64:
		pCursor = const_cast<uint8_t*>(reinterpret_cast<const uint8_t*>(&input.AsUint64()));
		stack.insert(stack.end(), pCursor, pCursor + size);
		break;
	case scenario::Value::Float32:
		pCursor = const_cast<uint8_t*>(reinterpret_cast<const uint8_t*>(&input.AsFloat32()));
		stack.insert(stack.end(), pCursor, pCursor + size);
		break;
	case scenario::Value::Float64:
		pCursor = const_cast<uint8_t*>(reinterpret_cast<const uint8_t*>(&input.AsFloat64()));
		stack.insert(stack.end(), pCursor, pCursor + size);
		break;
	case scenario::Value::Bool:
		pCursor = const_cast<uint8_t*>(reinterpret_cast<const uint8_t*>(&input.AsBool()));
		stack.insert(stack.end(), pCursor, pCursor + size);
		break;
	case scenario::Value::String:
		{
			auto& string = input.AsString();
			pCursor = new uint8_t[string.size() + 1];
			std::copy(string.begin(), string.end(), pCursor);
			pCursor[string.size()] = '\0';

			stack.insert(stack.end(), reinterpret_cast<uint8_t*>(&pCursor)
				, reinterpret_cast<uint8_t*>(&pCursor) + sizeof(pCursor));
			memoryHolders.push_back(
				std::shared_ptr<uint8_t>(pCursor, std::default_delete<uint8_t[]>()));
		}
		break;
	case scenario::Value::Array:
		{
			const Array& array = input.AsArray();

			std::vector<uint8_t> arrayStack;

			for (auto itor = array.begin(), end = array.end()
				; itor != end; ++itor)
				StackTo(arrayStack, *itor, memoryHolders);

			if (arrayStack.size() != 0)
			{
				pCursor = new uint8_t[arrayStack.size()];
				std::copy(arrayStack.begin(), arrayStack.end(), pCursor);
				memoryHolders.push_back
					(std::shared_ptr<uint8_t>(pCursor, std::default_delete<uint8_t[]>()));
			}

			stack.insert(stack.end(), reinterpret_cast<uint8_t*>(&pCursor)
				, reinterpret_cast<uint8_t*>(&pCursor) + sizeof(pCursor));
		}
		break;
	case scenario::Value::Struct:
		{
			const Structure& structure = input.AsStructure();
			bytePaddingSize = bytePaddingSize == 0 
				? CalculateBytePaddingSize(structure) : bytePaddingSize;

			for (auto itor = structure.begin(), end = structure.end()
				; itor != end; ++itor)
				StackTo(stack, itor->second, memoryHolders, bytePaddingSize);
		}
		break;
	default:
		assert(!"Not supported parameter type");
		break;
	}
}

size_t scenario::FunctionCaller::CalculateBytePaddingSize( const Structure& structure )
{
	size_t bytePaddingSize = 1;
	for (auto itor = structure.begin(), end = structure.end()
		; itor != end; ++itor)
	{
		const Value& field = itor->second;
		Value::Type fieldType = field.GetType();

		if (fieldType == Value::Struct)
		{
			bytePaddingSize = std::max(bytePaddingSize
				, CalculateBytePaddingSize(field.AsStructure()));
		}
		else
		{
			const size_t valueByteSize = Value::GetByteSize(fieldType);

			if (valueByteSize == 0)
				bytePaddingSize = std::max(bytePaddingSize, sizeof(void*));
			else
				bytePaddingSize = std::max(bytePaddingSize, valueByteSize);
		}	
	}	

	return bytePaddingSize;
}

void FunctionCaller::CalculateSize(size_t& size, const Structure& structure
								   , size_t bytePaddingSize)
{		
	if (structure.size() == 0)
	{
		const size_t remainPaddingSize = bytePaddingSize - (size % bytePaddingSize); 
		// 멤버가 없는 구조체의 크기는 1 이다.
		size += 1 <= remainPaddingSize ? 1 : (remainPaddingSize + 1);
		return;
	}	

	for (auto itor = structure.begin(), end = structure.end(); itor != end; ++itor)
	{
		const Value& field = itor->second;
		Value::Type fieldType = field.GetType();

		const size_t remainPaddingSize = bytePaddingSize - (size % bytePaddingSize); 

		if (fieldType == Value::Struct)
		{
			CalculateSize(size, field.AsStructure(), bytePaddingSize);
		}
		else
		{
			size_t fieldSize = Value::GetByteSize(fieldType);
			fieldSize = fieldSize  == 0 ? sizeof(void*) : fieldSize ;

			// 남은 바이트 패딩 안에 필드가 저장될 수 있다면 저장하고,
			// 그렇지 않을 경우, 바이트 패딩을 모두 채운 후 다음 바이트 패딩에 넣는다.
			size += fieldSize <= remainPaddingSize ? fieldSize : (remainPaddingSize + fieldSize);
		}	
	}
}

size_t FunctionCaller::CalculateSize( const Structure& structure )
{
	size_t result = 0;
	size_t bytePaddingSize = CalculateBytePaddingSize(structure);

	CalculateSize(result, structure, bytePaddingSize);

	// 멤버의 마지막에 바이트패딩 채우기
	if (result % bytePaddingSize != 0)
		result += bytePaddingSize - (result % bytePaddingSize);
	return result;
}

Value FunctionCaller::GetReturnValue( const std::array<StackElement, 2>& rawReturnValue
									 , const Value& type)
{
	Value result(type);

	switch (type.GetType())
	{
	case scenario::Value::Int8:
		result.AsInt8() = reinterpret_cast<const int8_t&>(rawReturnValue[0]);
		break;
	case scenario::Value::Uint8:
		result.AsUint8() = reinterpret_cast<const uint8_t&>(rawReturnValue[0]);
		break;
	case scenario::Value::Int16:
		result.AsInt16() = reinterpret_cast<const int16_t&>(rawReturnValue[0]);
		break;
	case scenario::Value::Uint16:
		result.AsUint16() = reinterpret_cast<const uint16_t&>(rawReturnValue[0]);
		break;
	case scenario::Value::Int32:
		result.AsInt32() = reinterpret_cast<const int32_t&>(rawReturnValue[0]);
		break;
	case scenario::Value::Uint32:
		result.AsUint32() = reinterpret_cast<const uint32_t&>(rawReturnValue[0]);
		break;
	case scenario::Value::Int64:
		result.AsInt64() = reinterpret_cast<const int64_t&>(rawReturnValue[0]);
		break;
	case scenario::Value::Uint64:
		result.AsUint64() = reinterpret_cast<const uint64_t&>(rawReturnValue[0]);
		break;
	case scenario::Value::Float32:
		result.AsFloat32() = reinterpret_cast<const float&>(rawReturnValue[0]);
		break;
	case scenario::Value::Float64:
		result.AsFloat64() = reinterpret_cast<const double&>(rawReturnValue[0]);
		break;
	case scenario::Value::Bool:
		result.AsBool() = reinterpret_cast<const bool&>(rawReturnValue[0]);
		break;
	case scenario::Value::String:
		result.AsString() = std::string(reinterpret_cast<const char*>(rawReturnValue[0]));
		break;
	case scenario::Value::Array:
		assert(!"Not surpported");
		break;
	case scenario::Value::Struct:
		{
			Structure& structure = result.AsStructure();
			result = GetReturnValue(rawReturnValue, structure
				, CalculateBytePaddingSize(structure));
		}
		break;
	default:
		break;
	}

	return result;
}

Value FunctionCaller::GetReturnValue( 
	const std::array<StackElement, 2>& rawReturnValue
	, const Structure& type, size_t bytePaddingSize )
{
	Value result(type);
	Structure& structure = result.AsStructure();
	const uint8_t* const offsetAddress
		= reinterpret_cast<const uint8_t*>(rawReturnValue[0]);
	const uint8_t* pStruct = offsetAddress;

	std::array<StackElement, 2> rawFieldData;
	for (auto itor = structure.begin(), end = structure.end(); itor != end; ++itor)
	{
		Value& field = itor->second;
		size_t fieldSize = Value::GetByteSize(field.GetType());
		fieldSize = fieldSize == 0 ? sizeof(void*) : fieldSize;

		const size_t remainPaddingSize 
			= bytePaddingSize - ((pStruct - offsetAddress) % bytePaddingSize); 

		pStruct = fieldSize <= remainPaddingSize
			? pStruct : (pStruct + remainPaddingSize);

		rawFieldData[0] = *reinterpret_cast<const StackElement*>(pStruct);
		rawFieldData[1]
		= *reinterpret_cast<const StackElement*>(pStruct + sizeof(StackElement));

		if (field.GetType() == Value::Struct)
			field = GetReturnValue(rawFieldData, field.AsStructure()
			, bytePaddingSize);
		else
			field = GetReturnValue(rawFieldData, field);

		pStruct += fieldSize;
	}

	return result;
}