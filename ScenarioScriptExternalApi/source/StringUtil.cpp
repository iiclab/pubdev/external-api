#include "Scenario/Util/StringUtil.h"

#include <algorithm>

using namespace scenario;

std::vector<std::string> StringUtil::Split( const std::string& input
										   , const std::string& splitIndicator
										   , bool noEmpty)
{
	std::vector<std::string> result;

	size_t previousIndicator = 0;
	for (size_t position = input.find(splitIndicator)
		; position != input.npos
		; previousIndicator = position + splitIndicator.size()
		, position = input.find(splitIndicator, previousIndicator))
	{
		size_t subStringSzie = position - previousIndicator;

		if (subStringSzie != 0 || !noEmpty)
			result.push_back(input.substr(previousIndicator, subStringSzie));
	}

	if (previousIndicator < input.size() || !noEmpty)
	{
		result.push_back(input.substr(previousIndicator
			, input.size() - previousIndicator));
	}

	return result;
}

std::string scenario::StringUtil::SubString( const std::string& input
											, const std::string& start
											, const std::string& end )
{
	size_t startPosition = start.empty() ? 0 : input.find(start);
	size_t endPosition = end.empty() ? std::string::npos : input.find(end);

	if (startPosition == std::string::npos)
		return "";

	startPosition += start.size();

	return input.substr(startPosition
		, endPosition == std::string::npos 
		? endPosition : endPosition - startPosition);
}

std::string StringUtil::Trim( const std::string& input )
{
	if (input.size() == 0)
		return input;

	std::size_t beg = input.find_first_not_of(" \a\b\f\n\r\t\v");
	std::size_t end = input.find_last_not_of(" \a\b\f\n\r\t\v");
	if(beg == std::string::npos) // No non-spaces
		return "";

	return std::string(input, beg, end - beg + 1);
}

std::string StringUtil::Replace( const std::string& input
								, const std::string& original
								, const std::string& changed )
{
	std::string result = input;

	for (std::string::size_type position = 0
		; (position = result.find(original, position)) != std::string::npos
		; position += changed.size())
	{
		result.replace(position, original.size(), changed);
	}

	return result;
}

std::string StringUtil::ToLower( const std::string& input )
{
	std::string result(input);

	std::transform(result.begin(), result.end(), result.begin(), tolower);

	return result;
}

std::string StringUtil::ToUpper( const std::string& input )
{
	std::string result(input);

	std::transform(result.begin(), result.end(), result.begin(), toupper);

	return result;
}