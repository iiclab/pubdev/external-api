#include "Scenario/ExternalApi/ExternalApi.h"

#include "Scenario/ExternalApi/TypeManager.h"
#include "Scenario/Util/StringUtil.h"

using namespace scenario;

bool FunctionType::Generate( FunctionType& result
							, const std::string& functionSignature
							, const TypeManager& typeManager )
{
	std::string temp = StringUtil::Trim(functionSignature);

	size_t optionPosition = temp.find_last_of(')');
	if(optionPosition == std::string::npos)
		return false;
	//옵션
	result.option = StringUtil::Trim(temp.substr(optionPosition + 1));

	temp = temp.substr(0, optionPosition + 1);
	temp = StringUtil::Replace(temp, "(", "|");
	temp = StringUtil::Replace(temp, ",", "|");
	temp = StringUtil::Replace(temp, ")", "|");
	temp = temp.replace(temp.find(' '), 1, "|"); //함수 반환형에 대해서만 구분자 삽입

	auto splitedSignature = StringUtil::Split(temp, "|");

	if (splitedSignature.size() < 2)
		return false;

	auto signatureItor = splitedSignature.begin();

	//반환형
	if (!typeManager.GetType(result.returnType, *signatureItor))
	{
		result.returnType = Value(Value::Struct);
	}

	//함수명
	result.name = StringUtil::Trim(*++signatureItor);

	//파라미터 형
	result.parameters.clear();
	result.parameters.reserve(splitedSignature.size() - 2);

	++signatureItor;
	for (auto end = splitedSignature.end(); signatureItor != end; ++signatureItor)
	{
		std::string token = StringUtil::Trim(*signatureItor);
		//파라미터 타입과 변수명 분리
		std::string::size_type splitPosition = token.find(' ');

		Parameter parameter;

		if (splitPosition != std::string::npos)
		{
			if (!typeManager.GetType(parameter.type, token.substr(0, splitPosition)))
			{
				parameter.type = Value(Value::Struct);
			}

			parameter.name = StringUtil::Trim(token.substr(splitPosition + 1));
		}
		else
		{
			if (!typeManager.GetType(parameter.type, token))
			{
				parameter.type = Value(Value::Struct);
			}
		}

		result.parameters.push_back(parameter);
	}

	return true;
}
