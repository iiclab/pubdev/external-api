#include "Scenario/ExternalApi/DynamicLibraryLoader.h"

#if defined(WIN32)
#include <Windows.h>
#else
#include <dlfcn.h>
#endif

#include <cassert>
#include <stdexcept>

using namespace scenario;

DynamicLibrary::DynamicLibrary( LibraryHandle hLibrary )
{
	assert(hLibrary != nullptr);
	mhLibrary = hLibrary;
}

DynamicLibrary::~DynamicLibrary()
{
#if defined(WIN32)
	FreeLibrary(reinterpret_cast<HMODULE>(mhLibrary));
#else
	dlclose(mhLibrary);
#endif
}

void* DynamicLibrary::GetRawFunction( const std::string& name )
{
	void* pFunction = nullptr;

#if defined(WIN32)
	pFunction = GetProcAddress(reinterpret_cast<HMODULE>(mhLibrary), name.c_str());
#else
	pFunction = dlsym(mhLibrary, name.c_str());
	if(dlerror() != nullptr)
		pFunction = nullptr;
#endif

	return pFunction;
}

DynamicLibrary* DynamicLibraryLoader::Load( const std::string& name )
{
	LibraryHandle hLibrary = nullptr;

#if defined(WIN32)
	hLibrary = LoadLibraryA(name.c_str());
#else
	hLibrary = dlopen(name.c_str(), RTLD_LAZY);
#endif

	if (hLibrary == nullptr)
	{
		return nullptr;
	}

	return new DynamicLibrary(hLibrary);
}
