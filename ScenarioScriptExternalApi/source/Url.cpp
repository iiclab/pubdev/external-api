#include "Scenario/ExternalApi/Url.h"

#include <algorithm>
#include <limits>
#include <cstdint>
#include <sstream>
#include <iterator>

#include "Scenario/Util/StringUtil.h"

using namespace scenario;

Url::Url()
	: mScheme(), mUserName(), mPassword(), mHostName()
	, mPortNumber(INVALID_PORT_NUMBER), mPath(), mQuery(), mFragment()
{
}

Url::Url( const std::string& url_ )
{
	*this = Parse(url_);
}

Url::Url( const Url& url_ )
{
	*this = url_;
}

Url::~Url()
{
}

Url& Url::operator=( const Url& url_ )
{
	if(this == &url_)
		return *this;

	if (url_.IsValid())
	{
		mScheme = url_.mScheme;
		mUserName = url_.mUserName;
		mPassword = url_.mPassword;
		mHostName = url_.mHostName;
		mPortNumber = url_.mPortNumber;
		mPath = url_.mPath;
		mQuery = url_.mQuery;
		mFragment = url_.mFragment;
	}
	else
	{
		mScheme.clear();
		mUserName.clear();
		mPassword.clear();
		mHostName.clear();
		mPortNumber = INVALID_PORT_NUMBER;
		mPath.clear();
		mQuery.clear();
		mFragment.clear();
	}

	return *this;
}

std::string Url::ToString() const
{
	if(!IsValid())
		return "";

	std::stringstream stream;

	stream << mScheme << "://";

	if(!mUserName.empty())
	{
		stream << mUserName;

		if(!mPassword.empty())
		{
			stream << ":" << mPassword;
		}

		stream << "@";
	}

	stream << mHostName;

	if(IsValidPortNumber(mPortNumber))
		stream << ":" << mPortNumber;

	for(auto itor = mPath.begin(), end = mPath.end()
		; itor != end; ++itor)
	{
		stream << "/" << *itor;
	}

	if (!mQuery.empty())
	{
		auto lastItor = --mQuery.end();		

		stream << "?";
		for(auto itor = mQuery.begin(); itor != lastItor; ++itor)
		{
			stream << itor->first << "=" << itor->second << "&";
		}
		stream << lastItor->first << "=" << lastItor->second;
	}

	if(!mFragment.empty())
		stream << "#" << mFragment;

	return stream.str();
}

Url Url::Parse( const std::string& url )
{       
	std::vector<std::string> splitedUrl
		= StringUtil::Split(StringUtil::Trim(url), "/", false);
	const Url invalidUrl;

	if (splitedUrl.size() < 3)
		return invalidUrl;

	Url result;
	auto itor = splitedUrl.cbegin();
	auto end = splitedUrl.cend();

	if (!ParseScheme(result, itor, end))
		return invalidUrl;

	// '/' 두개 넘기기
	++itor;
	++itor;

	if (!ParseAutority(result, itor, end))
		return invalidUrl;

	ParsePath(result, ++itor, end);
	ParseQuery(result, end - 1, end);
	ParseFragment(result, end - 1, end);

	return result;
}

bool Url::IsValidScheme( const std::string& scheme_ )
{
	return !StringUtil::Trim(scheme_).empty();
}

bool Url::IsValidPortNumber( int portNumber_ )
{
	return 0 <= portNumber_ && portNumber_ <= std::numeric_limits<uint16_t>::max();
}


bool Url::ParseScheme( Url& result, SplitedString::const_iterator itor
					  , const SplitedString::const_iterator& end )
{
	if (itor == end)
		return false;

	std::string::size_type position = itor->rfind(':');

	if (position == std::string::npos)
		return false;

	result.mScheme = itor->substr(0, position);
	result.mScheme = StringUtil::ToLower(StringUtil::Trim(result.mScheme));

	return IsValidScheme(result.mScheme);
}


bool Url::ParseAutority( Url& result, SplitedString::const_iterator itor
						, const SplitedString::const_iterator& end )
{
	if (itor == end)
		return false;

	std::vector<std::string> autorityList = StringUtil::Split(*itor, "@", false);
	auto autorityItor = autorityList.begin();

	//autority = user_info@host_name:port
	//user_info는 생략 될 수 있다.
	//port는 생략 될 수 있다.
	switch(autorityList.size())
	{
	case 2:
		{
			auto userInfo = StringUtil::Split(*autorityItor, ":", false);

			if (userInfo.size() < 2)
				return false;

			result.mUserName = StringUtil::Trim(userInfo[0]);
			result.mPassword = StringUtil::Trim(userInfo[1]);

			++autorityItor;
		}

	case 1:
		{
			auto hostNamePort = StringUtil::Split(*autorityItor, ":", false);

			switch(hostNamePort.size())
			{
			case 2:
				{
					std::stringstream stream;

					stream << hostNamePort[1];
					stream >> result.mPortNumber;

					if (stream.fail() || stream.bad() || !stream.eof())
					{
						//문자열을 숫자로 변환 실패
						return false;
					}

					if (!IsValidPortNumber(result.mPortNumber))
					{
						return false;                
					}
				}
			case 1:
				{
					if (hostNamePort[0].find("[") != std::string::npos)
					{
						result.mHostName 
							= StringUtil::SubString(hostNamePort[0], "[", "]");
					}
					else
					{
						result.mHostName = StringUtil::Trim(hostNamePort[0]);
					}
				}
				break;

			default:
				return false;
			}
		}
		break;

	default:
		return false;
	}    

	return true;
}

bool Url::ParsePath( Url& result, SplitedString::const_iterator itor
					, const SplitedString::const_iterator& end )
{
	if (itor == end)
		return false;

	auto& resultPath = result.mPath;
	resultPath.assign(itor, end);

	std::string& lastNode = *resultPath.rbegin();
	std::string::size_type position = lastNode.find_first_of("?#");

	if (position != std::string::npos)
	{
		lastNode = lastNode.substr(0, position);
	}

	//Path의 Node를 트림
	for (auto nodeItor = resultPath.begin(), endNode = resultPath.end()
		; nodeItor != endNode; ++nodeItor)
	{
		*nodeItor = StringUtil::Trim(*nodeItor);
	}       

	return true;
}

bool Url::ParseQuery( Url& result, SplitedString::const_iterator itor
					 , const SplitedString::const_iterator& end )
{
	if (itor == end)
		return false;

	std::string::size_type position = itor->find('?');

	if (position == std::string::npos)
		return false;

	auto queryList = StringUtil::Split(itor->substr(position + 1), "&");

	for (std::vector<std::string>::const_iterator queryItor = queryList.cbegin()
		, end = queryList.cend() - 1; queryItor != end; ++queryItor)
	{
		auto query = *queryItor; 
		position = query.find('=');

		if(position != std::string::npos)
			return false;

		result.mQuery.insert(std::make_pair(
			StringUtil::Trim(query.substr(0, position))
			, StringUtil::Trim(query.substr(position + 1))));
	}

	std::string lastQuery = *queryList.rbegin();
	position = lastQuery.find('#');

	if (position != std::string::npos)
	{
		lastQuery = lastQuery.substr(0, position);
	}

	position = lastQuery.find('=');
	if(position != std::string::npos)
		return false;

	result.mQuery.insert(
		std::make_pair(StringUtil::Trim(lastQuery.substr(0, position))
		, StringUtil::Trim(lastQuery.substr(position + 1))));

	return true;
}

bool Url::ParseFragment( Url& result, SplitedString::const_iterator itor
						, const SplitedString::const_iterator& end )
{
	if (itor == end)
		return false;

	std::string::size_type position = itor->find('#');

	if (position == std::string::npos)
		return false;

	result.mFragment = StringUtil::Trim(itor->substr(position + 1));

	return true;
}

