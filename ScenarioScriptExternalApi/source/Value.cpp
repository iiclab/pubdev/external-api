#include "Scenario/ExternalApi/Value.h"

#include <sstream>
#include <unordered_map>
#include <algorithm>
#include <limits>
#include <cassert>

#include "Scenario/ExternalApi/TypeManager.h"
#include "Scenario/ExternalApi/Structure.h"
#include "Scenario/Util/StringUtil.h"
#include "Scenario/ExternalApi/ParameterConverter.h"
#include "Scenario/ExternalApi/ValueImpl.h"

using namespace scenario;

Value::Value()
	: mpValueImpl(new ValueData<void>())
{
}

Value::Value(Type type_) 
{
	Clear(type_);
}

Value::Value( int8_t value )
	: mpValueImpl(new ValueData<int8_t>(value))
{
}

Value::Value(uint8_t value)
	: mpValueImpl(new ValueData<uint8_t>(value))
{
}

Value::Value(int16_t value)
	: mpValueImpl(new ValueData<int16_t>(value))
{
}

Value::Value(uint16_t value)
	: mpValueImpl(new ValueData<uint16_t>(value))
{
}

Value::Value(int32_t value)
	: mpValueImpl(new ValueData<int32_t>(value))
{
}

Value::Value(uint32_t value)
	: mpValueImpl(new ValueData<uint32_t>(value))
{
}

Value::Value(int64_t value)
	: mpValueImpl(new ValueData<int64_t>(value))
{
}

Value::Value(uint64_t value)
	: mpValueImpl(new ValueData<uint64_t>(value))
{
}

Value::Value(float value)
	: mpValueImpl(new ValueData<float>(value))
{
}

Value::Value(double value)
	: mpValueImpl(new ValueData<double>(value))
{
}

Value::Value(bool value)
	: mpValueImpl(new ValueData<bool>(value))
{
}

Value::Value(const std::string& str)
	: mpValueImpl(new ValueData<std::string>(str))
{
}

Value::Value(const char* str)
	: mpValueImpl(new ValueData<std::string>(str))
{
}

Value::Value(const scenario::Array& array)
	: mpValueImpl(new ValueData<scenario::Array>(array))
{
}

Value::Value(const Structure& structure) 
	: mpValueImpl(new ValueData<Structure>(structure))
{
}

Value::Value( const Value& value )
	: mpValueImpl(value.mpValueImpl->Clone())
{
}

Value::~Value()
{
}

Value& Value::operator=( const Value& value )
{
	if(this == &value)
		return *this;

	mpValueImpl.reset(value.mpValueImpl->Clone());

	return *this;
}

Value& Value::operator=( Value&& value )
{
	mpValueImpl.swap(value.mpValueImpl);
	return *this;
}

bool Value::operator==( const Value& rhs ) const
{
	if(this == &rhs)
		return true;

	return *mpValueImpl == *rhs.mpValueImpl;
}

bool Value::operator!=( const Value& rhs ) const
{
	return !operator==(rhs);
}

void Value::Clear()
{
	Clear(GetType());
}

void Value::Clear( Type type )
{
	switch (type)
	{
	case Value::Int8:
		mpValueImpl.reset(new ValueData<int8_t>());
		break;
	case Value::Uint8:
		mpValueImpl.reset(new ValueData<uint8_t>());
		break;
	case Value::Int16:
		mpValueImpl.reset(new ValueData<int16_t>());
		break;
	case Value::Uint16:
		mpValueImpl.reset(new ValueData<uint16_t>());
		break;
	case Value::Int32:
		mpValueImpl.reset(new ValueData<int32_t>());
		break;
	case Value::Uint32:
		mpValueImpl.reset(new ValueData<uint32_t>());
		break;
	case Value::Int64:
		mpValueImpl.reset(new ValueData<int64_t>());
		break;
	case Value::Uint64:
		mpValueImpl.reset(new ValueData<uint64_t>());
		break;
	case Value::Float32:
		mpValueImpl.reset(new ValueData<float>());
		break;
	case Value::Float64:
		mpValueImpl.reset(new ValueData<double>());
		break;
	case Value::Bool:
		mpValueImpl.reset(new ValueData<bool>());
		break;
	case Value::String:
		mpValueImpl.reset(new ValueData<std::string>());
		break;
	case Value::Array:
		mpValueImpl.reset(new ValueData<scenario::Array>());
		break;
	case Value::Struct:
		mpValueImpl.reset(new ValueData<Structure>());
		break;
	case Value::Void:
	default:
		mpValueImpl.reset(new ValueData<void>());
		break;
	}
}

size_t Value::GetByteSize( Type type_ )
{
	Value value(type_);
	return value.GetByteSize();
}

size_t Value::GetByteSize() const
{
	return mpValueImpl->GetByteSize();
}

Value& Value::ConvertTo( Type type_ )
{
	if (GetType() == type_)
		return *this;

	switch(type_)
	{
	case Int32: return *this = ParameterConverter<Int32>(*this); 
	case Uint32: return *this = ParameterConverter<Uint32>(*this); 
	case Float32: return *this = ParameterConverter<Float32>(*this); 
	case Float64: return *this = ParameterConverter<Float64>(*this); 
	case String: return *this = ParameterConverter<String>(*this); 
	case Int8: return *this = ParameterConverter<Int8>(*this); 
	case Uint8: return *this = ParameterConverter<Uint8>(*this); 
	case Bool: return *this = ParameterConverter<Bool>(*this); 
	case Int16: return *this = ParameterConverter<Int16>(*this); 
	case Uint16: return *this = ParameterConverter<Uint16>(*this); 
	case Int64: return *this = ParameterConverter<Int64>(*this); 
	case Uint64: return *this = ParameterConverter<Uint64>(*this); 
	case Array: return *this = ParameterConverter<Array>(*this);
	case Struct: return *this = ParameterConverter<Struct>(*this);
	case Void: return *this = Value(Void);
	default:
		assert(!"Not supported type");
	}

	return *this;
}

Value& Value::ConvertTo( const Value& value )
{
	if (this == &value)
		return *this;

	Value::Type type = value.GetType();

	if (GetType() == type && (type != Struct && type != Array))
		return *this;

	ConvertTo(type);

	if (type == Struct)
	{
		Structure& structure = AsStructure();
		const Structure& originStructure = value.AsStructure();

		structure.GetName() = originStructure.GetName();

		auto itor = structure.begin();
		auto originItor = originStructure.begin();
		for (; originItor != originStructure.end(); ++originItor)
		{
			if (itor != structure.end())
			{
				itor->first = originItor->first;
				itor->second.ConvertTo(originItor->second);

				++itor;
			}
			else
			{
				structure.push_back(*originItor);
			}				
		}

		structure.resize(originStructure.size());
	}
	else if(type == Array)
	{
		scenario::Array& array = AsArray();
		array.Normalize(value.AsArray().GetDefaultType());
	}

	return *this;
}

std::string Value::ToString() const
{
	return Value(*this).ConvertTo(String).AsString();
}

Value::Type Value::GetType() const
{
	return mpValueImpl->GetType();
}

int8_t const& Value::AsInt8() const throw(TypeNotMatchedException)
{
	return mpValueImpl->GetData<int8_t>();
}

uint8_t const& Value::AsUint8() const throw(TypeNotMatchedException)
{
	return mpValueImpl->GetData<uint8_t>();
}

int16_t const& Value::AsInt16() const throw(TypeNotMatchedException)
{
	return mpValueImpl->GetData<int16_t>();
}

uint16_t const& Value::AsUint16() const throw(TypeNotMatchedException)
{
	return mpValueImpl->GetData<uint16_t>();
}

int32_t const& Value::AsInt32() const throw(TypeNotMatchedException)
{
	return mpValueImpl->GetData<int32_t>();
}

uint32_t const& Value::AsUint32() const throw(TypeNotMatchedException)
{
	return mpValueImpl->GetData<uint32_t>();
}

int64_t const& Value::AsInt64() const throw(TypeNotMatchedException)
{
	return mpValueImpl->GetData<int64_t>();
}

uint64_t const& Value::AsUint64() const throw(TypeNotMatchedException)
{
	return mpValueImpl->GetData<uint64_t>();
}

float const& Value::AsFloat32() const throw(TypeNotMatchedException)
{
	return mpValueImpl->GetData<float>();
}

double const& Value::AsFloat64() const throw(TypeNotMatchedException)
{
	return mpValueImpl->GetData<double>();
}

bool const& Value::AsBool() const throw(TypeNotMatchedException)
{
	return mpValueImpl->GetData<bool>();
}

std::string const& Value::AsString() const throw(TypeNotMatchedException)
{
	return mpValueImpl->GetData<std::string>();
}

scenario::Array const& Value::AsArray() const throw(TypeNotMatchedException)
{
	return mpValueImpl->GetData<scenario::Array>();
}

Structure const& Value::AsStructure() const throw(TypeNotMatchedException)
{
	return mpValueImpl->GetData<Structure>();
}

int8_t& Value::AsInt8() throw(TypeNotMatchedException)
{
	return mpValueImpl->GetData<int8_t>();
}

uint8_t& Value::AsUint8() throw(TypeNotMatchedException)
{
	return mpValueImpl->GetData<uint8_t>();
}

int16_t& Value::AsInt16() throw(TypeNotMatchedException)
{
	return mpValueImpl->GetData<int16_t>();
}

uint16_t& Value::AsUint16() throw(TypeNotMatchedException)
{
	return mpValueImpl->GetData<uint16_t>();
}

int32_t& Value::AsInt32() throw(TypeNotMatchedException)
{
	return mpValueImpl->GetData<int32_t>();
}

uint32_t& Value::AsUint32() throw(TypeNotMatchedException)
{
	return mpValueImpl->GetData<uint32_t>();
}

int64_t& Value::AsInt64() throw(TypeNotMatchedException)
{
	return mpValueImpl->GetData<int64_t>();
}

uint64_t& Value::AsUint64() throw(TypeNotMatchedException)
{
	return mpValueImpl->GetData<uint64_t>();
}

float& Value::AsFloat32() throw(TypeNotMatchedException)
{
	return mpValueImpl->GetData<float>();
}

double& Value::AsFloat64() throw(TypeNotMatchedException)
{
	return mpValueImpl->GetData<double>();
}

bool& Value::AsBool() throw(TypeNotMatchedException)
{
	return mpValueImpl->GetData<bool>();
}

std::string& Value::AsString() throw(TypeNotMatchedException)
{
	return mpValueImpl->GetData<std::string>();
}

scenario::Array& Value::AsArray() throw(TypeNotMatchedException)
{
	return mpValueImpl->GetData<scenario::Array>();
}

Structure& Value::AsStructure() throw(TypeNotMatchedException)
{
	return mpValueImpl->GetData<Structure>();
}