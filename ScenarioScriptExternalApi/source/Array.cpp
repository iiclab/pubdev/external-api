#include "Scenario/ExternalApi/Array.h"

#include <algorithm>

using namespace scenario;

void Array::SetDefaultType( const Value& type )
{	
	mDefaultType = type;
	Normalize();
}

void Array::Normalize()
{
	for (auto itor = begin(); itor != end(); ++itor)
		itor->ConvertTo(mDefaultType);
}

void Array::Normalize( const Value& type )
{
	mDefaultType = type;
	Normalize();
}

bool Array::operator==( Array const& rhs ) const
{
	if (this == &rhs)
		return true;	

	if (size() != rhs.size())
		return false;

	if (mDefaultType != rhs.mDefaultType)
		return false;

	return std::equal(begin(), end(), rhs.begin()
	, [](Value const& lhs, Value const& rhs) -> bool
	{
		return lhs == rhs;
	});
}