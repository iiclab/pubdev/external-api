#include "Scenario/ExternalApi/Structure.h"

#include <algorithm>

using namespace scenario;

Structure::Structure( const std::string& name /*= ""*/ )
	: mName(name)
{
}

Structure::Structure( const std::string& name
					 , const std::vector<std::string>& filedNames )
					 : mName(name)
{
	for (auto itor = filedNames.begin(), end = filedNames.end()
		; itor != end; ++itor)
	{
		push_back(std::make_pair(*itor, Value()));
	}
}

Structure::Structure( const std::string& name
					 , const std::vector<std::pair<std::string, Value>>& fields )
					 : mName(name), Super(fields.begin(), fields.end())
{
}

Structure::Super::value_type& Structure::operator[](size_t i)
{
	auto itor = begin();
	for (size_t index = 0; index < i; index++)
		++itor;
	return *itor;
}

const Structure::Super::value_type& Structure::operator[](size_t i) const
{
	auto itor = begin();
	for (size_t index = 0; index < i; index++)
		++itor;
	return *itor;
}

bool Structure::GetField( Value& output, const std::string& fieldName ) const
{
	auto itor = std::find_if(begin(), end()
		, [&fieldName](const std::pair<std::string, Value>& pair)
	{
		return pair.first == fieldName;
	});

	if (itor == end())
		return false;

	output = itor->second;
	return true;
}

Value& Structure::GetField( const std::string& fieldName )
{	
	auto itor = std::find_if(begin(), end()
		, [&fieldName](const std::pair<std::string, Value>& pair)
	{
		return pair.first == fieldName;
	});

	if (itor == end())
	{
		push_back(std::make_pair(fieldName, Value()));
		itor = --end();
	}

	return itor->second;
}

const Value& Structure::GetField( const std::string& fieldName ) const
{
	auto itor = std::find_if(begin(), end()
		, [&fieldName](const std::pair<std::string, Value>& pair)
	{
		return pair.first == fieldName;
	});

	if (itor == end())
	{
		assert(!"Can't find field");
	}

	return itor->second;
}

void Structure::SetField( std::string const& fieldName, Value const& value )
{
	auto itor = std::find_if(begin(), end()
		, [&fieldName](const std::pair<std::string, Value>& pair)
	{
		return pair.first == fieldName;
	});

	if (itor == end())
	{
		push_back(std::make_pair(fieldName, value));
	}
	else
	{
		itor->second = value;
	}
}

bool Structure::AddField( const std::string& fieldName
						 , const Value& value /*= Value()*/ )
{
	auto itor = std::find_if(begin(), end()
		, [&fieldName](const std::pair<std::string, Value>& pair)
	{
		return pair.first == fieldName;
	});

	if (itor != end())
		return false;

	push_back(std::make_pair(fieldName, value));	
	return true;
}

void Structure::RemoveField( const std::string& fieldName )
{
	remove_if(
		[&fieldName](const std::pair<std::string, Value>& pair)
	{
		return pair.first == fieldName;
	});
}

bool Structure::operator==( Structure const& rhs ) const
{
	if (this == &rhs)
		return true;

	if (mName != rhs.mName)
		return false;
	
	if (size() != rhs.size())
		return false;

	return std::equal(begin(), end(), rhs.begin()
	, [](std::pair<std::string, Value> const& lhs
			, std::pair<std::string, Value> const& rhs) -> bool
	{
		if(lhs.first != rhs.first)
			return false;

		if (lhs.second != rhs.second)
			return false;

		return true;
	});
}