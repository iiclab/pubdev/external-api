#ifndef __SCENARIO_EXTERNAL_API_PARAMETER_CONVERTER_H__
#define __SCENARIO_EXTERNAL_API_PARAMETER_CONVERTER_H__

#include <cassert>

#include "Scenario/ExternalApi/Value.h"

namespace scenario
{
	template<size_t destinationType>
	Value ParameterConverter(const Value& value)
	{
		switch(value.GetType())
		{
		case Value::Int32:
			return ParameterConverterImpl<destinationType>::Convert(value.AsInt32()); 
		case Value::Uint32: 
			return ParameterConverterImpl<destinationType>::Convert(value.AsUint32()); 
		case Value::Float32:
			return ParameterConverterImpl<destinationType>::Convert(value.AsFloat32()); 
		case Value::Float64:
			return ParameterConverterImpl<destinationType>::Convert(value.AsFloat64()); 
		case Value::String:
			return ParameterConverterImpl<destinationType>::Convert(value.AsString()); 
		case Value::Int8: 
			return ParameterConverterImpl<destinationType>::Convert(value.AsInt8()); 
		case Value::Uint8:
			return ParameterConverterImpl<destinationType>::Convert(value.AsUint8()); 
		case Value::Bool: 
			return ParameterConverterImpl<destinationType>::Convert(value.AsBool()); 
		case Value::Int16:
			return ParameterConverterImpl<destinationType>::Convert(value.AsInt16()); 
		case Value::Uint16:
			return ParameterConverterImpl<destinationType>::Convert(value.AsUint16()); 
		case Value::Int64: 
			return ParameterConverterImpl<destinationType>::Convert(value.AsInt64()); 
		case Value::Uint64: 
			return ParameterConverterImpl<destinationType>::Convert(value.AsUint64());
		case Value::Array:
			return ParameterConverterImpl<destinationType>::Convert(value.AsArray());
		case Value::Struct:
			return ParameterConverterImpl<destinationType>::Convert(value.AsStructure());
		case Value::Void:
			return Value(static_cast<Value::Type>(destinationType));
		default:
			assert(!"Not supported type");
		}
		return Value();
	}

	template<size_t destinationType>
	struct ParameterConverterImpl
	{	
		template<typename ValueType>
		static Value Convert(ValueType const& input);
	};
} // namespace scenario

#include "Scenario/ExternalApi/ParameterConverterImpl.h"

#endif