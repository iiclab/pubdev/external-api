#ifndef __SCENARIO_EXTERNAL_API_LEGACY_EXTERNAL_API_H__
#define __SCENARIO_EXTERNAL_API_LEGACY_EXTERNAL_API_H__

#include "Scenario/ExternalApi/ExternalApi.h"
#include "Scenario/ExternalApi/Url.h"

#include <unordered_map>

namespace scenario
{
	class FunctionCaller;
	class DynamicLibrary;

	class LegacyExternalApi : public ExternalApi
	{
	public:
		LegacyExternalApi(const Url& url);
		~LegacyExternalApi();

	public:
		virtual bool Load(const FunctionType& functionType);
		virtual bool Call(const std::string& functionName, Value& output, const std::vector<Value>& input);

		inline bool IsValid()
		{
			return mpLibrary != nullptr;
		}

	private:
		std::unordered_map<std::string, FunctionCaller*> mFunctionMap;
		DynamicLibrary* mpLibrary;
	};
} // namespace scenario

#endif
