#ifndef __SCENARIO_EXTERNAL_API_DYNAMIC_LIBRARY_LOADER_H__
#define __SCENARIO_EXTERNAL_API_DYNAMIC_LIBRARY_LOADER_H__

#include <string>

#include "Scenario/Util/Noncopyable.h"

namespace scenario
{
	typedef void* LibraryHandle;

	class DynamicLibrary : private Noncopyable
	{
	public:
		DynamicLibrary(LibraryHandle hLibrary);
		~DynamicLibrary();

	public:
		template<typename Func>
		inline Func GetFunction(const std::string& name)
		{
			return reinterpret_cast<Func>(GetRawFunction(name));
		}

	private:
		void* GetRawFunction(const std::string& name);

	public:
		LibraryHandle mhLibrary;
	};

	class DynamicLibraryLoader : private Noncopyable
	{
	private:
		DynamicLibraryLoader();

	public:
		static DynamicLibrary* Load(const std::string& name);
	};
} // namespace scenario

#endif