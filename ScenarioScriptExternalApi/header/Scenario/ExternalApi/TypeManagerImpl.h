#ifndef __SCENARIO_EXTERNAL_API_TYPE_MANAGER_IMPL_H__
#define __SCENARIO_EXTERNAL_API_TYPE_MANAGER_IMPL_H__

#include <unordered_map>
#include <string>
#include <memory>

namespace scenario
{
	class Value;

	class TypeManagerImpl
	{
	public:
		typedef std::unordered_map<std::string, Value> TypeMap;

	public:
		TypeManagerImpl();

	public:
		bool RegisterType(const std::string& typeName, const Value& value);
		void UnregisterType(const std::string& typeName);
		bool GetType(Value& output, const std::string& typeName);

	private:	
		TypeMap mTypeMap;
	};
} // namespace scenario

#endif