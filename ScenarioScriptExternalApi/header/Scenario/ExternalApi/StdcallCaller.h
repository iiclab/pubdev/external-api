#ifndef __SCENARIO_EXTERNAL_API_STDCALL_CALLER_H__
#define __SCENARIO_EXTERNAL_API_STDCALL_CALLER_H__

#include <queue>
#include <array>
#include <stdint.h>
#include <memory>

#include "Scenario/ExternalApi/ExternalApi.h"
#include "FunctionCaller.h"

namespace scenario
{
	class StdcallCaller : public FunctionCaller
	{
	public:
		typedef uint32_t StackElement;

	public:
		StdcallCaller(void* pFunction, const FunctionType& funtionType);
		virtual ~StdcallCaller();

	public:
		virtual Value Call(const std::vector<Value>& paramList);

	private:
		void Call(std::array<StackElement, 2>& rawReturnValue, std::deque<StackElement>& stack);

	private:
		void* const mpFunction;
		const FunctionType mFunctionType;
	};
} // namespace scenario

#endif