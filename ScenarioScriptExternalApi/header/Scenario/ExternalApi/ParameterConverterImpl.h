#ifndef __SCENARIO_EXTERNAL_API_PARAMETER_CONVERTER_IMPL_H__
#define __SCENARIO_EXTERNAL_API_PARAMETER_CONVERTER_IMPL_H__

#include <cassert>
#include <climits>
#include <sstream>

#include "Scenario/ExternalApi/Value.h"
#include "Scenario/ExternalApi/Array.h"
#include "Scenario/ExternalApi/ParameterConverter.h"

#define PARAMETER_CONVERTER_STRING_TYPE_SERIES_SPECIALIZER() \
	template<> \
	static Value Convert(const char* const& str) \
	{ \
	return Convert<std::string>(str); \
	} \
	template<> \
	static Value Convert(char* const& str) \
	{ \
	return Convert<std::string>(str); \
	} \
	template<size_t size> \
	static Value Convert(const char (&str)[size]) \
	{ \
	return Convert<std::string>(str); \
	} \
	template<size_t size> \
	static Value Convert(char (&str)[size]) \
	{ \
	return Convert<std::string>(str); \
	}

#define PARAMETER_CONVERTER_ARRAY_TYPE_SPECIALIZER() \
	template<> \
	static Value Convert(Array const& input) \
	{ \
	if(input.size() == 0)\
	return Value(OriginalType()); \
	return Value(input[0]).ConvertTo(Value(OriginalType()).GetType()); \
	}

#define PARAMETER_CONVERTER_STRUCT_TYPE_SPECIALIZER() \
	template<> \
	static Value Convert(Structure const& input) \
	{ \
	assert(!"Not supported type"); \
	return Value(); \
	}

namespace scenario
{    
	template<typename T1, typename T2, typename T3>
	static inline bool IsBoundaryInclude(T1 value, T2 min, T3 max)
	{
		return min <= value && value <= max;
	}

	template<>
	struct ParameterConverterImpl<Value::Int8>
	{
		typedef int8_t OriginalType;

		template<typename ValueType>
		static Value Convert(ValueType const& input)
		{
			if (!IsBoundaryInclude(input
				, std::numeric_limits<OriginalType>::min()
				, std::numeric_limits<OriginalType>::max()
				))
			{
				assert(!"Not supported type");
			}

			return Value(static_cast<OriginalType>(input));
		}

		template<>
		static Value Convert(std::string const& str)
		{
			std::stringstream stringStream;
			OriginalType value;

			stringStream << str;
			stringStream >> value;

			return Value(value);            
		}    

		PARAMETER_CONVERTER_STRING_TYPE_SERIES_SPECIALIZER();
		PARAMETER_CONVERTER_ARRAY_TYPE_SPECIALIZER();
		PARAMETER_CONVERTER_STRUCT_TYPE_SPECIALIZER();
	};

	template<>
	struct ParameterConverterImpl<Value::Uint8>
	{
		typedef uint8_t OriginalType;

		template<typename ValueType>
		static Value Convert(ValueType const& input)
		{
			if (!IsBoundaryInclude(input
				, std::numeric_limits<OriginalType>::min()
				, std::numeric_limits<OriginalType>::max()
				))
			{
				assert(!"Not supported type");
			}

			return Value(static_cast<OriginalType>(input));
		}

		template<>
		static Value Convert(std::string const& str)
		{
			std::stringstream stringStream;
			OriginalType value;

			stringStream << str;
			stringStream >> value;

			return Value(value);            
		}    

		PARAMETER_CONVERTER_STRING_TYPE_SERIES_SPECIALIZER();
		PARAMETER_CONVERTER_ARRAY_TYPE_SPECIALIZER();
		PARAMETER_CONVERTER_STRUCT_TYPE_SPECIALIZER();
	};

	template<>
	struct ParameterConverterImpl<Value::Int16>
	{
		typedef int16_t OriginalType;

		template<typename ValueType>
		static Value Convert(ValueType const& input)
		{
			if (!IsBoundaryInclude(input
				, std::numeric_limits<OriginalType>::min()
				, std::numeric_limits<OriginalType>::max()
				))
			{
				assert(!"Not supported type");
			}

			return Value(static_cast<OriginalType>(input));
		}

		template<>
		static Value Convert(std::string const& str)
		{
			std::stringstream stringStream;
			OriginalType value;

			stringStream << str;
			stringStream >> value;

			return Value(value);            
		}  

		PARAMETER_CONVERTER_STRING_TYPE_SERIES_SPECIALIZER();
		PARAMETER_CONVERTER_ARRAY_TYPE_SPECIALIZER();
		PARAMETER_CONVERTER_STRUCT_TYPE_SPECIALIZER();
	};

	template<>
	struct ParameterConverterImpl<Value::Uint16>
	{
		typedef uint16_t OriginalType;

		template<typename ValueType>
		static Value Convert(ValueType const& input)
		{
			if (!IsBoundaryInclude(input
				, std::numeric_limits<OriginalType>::min()
				, std::numeric_limits<OriginalType>::max()
				))
			{
				assert(!"Not supported type");
			}

			return Value(static_cast<OriginalType>(input));
		}

		template<>
		static Value Convert(std::string const& str)
		{
			std::stringstream stringStream;
			OriginalType value;

			stringStream << str;
			stringStream >> value;

			return Value(value);            
		}

		PARAMETER_CONVERTER_STRING_TYPE_SERIES_SPECIALIZER();
		PARAMETER_CONVERTER_ARRAY_TYPE_SPECIALIZER();
		PARAMETER_CONVERTER_STRUCT_TYPE_SPECIALIZER();
	};

	template<>
	struct ParameterConverterImpl<Value::Int32>
	{
		typedef int32_t OriginalType;

		template<typename ValueType>
		static Value Convert(ValueType const& input)
		{
			if (!IsBoundaryInclude(input
				, std::numeric_limits<OriginalType>::min()
				, std::numeric_limits<OriginalType>::max()
				))
			{
				assert(!"Not supported type");
			}

			return Value(static_cast<OriginalType>(input));
		}

		template<>
		static Value Convert(std::string const& str)
		{
			std::stringstream stringStream;
			OriginalType value;

			stringStream << str;
			stringStream >> value;

			return Value(value);            
		}    

		PARAMETER_CONVERTER_STRING_TYPE_SERIES_SPECIALIZER();
		PARAMETER_CONVERTER_ARRAY_TYPE_SPECIALIZER();
		PARAMETER_CONVERTER_STRUCT_TYPE_SPECIALIZER();
	};

	template<>
	struct ParameterConverterImpl<Value::Uint32>
	{
		typedef uint32_t OriginalType;

		template<typename ValueType>
		static Value Convert(ValueType const& input)
		{
			if (!IsBoundaryInclude(input
				, std::numeric_limits<OriginalType>::min()
				, std::numeric_limits<OriginalType>::max()
				))
			{
				assert(!"Not supported type");
			}

			return Value(static_cast<OriginalType>(input));
		}

		template<>
		static Value Convert(std::string const& str)
		{
			std::stringstream stringStream;
			OriginalType value;

			stringStream << str;
			stringStream >> value;

			return Value(value);            
		}    

		PARAMETER_CONVERTER_STRING_TYPE_SERIES_SPECIALIZER();
		PARAMETER_CONVERTER_ARRAY_TYPE_SPECIALIZER();
		PARAMETER_CONVERTER_STRUCT_TYPE_SPECIALIZER();
	};

	template<>
	struct ParameterConverterImpl<Value::Int64>
	{
		typedef int64_t OriginalType;

		template<typename ValueType>
		static Value Convert(ValueType const& input)
		{
			if (!IsBoundaryInclude(input
				, std::numeric_limits<OriginalType>::min()
				, std::numeric_limits<OriginalType>::max()
				))
			{
				assert(!"Not supported type");
			}

			return Value(static_cast<OriginalType>(input));
		}

		template<>
		static Value Convert(std::string const& str)
		{
			std::stringstream stringStream;
			OriginalType value;

			stringStream << str;
			stringStream >> value;

			return Value(value);            
		}

		PARAMETER_CONVERTER_STRING_TYPE_SERIES_SPECIALIZER();
		PARAMETER_CONVERTER_ARRAY_TYPE_SPECIALIZER();
		PARAMETER_CONVERTER_STRUCT_TYPE_SPECIALIZER();
	};

	template<>
	struct ParameterConverterImpl<Value::Uint64>
	{
		typedef uint64_t OriginalType;

		template<typename ValueType>
		static Value Convert(ValueType const& input)
		{
			if (!IsBoundaryInclude(input
				, std::numeric_limits<OriginalType>::min()
				, std::numeric_limits<OriginalType>::max()
				))
			{
				assert(!"Not supported type");
			}

			return Value(static_cast<OriginalType>(input));
		}

		template<>
		static Value Convert(std::string const& str)
		{
			std::stringstream stringStream;
			OriginalType value;

			stringStream << str;
			stringStream >> value;

			return Value(value);            
		}    

		PARAMETER_CONVERTER_STRING_TYPE_SERIES_SPECIALIZER();
		PARAMETER_CONVERTER_ARRAY_TYPE_SPECIALIZER();
		PARAMETER_CONVERTER_STRUCT_TYPE_SPECIALIZER();
	};

	template<>
	struct ParameterConverterImpl<Value::Float32>
	{
		typedef float OriginalType;

		template<typename ValueType>
		static Value Convert(ValueType const& input)
		{
			return Value(static_cast<OriginalType>(input));
		}

		template<>
		static Value Convert(std::string const& str)
		{
			std::stringstream stringStream;
			OriginalType value;

			stringStream << str;
			stringStream >> value;

			return Value(value);            
		}    

		PARAMETER_CONVERTER_STRING_TYPE_SERIES_SPECIALIZER();
		PARAMETER_CONVERTER_ARRAY_TYPE_SPECIALIZER();
		PARAMETER_CONVERTER_STRUCT_TYPE_SPECIALIZER();
	};

	template<>
	struct ParameterConverterImpl<Value::Float64>
	{
		typedef double OriginalType;

		template<typename ValueType>
		static Value Convert(ValueType const& input)
		{
			return Value(static_cast<OriginalType>(input));
		}

		template<>
		static Value Convert(std::string const& str)
		{
			std::stringstream stringStream;
			OriginalType value;

			stringStream << str;
			stringStream >> value;

			return Value(value);            
		}

		PARAMETER_CONVERTER_STRING_TYPE_SERIES_SPECIALIZER();
		PARAMETER_CONVERTER_ARRAY_TYPE_SPECIALIZER();
		PARAMETER_CONVERTER_STRUCT_TYPE_SPECIALIZER();
	};

	template<>
	struct ParameterConverterImpl<Value::Bool>
	{
		typedef bool OriginalType;

		template<typename ValueType>
		static Value Convert(ValueType const& input)
		{
			if (input == 0)
			{
				return Value(false);
			}
			else 
			{
				return Value(true);
			}
		}

		template<>
		static Value Convert(std::string const& str)
		{
			std::stringstream stringStream;
			OriginalType value;

			stringStream << str;
			stringStream >> value;

			return Value(value);            
		}     

		PARAMETER_CONVERTER_STRING_TYPE_SERIES_SPECIALIZER();
		PARAMETER_CONVERTER_ARRAY_TYPE_SPECIALIZER();
		PARAMETER_CONVERTER_STRUCT_TYPE_SPECIALIZER();
	};

	template<>
	struct ParameterConverterImpl<Value::String>
	{
		typedef std::string OriginalType;

		template<typename ValueType>
		static Value Convert(ValueType const& input)
		{
			std::stringstream stringStream;
			OriginalType value;

			stringStream << input;
			value = stringStream.str();

			return Value(value);  
		}

		template<>
		static Value Convert(std::string const& str)
		{
			return Value(str);
		}

		PARAMETER_CONVERTER_STRING_TYPE_SERIES_SPECIALIZER();

		template<>
		static Value Convert(Array const& input)
		{
			if (input.size() == 0)
				return Value("");			

			std::stringstream stream;

			stream << input[0].ToString();
			for (auto itor = ++input.begin(); itor != input.end(); ++itor)
			{
				stream << ", " << itor->ToString();
			}		

			return Value(stream.str());
		}

		template<>
		static Value Convert(Structure const& input)
		{
			if (input.begin() == input.end())
				return Value("");

			std::stringstream stream;

			stream << input.begin()->first << "=" << input.begin()->second;
			for (auto itor = ++input.begin(); itor != input.end(); ++itor)
			{
				stream << ", " << itor->first << "=" << itor->second;
			}		

			return Value(stream.str());
		}
	};

	template<>
	struct ParameterConverterImpl<Value::Array>
	{
		typedef Array OriginalType;

		template<typename ValueType>
		static Value Convert(ValueType const& input)
		{
			return Value(Array(&input, 1));  
		}

		template<>
		static Value Convert(std::string const& str)
		{
			return Value(Array(&str, 1));
		}

		PARAMETER_CONVERTER_STRING_TYPE_SERIES_SPECIALIZER();

		template<>
		static Value Convert(OriginalType const& input)
		{
			return Value(Array(input.begin(), input.end()));
		}

		template<>
		static Value Convert(Structure const& input)
		{
			Value result(Value::Array);

			auto& array = result.AsArray();
			for (auto itor = input.begin(), end = input.end(); itor != end; ++itor)
			{
				array.push_back(itor->second);
			}
			return result;
		}
	};

	template<>
	struct ParameterConverterImpl<Value::Struct>
	{
		typedef Structure OriginalType;

		template<typename ValueType>
		static Value Convert(ValueType const& input)
		{
			Value result(Value::Struct);
			result.AsStructure().AddField("field1", Value(input));
			return result;
		}

		template<>
		static Value Convert(std::string const& str)
		{
			Value result(Value::Struct);
			result.AsStructure().AddField("field1", Value(str));
			return result;
		}

		PARAMETER_CONVERTER_STRING_TYPE_SERIES_SPECIALIZER();

		template<>
		static Value Convert(Array const& input)
		{
			Structure structure;

			std::stringstream stream;

			for (size_t i = 0, size = input.size(); i < size; i++)
			{
				stream.str("");
				stream << "field" << i;
				structure.AddField(stream.str(), input[i]);
			}

			return Value(structure);
		}

		template<>
		static Value Convert(OriginalType const& input)
		{
			return Value(input);
		}
	};
} // namesapce scenario

#undef PARAMETER_CONVERTER_STRING_TYPE_SERIES_SPECIALIZER
#undef PARAMETER_CONVERTER_ARRAY_TYPE_SPECIALIZER

#endif