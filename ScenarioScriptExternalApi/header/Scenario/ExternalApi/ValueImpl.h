#ifndef __SCENARIO_EXTERNAL_API_VALUE_IMPL_H__

#include "Scenario/ExternalApi/Value.h"

namespace scenario
{
	class ValueImpl
	{
	public:
		virtual ~ValueImpl() {}

	public:
		virtual bool operator==(ValueImpl const& rhs) const = 0;
		bool operator!=(ValueImpl const& rhs) const
		{
			return !operator==(rhs);
		}

	public:
		virtual ValueImpl* Clone() = 0;
		virtual size_t GetByteSize() const = 0;
		virtual Value::Type GetType() const = 0;
		virtual void* GetData() = 0;

		template<typename Type>
		Type& GetData()
		{
			if (GetType() != ValueData<Type>::GetType_())
				throw TypeNotMatchedException();
			
			return *static_cast<Type*>(GetData());
		}
	};
	
	template<typename DataType>
	class ValueData : public ValueImpl
	{
	};

	template<>
	class ValueData<void> : public ValueImpl
	{
	public:
		virtual bool operator==(ValueImpl const& rhs) const 
		{
			if (this == &rhs)
				return true;
			if(GetType() != rhs.GetType())
				return false;
			return true;
		}

	public:
		virtual ValueImpl* Clone() { return new ValueData<void>(); }
		virtual size_t GetByteSize() const { return 0; }
		virtual Value::Type GetType() const { return GetType_(); }
		virtual void* GetData() { return nullptr; }

		static Value::Type GetType_() { return Value::Void; }
	};

	template<>
	class ValueData<int8_t> : public ValueImpl
	{
	public:
		typedef int8_t Type;
		typedef Type const ConstType;
		typedef Type& ReferenceType;
		typedef Type const& ConstReferenceType;

	public:
		ValueData(ConstReferenceType data_ = Type()) : data(data_) {}

	public:
		virtual bool operator==(ValueImpl const& rhs) const 
		{
			if (this == &rhs)
				return true;
			if(GetType() != rhs.GetType())
				return false;
			return data == const_cast<ValueImpl&>(rhs).GetData<Type>();
		}

	public:
		virtual ValueImpl* Clone() { return new ValueData<Type>(data); }
		virtual size_t GetByteSize() const { return sizeof(Type); }
		virtual Value::Type GetType() const { return GetType_(); }
		virtual void* GetData() { return &data; };

		static Value::Type GetType_() { return Value::Int8; }

	private:
		Type data;
	};

	template<>
	class ValueData<uint8_t> : public ValueImpl
	{
	public:
		typedef uint8_t Type;
		typedef Type const ConstType;
		typedef Type& ReferenceType;
		typedef Type const& ConstReferenceType;

	public:
		ValueData(ConstReferenceType data_ = Type()) : data(data_) {}

	public:
		virtual bool operator==(ValueImpl const& rhs) const 
		{
			if (this == &rhs)
				return true;
			if(GetType() != rhs.GetType())
				return false;
			return data == const_cast<ValueImpl&>(rhs).GetData<Type>();
		}

	public:
		virtual ValueImpl* Clone() { return new ValueData<Type>(data); }
		virtual size_t GetByteSize() const { return sizeof(Type); }
		virtual Value::Type GetType() const { return GetType_(); }
		virtual void* GetData() { return &data; };

		static Value::Type GetType_() { return Value::Uint8; }

	private:
		Type data;
	};

	template<>
	class ValueData<int16_t> : public ValueImpl
	{
	public:
		typedef int16_t Type;
		typedef Type const ConstType;
		typedef Type& ReferenceType;
		typedef Type const& ConstReferenceType;

	public:
		ValueData(ConstReferenceType data_ = Type()) : data(data_) {}

	public:
		virtual bool operator==(ValueImpl const& rhs) const 
		{
			if (this == &rhs)
				return true;
			if(GetType() != rhs.GetType())
				return false;
			return data == const_cast<ValueImpl&>(rhs).GetData<Type>();
		}

	public:
		virtual ValueImpl* Clone() { return new ValueData<Type>(data); }
		virtual size_t GetByteSize() const { return sizeof(Type); }
		virtual Value::Type GetType() const { return GetType_(); }
		virtual void* GetData() { return &data; };

		static Value::Type GetType_() { return Value::Int16; }

	private:
		Type data;
	};

	template<>
	class ValueData<uint16_t> : public ValueImpl
	{
	public:
		typedef uint16_t Type;
		typedef Type const ConstType;
		typedef Type& ReferenceType;
		typedef Type const& ConstReferenceType;

	public:
		ValueData(ConstReferenceType data_ = Type()) : data(data_) {}

	public:
		virtual bool operator==(ValueImpl const& rhs) const 
		{
			if (this == &rhs)
				return true;
			if(GetType() != rhs.GetType())
				return false;
			return data == const_cast<ValueImpl&>(rhs).GetData<Type>();
		}

	public:
		virtual ValueImpl* Clone() { return new ValueData<Type>(data); }
		virtual size_t GetByteSize() const { return sizeof(Type); }
		virtual Value::Type GetType() const { return GetType_(); }
		virtual void* GetData() { return &data; };

		static Value::Type GetType_() { return Value::Uint16; }

	private:
		Type data;
	};

	template<>
	class ValueData<int32_t> : public ValueImpl
	{
	public:
		typedef int32_t Type;
		typedef Type const ConstType;
		typedef Type& ReferenceType;
		typedef Type const& ConstReferenceType;

	public:
		ValueData(ConstReferenceType data_ = Type()) : data(data_) {}

	public:
		virtual bool operator==(ValueImpl const& rhs) const 
		{
			if (this == &rhs)
				return true;
			if(GetType() != rhs.GetType())
				return false;
			return data == const_cast<ValueImpl&>(rhs).GetData<Type>();
		}

	public:
		virtual ValueImpl* Clone() { return new ValueData<Type>(data); }
		virtual size_t GetByteSize() const { return sizeof(Type); }
		virtual Value::Type GetType() const { return GetType_(); }
		virtual void* GetData() { return &data; };

		static Value::Type GetType_() { return Value::Int32; }

	private:
		Type data;
	};

	template<>
	class ValueData<uint32_t> : public ValueImpl
	{
	public:
		typedef uint32_t Type;
		typedef Type const ConstType;
		typedef Type& ReferenceType;
		typedef Type const& ConstReferenceType;

	public:
		ValueData(ConstReferenceType data_ = Type()) : data(data_) {}

	public:
		virtual bool operator==(ValueImpl const& rhs) const 
		{
			if (this == &rhs)
				return true;
			if(GetType() != rhs.GetType())
				return false;
			return data == const_cast<ValueImpl&>(rhs).GetData<Type>();
		}

	public:
		virtual ValueImpl* Clone() { return new ValueData<Type>(data); }
		virtual size_t GetByteSize() const { return sizeof(Type); }
		virtual Value::Type GetType() const { return GetType_(); }
		virtual void* GetData() { return &data; };

		static Value::Type GetType_() { return Value::Uint32; }

	private:
		Type data;
	};

	template<>
	class ValueData<int64_t> : public ValueImpl
	{
	public:
		typedef int64_t Type;
		typedef Type const ConstType;
		typedef Type& ReferenceType;
		typedef Type const& ConstReferenceType;

	public:
		ValueData(ConstReferenceType data_ = Type()) : data(data_) {}

	public:
		virtual bool operator==(ValueImpl const& rhs) const 
		{
			if (this == &rhs)
				return true;
			if(GetType() != rhs.GetType())
				return false;
			return data == const_cast<ValueImpl&>(rhs).GetData<Type>();
		}

	public:
		virtual ValueImpl* Clone() { return new ValueData<Type>(data); }
		virtual size_t GetByteSize() const { return sizeof(Type); }
		virtual Value::Type GetType() const { return GetType_(); }
		virtual void* GetData() { return &data; };

		static Value::Type GetType_() { return Value::Int64; }

	private:
		Type data;
	};

	template<>
	class ValueData<uint64_t> : public ValueImpl
	{
	public:
		typedef uint64_t Type;
		typedef Type const ConstType;
		typedef Type& ReferenceType;
		typedef Type const& ConstReferenceType;

	public:
		ValueData(ConstReferenceType data_ = Type()) : data(data_) {}

	public:
		virtual bool operator==(ValueImpl const& rhs) const 
		{
			if (this == &rhs)
				return true;
			if(GetType() != rhs.GetType())
				return false;
			return data == const_cast<ValueImpl&>(rhs).GetData<Type>();
		}

	public:
		virtual ValueImpl* Clone() { return new ValueData<Type>(data); }
		virtual size_t GetByteSize() const { return sizeof(Type); }
		virtual Value::Type GetType() const { return GetType_(); }
		virtual void* GetData() { return &data; };

		static Value::Type GetType_() { return Value::Uint64; }

	private:
		Type data;
	};

	template<>
	class ValueData<float> : public ValueImpl
	{
	public:
		typedef float Type;
		typedef Type const ConstType;
		typedef Type& ReferenceType;
		typedef Type const& ConstReferenceType;

	public:
		ValueData(ConstReferenceType data_ = Type()) : data(data_) {}

	public:
		virtual bool operator==(ValueImpl const& rhs) const 
		{
			if (this == &rhs)
				return true;
			if(GetType() != rhs.GetType())
				return false;
			return data == const_cast<ValueImpl&>(rhs).GetData<Type>();
		}

	public:
		virtual ValueImpl* Clone() { return new ValueData<Type>(data); }
		virtual size_t GetByteSize() const { return sizeof(Type); }
		virtual Value::Type GetType() const { return GetType_(); }
		virtual void* GetData() { return &data; };

		static Value::Type GetType_() { return Value::Float32; }

	private:
		Type data;
	};

	template<>
	class ValueData<double> : public ValueImpl
	{
	public:
		typedef double Type;
		typedef Type const ConstType;
		typedef Type& ReferenceType;
		typedef Type const& ConstReferenceType;

	public:
		ValueData(ConstReferenceType data_ = Type()) : data(data_) {}

	public:
		virtual bool operator==(ValueImpl const& rhs) const 
		{
			if (this == &rhs)
				return true;
			if(GetType() != rhs.GetType())
				return false;
			return data == const_cast<ValueImpl&>(rhs).GetData<Type>();
		}

	public:
		virtual ValueImpl* Clone() { return new ValueData<Type>(data); }
		virtual size_t GetByteSize() const { return sizeof(Type); }
		virtual Value::Type GetType() const { return GetType_(); }
		virtual void* GetData() { return &data; };

		static Value::Type GetType_() { return Value::Float64; }

	private:
		Type data;
	};

	template<>
	class ValueData<bool> : public ValueImpl
	{
	public:
		typedef bool Type;
		typedef Type const ConstType;
		typedef Type& ReferenceType;
		typedef Type const& ConstReferenceType;

	public:
		ValueData(ConstReferenceType data_ = Type()) : data(data_) {}

	public:
		virtual bool operator==(ValueImpl const& rhs) const 
		{
			if (this == &rhs)
				return true;
			if(GetType() != rhs.GetType())
				return false;
			return data == const_cast<ValueImpl&>(rhs).GetData<Type>();
		}

	public:
		virtual ValueImpl* Clone() { return new ValueData<Type>(data); }
		virtual size_t GetByteSize() const { return sizeof(Type); }
		virtual Value::Type GetType() const { return GetType_(); }
		virtual void* GetData() { return &data; };

		static Value::Type GetType_() { return Value::Bool; }

	private:
		Type data;
	};

	template<>
	class ValueData<std::string> : public ValueImpl
	{
	public:
		typedef std::string Type;
		typedef Type const ConstType;
		typedef Type& ReferenceType;
		typedef Type const& ConstReferenceType;

	public:
		ValueData(ConstReferenceType data_ = Type()) : data(data_) {}

	public:
		virtual bool operator==(ValueImpl const& rhs) const 
		{
			if (this == &rhs)
				return true;
			if(GetType() != rhs.GetType())
				return false;
			return data == const_cast<ValueImpl&>(rhs).GetData<Type>();
		}

	public:
		virtual ValueImpl* Clone() { return new ValueData<Type>(data); }
		virtual size_t GetByteSize() const { return data.size(); }
		virtual Value::Type GetType() const { return GetType_(); }
		virtual void* GetData() { return &data; };

		static Value::Type GetType_() { return Value::String; }

	private:
		Type data;
	};

	template<>
	class ValueData<Structure> : public ValueImpl
	{
	public:
		typedef Structure Type;
		typedef Type const ConstType;
		typedef Type& ReferenceType;
		typedef Type const& ConstReferenceType;

	public:
		ValueData(ConstReferenceType data_ = Type()) : data(data_) {}

	public:
		virtual bool operator==(ValueImpl const& rhs) const 
		{
			if (this == &rhs)
				return true;
			if(GetType() != rhs.GetType())
				return false;
			return data == const_cast<ValueImpl&>(rhs).GetData<Type>();
		}

	public:
		virtual ValueImpl* Clone() { return new ValueData<Type>(data); }
		virtual size_t GetByteSize() const
		{
			size_t size = 0;
			for (auto itor = data.begin(), end = data.end(); itor != end; ++itor)
			{
				size += itor->second.GetByteSize();
			}
			return size;
		}
		virtual Value::Type GetType() const { return GetType_(); }
		virtual void* GetData() { return &data; }

		static Value::Type GetType_() { return Value::Struct; }

	public:
		Type data;
	};

	template<>
	class ValueData<Array> : public ValueImpl
	{
	public:
		typedef Array Type;
		typedef Type const ConstType;
		typedef Type& ReferenceType;
		typedef Type const& ConstReferenceType;

	public:
		ValueData(ConstReferenceType data_ = Type()) : data(data_) {}

	public:
		virtual bool operator==(ValueImpl const& rhs) const 
		{
			if (this == &rhs)
				return true;
			if(GetType() != rhs.GetType())
				return false;
			return data == const_cast<ValueImpl&>(rhs).GetData<Type>();
		}

	public:
		virtual ValueImpl* Clone() { return new ValueData<Type>(data); }
		virtual size_t GetByteSize() const
		{
			size_t size = 0;
			for (auto itor = data.begin(), end = data.end(); itor != end; ++itor)
			{
				size += itor->GetByteSize();
			}
			return size;
		}
		virtual Value::Type GetType() const { return GetType_(); }
		virtual void* GetData() { return &data; }

		static Value::Type GetType_() { return Value::Array; }

	public:
		Type data;
	};
} // namespace scenario

#endif // !__SCENARIO_EXTERNAL_API_VALUE_IMPL_H__
