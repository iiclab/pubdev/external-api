#ifndef __SCENARIO_EXTERNAL_API_CDECL_CALLER_H__
#define __SCENARIO_EXTERNAL_API_CDECL_CALLER_H__

#include <stack>
#include <array>
#include <stdint.h>
#include <memory>

#include "Scenario/ExternalApi/ExternalApi.h"
#include "FunctionCaller.h"

namespace scenario
{
	class CdeclCaller : public FunctionCaller
	{
		typedef uint32_t StackElement;

	public:
		CdeclCaller(void* pFunction, const FunctionType& funtionType );
		virtual ~CdeclCaller();

	public:
		virtual Value Call(const std::vector<Value>& paramList);

	private:
		void Call(std::array<StackElement, 2>& rawReturnValue, std::deque<StackElement>& stack);

	private:
		void* const mpFunction;
		const FunctionType mFunctionType;
	};
} // namespace scenario

#endif