#ifndef __SCENARIO_EXTERNAL_API_FUNCTION_CALLER_H__
#define __SCENARIO_EXTERNAL_API_FUNCTION_CALLER_H__

#include <vector>
#include <array>
#include <list>
#include <memory>
#include <deque>
#include <cstdint>

#include "Scenario/ExternalApi/Value.h"
#include "Scenario/ExternalApi/ExternalApi.h"

namespace scenario
{
	class FunctionCaller
	{
	public:
		typedef uint32_t StackElement;

	public:
		virtual ~FunctionCaller() {}

	public:
		virtual Value Call(const std::vector<Value>& paramList) = 0;

	public:
		static size_t CalculateBytePaddingSize(const Structure& structure);
		static size_t CalculateSize(const Structure& structure);

	protected:
		std::deque<StackElement> StackTo(const std::vector<Value>& parameters, const std::vector<FunctionType::Parameter>& parameterTypes, std::list<std::shared_ptr<uint8_t>>& memoryHolders);
		void StackTo(std::vector<uint8_t>& stack, const Value& input, std::list<std::shared_ptr<uint8_t>>& memoryHolders, size_t bytePaddingSize = 0);

		Value GetReturnValue(const std::array<StackElement, 2>& rawReturnValue, const Value& type);
		Value GetReturnValue(const std::array<StackElement, 2>& rawReturnValue, const Structure& type, size_t bytePaddingSize);

		static void CalculateSize(size_t& size, const Structure& structure, size_t bytePaddingSize);
	};
} //namespace scenario

#endif