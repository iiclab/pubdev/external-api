#ifndef __SCENARIO_EXTERNAL_API_UTIL_STRING_UTIL_H__

#include <string>
#include <vector>

namespace scenario
{
	class StringUtil
	{
	public:
		static std::vector<std::string> Split(const std::string& input, const std::string& splitIndicator, bool noEmpty = true);
		static std::string SubString(const std::string& input, const std::string& start = "", const std::string& end = "");
		static std::string Trim(const std::string& input);
		static std::string Replace(const std::string& input, const std::string& original, const std::string& changed);
		static std::string ToLower(const std::string& input);
		static std::string ToUpper(const std::string& input);
	};
} // namespace scenario

#endif