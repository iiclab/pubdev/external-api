#ifndef __SCENARIO_UTIL_NONCOPYABLE_H__
#define __SCENARIO_UTIL_NONCOPYABLE_H__

namespace scenario
{
	class Noncopyable
	{
	protected:
		Noncopyable() {}
		~Noncopyable() {}

	private:
		Noncopyable(const Noncopyable&) {}
		const Noncopyable& operator=(const Noncopyable&);
	};
} // namespace scenario

#endif