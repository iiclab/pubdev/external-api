#ifndef __SCENARIO_EXTERNAL_API_ARRAY_H__
#define __SCENARIO_EXTERNAL_API_ARRAY_H__

#include <vector>

#include "Value.h"

namespace scenario
{
	class Array : public std::vector<Value>
	{
	private:
		typedef std::vector<Value> Super;

	public:
		Array() : mDefaultType() {}
		Array(const Value& type) : mDefaultType(type) {}

	public:
		template<typename DataType, size_t arraySize>
		Array(DataType (&array)[arraySize])
			: Super(array, array + arraySize), mDefaultType(Value::Void)
		{
			if (size() != 0)
				SetDefaultType(operator[](0));
		}

		template<typename DataType>
		Array(DataType* array, size_t arraySize) 
			: Super(array, array + arraySize), mDefaultType(Value::Void)
		{
			if (size() != 0)
				SetDefaultType(operator[](0));
		}

		template<typename Iterator>
		Array(Iterator& begin, Iterator& end) 
			: Super(begin, end), mDefaultType(Value::Void)
		{
			if (size() != 0)
				SetDefaultType(operator[](0));
		}

	public:
		bool operator==(Array const& rhs) const;
		bool operator!=(Array const& rhs) const { return !operator==(rhs); }

	public:
		const Value& GetDefaultType() const { return mDefaultType; }
		void SetDefaultType(const Value& type);
		void Normalize();
		void Normalize(const Value& type);

	private:
		Value mDefaultType;
	};
}

#endif