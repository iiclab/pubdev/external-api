#ifndef __SCENARIO_EXTERNAL_API_VALUE_H__
#define __SCENARIO_EXTERNAL_API_VALUE_H__

#include <memory>
#include <vector>
#include <string>
#include <cstdint>
#include <cassert>

namespace scenario
{
	class Structure;
	class Array;
	class ValueImpl;

	class TypeNotMatchedException : public std::runtime_error
	{
	public:
		TypeNotMatchedException()
			: runtime_error("Type is not matched")
		{}
	};

	class Value
	{
	public:
		enum Type
		{
			Void, Int8, Uint8, Int16, Uint16, Int32, Uint32, Int64, Uint64
			, Float32, Float64, Bool, String, Array, Struct
		};

	public:
		Value();
		explicit Value(Type type_);
		explicit Value(int8_t value);
		explicit Value(uint8_t value);
		explicit Value(int16_t value);
		explicit Value(uint16_t value);
		explicit Value(int32_t value);
		explicit Value(uint32_t value);
		explicit Value(int64_t value);
		explicit Value(uint64_t value);
		explicit Value(float value);
		explicit Value(double value);
		explicit Value(bool value);
		explicit Value(const std::string& str);
		explicit Value(const char* str);
		explicit Value(const scenario::Array& array);
		explicit Value(const Structure& structure);

		Value(const Value& value);

		~Value();

	public:
		Value& operator=(const Value& value);
		Value& operator=(Value&& value);

		template<typename ValueType>
		Value& operator=(ValueType const& value)
		{
			return *this = Value(value);
		}

		bool operator==(const Value& rhs) const;
		bool operator!=(const Value& rhs) const;

	public:
	 	static size_t GetByteSize(Type type_);

	public:
		size_t GetByteSize() const;
		Value& ConvertTo(Type type_);
		Value& ConvertTo(const Value& value);
		std::string ToString() const;
		Type GetType() const;
		void Clear();
		void Clear(Type type);

		int8_t const& AsInt8() const throw(TypeNotMatchedException);
		uint8_t const& AsUint8() const throw(TypeNotMatchedException);
		int16_t const& AsInt16() const throw(TypeNotMatchedException);
		uint16_t const& AsUint16() const throw(TypeNotMatchedException);
		int32_t const& AsInt32() const throw(TypeNotMatchedException);
		uint32_t const& AsUint32() const throw(TypeNotMatchedException);
		int64_t const& AsInt64() const throw(TypeNotMatchedException);
		uint64_t const& AsUint64() const throw(TypeNotMatchedException);
		float const& AsFloat32() const throw(TypeNotMatchedException);
		double const& AsFloat64() const throw(TypeNotMatchedException);
		bool const& AsBool() const throw(TypeNotMatchedException);
		std::string const& AsString() const throw(TypeNotMatchedException);
		scenario::Array const& AsArray() const throw(TypeNotMatchedException);
		Structure const& AsStructure() const throw(TypeNotMatchedException);

		int8_t& AsInt8() throw(TypeNotMatchedException);
		uint8_t& AsUint8() throw(TypeNotMatchedException);
		int16_t& AsInt16() throw(TypeNotMatchedException);
		uint16_t& AsUint16() throw(TypeNotMatchedException);
		int32_t& AsInt32() throw(TypeNotMatchedException);
		uint32_t& AsUint32() throw(TypeNotMatchedException);
		int64_t& AsInt64() throw(TypeNotMatchedException);
		uint64_t& AsUint64() throw(TypeNotMatchedException);
		float& AsFloat32() throw(TypeNotMatchedException);
		double& AsFloat64() throw(TypeNotMatchedException);
		bool& AsBool() throw(TypeNotMatchedException);
		std::string& AsString() throw(TypeNotMatchedException);
		scenario::Array& AsArray() throw(TypeNotMatchedException);
		Structure& AsStructure() throw(TypeNotMatchedException);
		
	private:
		std::unique_ptr<ValueImpl> mpValueImpl;
	};

	template<typename Stream>
	inline Stream& operator<<(Stream& stream, const Value& value)
	{
		switch(value.GetType())
		{
		case Value::Int32: stream << value.AsInt32(); break; 
		case Value::Uint32: stream << value.AsUint32(); break; 
		case Value::Float32: stream << value.AsFloat32(); break; 
		case Value::Float64: stream << value.AsFloat64(); break; 
		case Value::String: stream << value.AsString(); break; 
		case Value::Int8: stream << value.AsInt8(); break; 
		case Value::Uint8: stream << value.AsUint8(); break; 
		case Value::Bool: stream << value.AsBool(); break; 
		case Value::Int16: stream << value.AsInt16(); break; 
		case Value::Uint16: stream << value.AsUint16(); break; 
		case Value::Int64: stream << value.AsInt64(); break; 
		case Value::Uint64: stream << value.AsUint64(); break; 
		case Value::Array:
			{
				auto& array = value.AsArray();
				if (array.size() == 0)
					break;
				stream << array[0];
				for (auto itor = ++array.begin(); itor != array.end(); ++itor)
				{
					stream << ", " << *itor;
				}	
			}
			break;
		case Value::Struct: 
			{
				auto& structure = value.AsStructure();
				if (structure.begin() == structure.end())
					break;
				stream << structure.begin()->first << "=" << structure.begin()->second;
				for (auto itor = ++structure.begin(); itor != structure.end(); ++itor)
				{
					stream << ", " << itor->first << "=" << itor->second;
				}		
			}
		case Value::Void: break; //void는 값이 없는 것이므로
		default:
			assert(!"Not supported type");
		}

		return stream;
	}
} // namespace scenario

#include "Array.h"
#include "Structure.h"

#endif