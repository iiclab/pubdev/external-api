#ifndef __SCENARIO_EXTERNAL_API_EXTERNAL_API_H__
#define __SCENARIO_EXTERNAL_API_EXTERNAL_API_H__

#include <vector>
#include <string>
#include <cstdint>

#include "Value.h"
#include "TypeManager.h"

namespace scenario
{
	class FunctionCallException : public std::runtime_error
	{
	public:
		FunctionCallException()
			: runtime_error("Function call fails")
		{
		}
	};

	class FunctionType
	{
	public:
		struct Parameter
		{
			Value type;
			std::string name;
		};

	public:
		std::string name;
		Value returnType;
		std::vector<Parameter> parameters;
		std::string option;

	public:
		static bool Generate(FunctionType& result, const std::string& functionSignature, const TypeManager& typeManager);
	};

	class ExternalApi : public TypeManager
	{
	public:
		ExternalApi() {}
		virtual ~ExternalApi() {}

	private:
		// for noncopyable
		ExternalApi(const ExternalApi&);
		const ExternalApi& operator=(const ExternalApi&);

	public:
		virtual bool Load(const FunctionType& functionType) = 0;
		virtual bool Call(const std::string& functionName, Value& output, const std::vector<Value>& input) = 0;

	public:
		Value Call(const std::string& functionName, const std::vector<Value>& input)
		{
			Value result;

			if (!Call(functionName, result, input)) throw FunctionCallException();

			return result;            
		}
		
		template<typename Param1>
		Value Call(const std::string& functionName, Param1 param1)
		{
			Value result;
			std::vector<Value> parameter;
			parameter.push_back(param1);

			if (!Call(functionName, result, parameter)) throw FunctionCallException();

			return result;            
		}
		
		template<typename Param1, typename Param2>
		Value Call(const std::string& functionName, Param1 param1, Param2 param2)
		{
			Value result;
			std::vector<Value> parameter;
			parameter.reserve(2);
			parameter.push_back(param1);
			parameter.push_back(param2);

			if (!Call(functionName, result, parameter)) throw FunctionCallException();

			return result;            
		}

		template<typename Param1, typename Param2, typename Param3>
		Value Call(const std::string& functionName, Param1 param1, Param2 param2, Param3 param3)
		{
			Value result;
			std::vector<Value> parameter;
			parameter.reserve(3);
			parameter.push_back(param1);
			parameter.push_back(param2);
			parameter.push_back(param3);

			return Call(parameter);            
		}

		template<typename Param1, typename Param2, typename Param3
			, typename Param4>
			Value Call(const std::string& functionName, Param1 param1, Param2 param2, Param3 param3, Param4 param4)
		{
			Value result;
			std::vector<Value> parameter;
			parameter.reserve(4);
			parameter.push_back(param1);
			parameter.push_back(param2);
			parameter.push_back(param3);
			parameter.push_back(param4);

			if (!Call(functionName, result, parameter)) throw FunctionCallException();

			return result;                
		}

		template<typename Param1, typename Param2, typename Param3
			, typename Param4, typename Param5>
			Value Call(const std::string& functionName, Param1 param1, Param2 param2, Param3 param3, Param4 param4
			, Param5 param5)
		{
			Value result;
			std::vector<Value> parameter;
			parameter.reserve(5);
			parameter.push_back(param1);
			parameter.push_back(param2);
			parameter.push_back(param3);
			parameter.push_back(param4);
			parameter.push_back(param5);

			if (!Call(functionName, result, parameter)) throw FunctionCallException();

			return result;                
		}

		template<typename Param1, typename Param2, typename Param3
			, typename Param4, typename Param5, typename Param6>
			Value Call(const std::string& functionName, Param1 param1, Param2 param2, Param3 param3, Param4 param4
			, Param5 param5, Param6 param6)
		{
			Value result;
			std::vector<Value> parameter;
			parameter.reserve(6);
			parameter.push_back(param1);
			parameter.push_back(param2);
			parameter.push_back(param3);
			parameter.push_back(param4);
			parameter.push_back(param5);
			parameter.push_back(param6);

			if (!Call(functionName, result, parameter)) throw FunctionCallException();

			return result;                
		}

		template<typename Param1, typename Param2, typename Param3
			, typename Param4, typename Param5, typename Param6, typename Param7>
			Value Call(const std::string& functionName, Param1 param1, Param2 param2, Param3 param3, Param4 param4
			, Param5 param5, Param6 param6, Param7 param7)
		{
			Value result;
			std::vector<Value> parameter;
			parameter.reserve(7);
			parameter.push_back(param1);
			parameter.push_back(param2);
			parameter.push_back(param3);
			parameter.push_back(param4);
			parameter.push_back(param5);
			parameter.push_back(param6);
			parameter.push_back(param7);

			if (!Call(functionName, result, parameter)) throw FunctionCallException();

			return result;                  
		}

		template<typename Param1, typename Param2, typename Param3
			, typename Param4, typename Param5, typename Param6, typename Param7
			, typename Param8>
			Value Call(const std::string& functionName, Param1 param1, Param2 param2, Param3 param3, Param4 param4
			, Param5 param5, Param6 param6, Param7 param7, Param8 param8)
		{
			Value result;
			std::vector<Value> parameter;
			parameter.reserve(8);
			parameter.push_back(param1);
			parameter.push_back(param2);
			parameter.push_back(param3);
			parameter.push_back(param4);
			parameter.push_back(param5);
			parameter.push_back(param6);
			parameter.push_back(param7);
			parameter.push_back(param8);

			if (!Call(functionName, result, parameter)) throw FunctionCallException();

			return result;                  
		}

		template<typename Param1, typename Param2, typename Param3
			, typename Param4, typename Param5, typename Param6, typename Param7
			, typename Param8, typename Param9>
			Value Call(const std::string& functionName, Param1 param1, Param2 param2, Param3 param3, Param4 param4
			, Param5 param5, Param6 param6, Param7 param7, Param8 param8, Param9 param9)
		{
			Value result;
			std::vector<Value> parameter;
			parameter.reserve(9);
			parameter.push_back(param1);
			parameter.push_back(param2);
			parameter.push_back(param3);
			parameter.push_back(param4);
			parameter.push_back(param5);
			parameter.push_back(param6);
			parameter.push_back(param7);
			parameter.push_back(param8);
			parameter.push_back(param9);

			if (!Call(functionName, result, parameter)) throw FunctionCallException();

			return result;                 
		}

		template<typename Param1, typename Param2, typename Param3
			, typename Param4, typename Param5, typename Param6, typename Param7
			, typename Param8, typename Param9, typename Param10>
			Value Call(const std::string& functionName, Param1 param1, Param2 param2, Param3 param3, Param4 param4
			, Param5 param5, Param6 param6, Param7 param7, Param8 param8, Param9 param9
			, Param10 param10)
		{
			Value result;
			std::vector<Value> parameter;
			parameter.reserve(10);
			parameter.push_back(param1);
			parameter.push_back(param2);
			parameter.push_back(param3);
			parameter.push_back(param4);
			parameter.push_back(param5);
			parameter.push_back(param6);
			parameter.push_back(param7);
			parameter.push_back(param8);
			parameter.push_back(param9);
			parameter.push_back(param10);

			if (!Call(functionName, result, parameter)) throw FunctionCallException();

			return result;                   
		}
	};
} // namespace scenario

#endif