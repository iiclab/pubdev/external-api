#ifndef __SCENARIO_EXTERNAL_API_URL_H__
#define __SCENARIO_EXTERNAL_API_URL_H__

#include <vector>
#include <string>
#include <unordered_map>

namespace scenario
{
	class Url
	{
	public:
		typedef std::unordered_map<std::string, std::string> QueryMap;
		typedef std::vector<std::string> SplitedString;

		enum {INVALID_PORT_NUMBER = -1};

	public:
		Url();
		Url(const std::string& url_);
		Url(const Url& url_);

		~Url();

	public:
		Url& operator=(const Url& uri_);

	public:
		inline bool IsValid() const { return !mScheme.empty(); }

		inline const std::string& GetScheme() const { return mScheme; }
		inline const std::string& GetUserName() const { return mUserName; }
		inline const std::string& GetPassword() const { return mPassword; }
		inline const std::string& GetHostName() const { return mHostName; }
		inline int GetPortNumber() const { return mPortNumber; }
		inline const std::vector<std::string>& GetPath() const { return mPath; }
		inline const QueryMap& GetQuery() const { return mQuery; }
		inline const std::string& GetFragment() const { return mFragment; }

		std::string ToString() const;

	private:
		Url Parse(const std::string& uri);

		static bool IsValidScheme(const std::string& scheme_);
		static bool IsValidPortNumber(int portNumber_);

		static bool ParseScheme(Url& result, SplitedString::const_iterator itor, const SplitedString::const_iterator& end);
		static bool ParseAutority(Url& result, SplitedString::const_iterator itor, const SplitedString::const_iterator& end);
		static bool ParsePath(Url& result, SplitedString::const_iterator itor, const SplitedString::const_iterator& end);
		static bool ParseQuery(Url& result, SplitedString::const_iterator itor, const SplitedString::const_iterator& end);
		static bool ParseFragment(Url& result, SplitedString::const_iterator itor, const SplitedString::const_iterator& end);

	private:
		std::string mScheme;
		std::string mUserName;
		std::string mPassword;
		std::string mHostName;
		int mPortNumber;
		std::vector<std::string> mPath;
		QueryMap mQuery;
		std::string mFragment;
	};

} // namespace scenario

#endif