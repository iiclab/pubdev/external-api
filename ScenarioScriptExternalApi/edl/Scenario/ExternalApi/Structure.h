#ifndef __SCENARIO_EXTERNAL_API_STRUCTURE_H__
#define __SCENARIO_EXTERNAL_API_STRUCTURE_H__

#include <list>
#include <string>

#include "Value.h"

namespace scenario
{
	class Structure : public std::list<std::pair<std::string, Value>>
	{
	private:
		typedef std::list<std::pair<std::string, Value>> Super;

	public:
		Structure(const std::string& name = "");
		Structure(const std::string& name, const std::vector<std::string>& filedNames);
		Structure(const std::string& name, const std::vector<std::pair<std::string, Value>>& fields);

	public:
		bool operator==(Structure const& rhs) const;
		bool operator!=(Structure const& rhs) const { return !operator==(rhs); }

		inline Value& operator[](const std::string& fieldName) { return GetField(fieldName); }
		inline const Value& operator[](const std::string& fieldName) const { return GetField(fieldName); }

		Super::value_type& operator[](size_t i);
		const Super::value_type& operator[](size_t i) const;

	public:
		inline std::string& GetName() { return mName; }
		inline const std::string& GetName() const { return mName; }
		inline void SetName(std::string const& name) { mName = name; }

		Value& GetField(const std::string& fieldName);
		const Value& GetField(const std::string& fieldName) const;
		bool GetField(Value& output, const std::string& fieldName) const;
		void SetField(std::string const& fieldName, Value const& value);

		bool AddField(const std::string& fieldName, const Value& value = Value());
		void RemoveField(const std::string& fieldName);

	private:
		std::string mName;
	};
} // namespace scenario

#endif
