#ifndef __SCENARIO_EXTERNAL_API_EXTERNAL_API_LOAD_MANAGER_H__
#define __SCENARIO_EXTERNAL_API_EXTERNAL_API_LOAD_MANAGER_H__

#include <string>
#include <unordered_map>
#include <memory>

#include "ExternalApi.h"
#include "Url.h"

namespace scenario
{
	class DynamicLibrary;

	typedef ExternalApi* (*ExternalApiLoader)(const char* uri);
	typedef void (*ExternalApiReleaser)(ExternalApi* pExternalApi);

	class ExternalApiLoadManager
	{
	private:
		struct LoaderSet
		{
			std::shared_ptr<DynamicLibrary> pDynamicLibrary;
			ExternalApiLoader loader;
			ExternalApiReleaser releaser;
		};

	private:		
		ExternalApiLoadManager();

	public:
		~ExternalApiLoadManager();

	private:
		// for noncopyable
		ExternalApiLoadManager(const ExternalApiLoadManager&);
		const ExternalApiLoadManager& operator=(const ExternalApiLoadManager&);

	public:
		inline static ExternalApiLoadManager& GetInstance()
		{
			static ExternalApiLoadManager ExternalApiLoadManager;
			return ExternalApiLoadManager;
		}
		bool Register(const std::string& loaderPath, const std::string& scheme);
		void Unregister(const std::string& scheme);
		std::shared_ptr<ExternalApi> Load(const Url& url);
		std::shared_ptr<ExternalApi> Load(const std::string& url);

	private:
		std::unordered_map<std::string, LoaderSet> mLoaderMap;
	};
} // namespace scenario

#endif