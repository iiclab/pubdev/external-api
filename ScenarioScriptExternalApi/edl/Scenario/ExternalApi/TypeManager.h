#ifndef __SCENARIO_EXTERNAL_API_TYPE_MANAGER_H__
#define __SCENARIO_EXTERNAL_API_TYPE_MANAGER_H__

#include <string>

namespace scenario
{
	class Value;

	class TypeManager
	{
	public:
		TypeManager();
		virtual ~TypeManager();

	public:
		bool RegisterType(const std::string& typeName, const Value& value);
		void UnregisterType(const std::string& typeName);
		bool GetType(Value& output, const std::string& typeName) const;

	private:
		class TypeManagerImpl* mpImpl;
	};
} // namespace scenario

#endif