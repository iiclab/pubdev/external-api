
#ifndef _TestComponent_COMPONENT_H
#define _TestComponent_COMPONENT_H
/*
*  Generated sources by OPRoS Component Generator (OCG V2.0[Symbol])
*   
*/
#include <Component.h>
#include <InputDataPort.h>
#include <OutputDataPort.h>
#include <InputEventPort.h>
#include <OutputEventPort.h>
#include <Event.h>
#include <OPRoSTypes.h>




#include "TestServiceProvided.h"

class TestComponent: public Component
	,public ITestService
{
protected:
	// service
	// method for provider
	ITestService *ptrTestService;

public:
	virtual void Print(std::string message);
	virtual bool Execute(std::string programName);
	virtual unsigned long Factorial(unsigned int number);
	virtual double Power(float base,float radix);
	virtual std::string GetIpAddress();
	virtual Person PrintPerson(Person person);

protected:
	// data


	//event


	// method for provider


	// method for required



	// symbol value generation
	int count;

public:
	TestComponent();
	TestComponent(const std::string &compId);
	virtual ~TestComponent();
	virtual void portSetup();

protected:
	virtual ReturnType onInitialize();
	virtual ReturnType onStart();
	virtual ReturnType onStop();
	virtual ReturnType onReset();
	virtual ReturnType onError();
	virtual ReturnType onRecover();
	virtual ReturnType onDestroy();

public:
	virtual ReturnType onEvent(Event *evt);
	virtual ReturnType onExecute();
	virtual ReturnType onUpdated();
	virtual ReturnType onPeriodChanged();


};

#endif

