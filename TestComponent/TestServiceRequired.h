

#ifndef _TestService_REQUIRED_PORT_H
#define _TestService_REQUIRED_PORT_H

#include <OPRoSTypes.h>
#include <InputDataPort.h>
#include <OutputDataPort.h>
#include <InputEventPort.h>
#include <OutputEventPort.h>
#include <ProvidedServicePort.h>
#include <RequiredServicePort.h>
#include <ProvidedMethod.h>
#include <RequiredMethod.h>


#include <string>
		



/*
 * 
 * TestService : public RequiredServicePort
 *
 */
class TestServiceRequired
   :public RequiredServicePort
{
protected:

	typedef RequiredMethod<void> PrintFuncType;
	PrintFuncType *PrintFunc;

	typedef RequiredMethod<bool> ExecuteFuncType;
	ExecuteFuncType *ExecuteFunc;

	typedef RequiredMethod<unsigned long> FactorialFuncType;
	FactorialFuncType *FactorialFunc;

	typedef RequiredMethod<double> PowerFuncType;
	PowerFuncType *PowerFunc;

	typedef RequiredMethod<std::string> GetIpAddressFuncType;
	GetIpAddressFuncType *GetIpAddressFunc;

public:
	//
	// Constructor
	//
	TestServiceRequired()
	{
            PrintFunc = NULL;
            ExecuteFunc = NULL;
            FactorialFunc = NULL;
            PowerFunc = NULL;
            GetIpAddressFunc = NULL;
            
	   setup();
	}

	// method implementation for required method
	void Print(std::string message)
	{
            opros_assert(PrintFunc != NULL);
	
            (*PrintFunc)(message);
		
	}

	bool Execute(std::string programName)
	{
            opros_assert(ExecuteFunc != NULL);
	
            return (*ExecuteFunc)(programName);
		
	}

	unsigned long Factorial(unsigned int number)
	{
            opros_assert(FactorialFunc != NULL);
	
            return (*FactorialFunc)(number);
		
	}

	double Power(float base,float radix)
	{
            opros_assert(PowerFunc != NULL);
	
            return (*PowerFunc)(base,radix);
		
	}

	std::string GetIpAddress()
	{
            opros_assert(GetIpAddressFunc != NULL);
	
            return (*GetIpAddressFunc)();
		
	}

	

    // generated setup function
    virtual void setup()
    {
        Method *ptr_method;
    
        ptr_method = makeRequiredMethod(&TestServiceRequired::Print,"Print");
        opros_assert(ptr_method != NULL);
        addMethod("Print",ptr_method);
        PrintFunc = reinterpret_cast<PrintFuncType *>(ptr_method);
        ptr_method = NULL;

    
        ptr_method = makeRequiredMethod(&TestServiceRequired::Execute,"Execute");
        opros_assert(ptr_method != NULL);
        addMethod("Execute",ptr_method);
        ExecuteFunc = reinterpret_cast<ExecuteFuncType *>(ptr_method);
        ptr_method = NULL;

    
        ptr_method = makeRequiredMethod(&TestServiceRequired::Factorial,"Factorial");
        opros_assert(ptr_method != NULL);
        addMethod("Factorial",ptr_method);
        FactorialFunc = reinterpret_cast<FactorialFuncType *>(ptr_method);
        ptr_method = NULL;

    
        ptr_method = makeRequiredMethod(&TestServiceRequired::Power,"Power");
        opros_assert(ptr_method != NULL);
        addMethod("Power",ptr_method);
        PowerFunc = reinterpret_cast<PowerFuncType *>(ptr_method);
        ptr_method = NULL;

    
        ptr_method = makeRequiredMethod(&TestServiceRequired::GetIpAddress,"GetIpAddress");
        opros_assert(ptr_method != NULL);
        addMethod("GetIpAddress",ptr_method);
        GetIpAddressFunc = reinterpret_cast<GetIpAddressFuncType *>(ptr_method);
        ptr_method = NULL;

    
    }
};
#endif
