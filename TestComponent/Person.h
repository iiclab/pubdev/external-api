#ifndef __PERSON_H__

class Person
{
public:
	std::string name;
	int age;

public:

	void save(opros::archive::oarchive &ar, const unsigned int)
	{
		ar << name << age;
	}

	void load(opros::archive::iarchive &ar, const unsigned int)
	{
		ar >> name >> age;
	}
};

inline std::string typeName(Person* p)
{
	return "Person";
}

#endif // !__PERSON_H__
