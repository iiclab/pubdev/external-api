

#ifndef _TestService_PROVIDED_PORT_H
#define _TestService_PROVIDED_PORT_H

#include <OPRoSTypes.h>
#include <InputDataPort.h>
#include <OutputDataPort.h>
#include <InputEventPort.h>
#include <OutputEventPort.h>
#include <ProvidedServicePort.h>
#include <RequiredServicePort.h>
#include <ProvidedMethod.h>
#include <RequiredMethod.h>


#include <string>
#include "Person.h"
		

#ifndef _ITestService_CLASS_DEF
#define _ITestService_CLASS_DEF
/*
 * ITestService
 *
 *  The comonent inherits this class and implements methods. 
 */
class ITestService
{
 public:
    virtual void Print(std::string message)=0;
    virtual bool Execute(std::string programName)=0;
    virtual unsigned long Factorial(unsigned int number)=0;
    virtual double Power(float base,float radix)=0;
    virtual std::string GetIpAddress()=0;
	virtual Person PrintPerson(Person person)=0;
 };
#endif

/*
 * 
 * TestService : public ProvidedServicePort
 *
 */
class TestServiceProvided
	:public ProvidedServicePort, public ITestService
{
protected:
    ITestService *pcom;


   typedef ProvidedMethod<void> PrintFuncType;
   PrintFuncType *PrintFunc;

   typedef ProvidedMethod<bool> ExecuteFuncType;
   ExecuteFuncType *ExecuteFunc;

   typedef ProvidedMethod<unsigned long> FactorialFuncType;
   FactorialFuncType *FactorialFunc;

   typedef ProvidedMethod<double> PowerFuncType;
   PowerFuncType *PowerFunc;

   typedef ProvidedMethod<std::string> GetIpAddressFuncType;
   GetIpAddressFuncType *GetIpAddressFunc;

   typedef ProvidedMethod<Person> PrintPersonFuncType;
   PrintPersonFuncType *PrintPersonFunc;


public: // default method
   virtual void Print(std::string message)
   {
		opros_assert(PrintFunc != NULL);
		(*PrintFunc)(message);
   }
   virtual bool Execute(std::string programName)
   {
		opros_assert(ExecuteFunc != NULL);
		
            return (*ExecuteFunc)(programName);
		
   }
   virtual unsigned long Factorial(unsigned int number)
   {
		opros_assert(FactorialFunc != NULL);
		
            return (*FactorialFunc)(number);
		
   }
   virtual double Power(float base,float radix)
   {
		opros_assert(PowerFunc != NULL);
		
            return (*PowerFunc)(base,radix);
		
   }
   virtual std::string GetIpAddress()
   {
		opros_assert(GetIpAddressFunc != NULL);
		
            return (*GetIpAddressFunc)();
		
   }
   virtual Person PrintPerson(Person person)
   {
	   opros_assert(PrintPersonFunc != NULL);

	   return (*PrintPersonFunc)(person);
   }

public:
    //
    // Constructor
    //
    TestServiceProvided(ITestService *fn)
    {
        pcom = fn;

        PrintFunc = NULL;
        ExecuteFunc = NULL;
        FactorialFunc = NULL;
        PowerFunc = NULL;
        GetIpAddressFunc = NULL;
		PrintPersonFunc = NULL;
        

        setup();
    }

    // generated setup function
    virtual void setup()
    {
        Method *ptr_method;
    
        ptr_method = makeProvidedMethod(&ITestService::Print,pcom,"Print");

        opros_assert(ptr_method != NULL);
        addMethod("Print",ptr_method);
        PrintFunc = reinterpret_cast<PrintFuncType *>(ptr_method);
        ptr_method = NULL;

    
        ptr_method = makeProvidedMethod(&ITestService::Execute,pcom,"Execute");

        opros_assert(ptr_method != NULL);
        addMethod("Execute",ptr_method);
        ExecuteFunc = reinterpret_cast<ExecuteFuncType *>(ptr_method);
        ptr_method = NULL;

    
        ptr_method = makeProvidedMethod(&ITestService::Factorial,pcom,"Factorial");

        opros_assert(ptr_method != NULL);
        addMethod("Factorial",ptr_method);
        FactorialFunc = reinterpret_cast<FactorialFuncType *>(ptr_method);
        ptr_method = NULL;

    
        ptr_method = makeProvidedMethod(&ITestService::Power,pcom,"Power");

        opros_assert(ptr_method != NULL);
        addMethod("Power",ptr_method);
        PowerFunc = reinterpret_cast<PowerFuncType *>(ptr_method);
        ptr_method = NULL;

    
        ptr_method = makeProvidedMethod(&ITestService::GetIpAddress,pcom,"GetIpAddress");

        opros_assert(ptr_method != NULL);
        addMethod("GetIpAddress",ptr_method);
        GetIpAddressFunc = reinterpret_cast<GetIpAddressFuncType *>(ptr_method);
        ptr_method = NULL;

		ptr_method = makeProvidedMethod(&ITestService::PrintPerson,pcom,"PrintPerson");
		opros_assert(ptr_method != NULL);
		addMethod("PrintPerson", ptr_method);
		PrintPersonFunc = reinterpret_cast<PrintPersonFuncType*>(ptr_method);
		ptr_method = NULL;
    
    }
};
#endif
