
/*
 *  Generated sources by OPRoS Component Generator (OCG V2.1 [Symbol,Topic])
 *  
 */
#include <Component.h>
#include <InputDataPort.h>
#include <OutputDataPort.h>
#include <InputEventPort.h>
#include <OutputEventPort.h>
#include <OPRoSTypes.h>
#include <EventData.h>
#include <ServiceUtils.h>

#include <cstdint>
#include <WinSock2.h>

#include "TestComponent.h"
#include <SymbolService.h>

//
// constructor declaration
//
TestComponent::TestComponent()
{

	
	portSetup();
}

//
// constructor declaration (with name)
//
TestComponent::TestComponent(const std::string &name):Component(name)
{

	
	portSetup();
}

//
// destructor declaration
//

TestComponent::~TestComponent() 
{
}

std::string TestComponent::GetIpAddress()
{
	std::string result;
	char hostName[256] = {'\0'};

	WSADATA wsadata;

	if(WSAStartup(MAKEWORD(2, 2), &wsadata) == 0 
		&& gethostname(hostName, sizeof(hostName)) != SOCKET_ERROR) 
	{
        HOSTENT* pHostEntry = gethostbyname(hostName);

		if (pHostEntry != nullptr)
		{
			for (auto ip = pHostEntry->h_addr_list; *ip != nullptr; ip++)
			{
                result.append(inet_ntoa(*(IN_ADDR*)*ip));
                result.append("; ");
			}
		}
	}

	WSACleanup();

	return result;
}

double TestComponent::Power(float base,float radix)
{
	return pow(base, radix);
}

unsigned long TestComponent::Factorial(unsigned int number)
{
    uint64_t result = 1;

    for (auto temp = number; temp > 1; temp--)
    {
        result *= temp;
    }

    return (unsigned long)result;
}

bool TestComponent::Execute(std::string programName)
{
	return WinExec(programName.c_str(), SW_SHOWNORMAL) > 31;
}

void TestComponent::Print(std::string message)
{
	std::cout << message << std::endl;
}

void TestComponent::portSetup() {
	ProvidedServicePort *pa1;
	pa1=new TestServiceProvided(this);
	addPort("TestService",pa1);

	// export variable setup
	EXPORT_VARIABLE("count", count);

}

// Call back Declaration
ReturnType TestComponent::onInitialize()
{
	count = 0;
	return OPROS_SUCCESS;
}

ReturnType TestComponent::onStart()
{
	return OPROS_SUCCESS;
}
	
ReturnType TestComponent::onStop()
{
	// user code here
	return OPROS_SUCCESS;
}

ReturnType TestComponent::onReset()
{
	// user code here
	return OPROS_SUCCESS;
}

ReturnType TestComponent::onError()
{
	// user code here
	return OPROS_SUCCESS;
}

ReturnType TestComponent::onRecover()
{
	// user code here
	return OPROS_SUCCESS;
}

ReturnType TestComponent::onDestroy()
{
	// user code here
	return OPROS_SUCCESS;
}

ReturnType TestComponent::onEvent(Event *evt)
{
	// user code here
	return OPROS_SUCCESS;
}

ReturnType TestComponent::onExecute()
{
	// user code here
	return OPROS_SUCCESS;
}
	
ReturnType TestComponent::onUpdated()
{
	// user code here
	return OPROS_SUCCESS;
}

ReturnType TestComponent::onPeriodChanged()
{
	// user code here
	return OPROS_SUCCESS;
}

Person TestComponent::PrintPerson( Person person )
{
	std::cout << "�̸� : " << person.name << std::endl;
	std::cout << "���� : " << person.age << std::endl;
	return person;
}







#ifndef MAKE_STATIC_COMPONENT
#ifdef WIN32

extern "C"
{
__declspec(dllexport) Component *getComponent();
__declspec(dllexport) void releaseComponent(Component *pcom);
}

Component *getComponent()
{
	return new TestComponent();
}

void releaseComponent(Component *com)
{
	delete com;
}

#else
extern "C"
{
	Component *getComponent();
	void releaseComponent(Component *com);
}
Component *getComponent()
{
	return new TestComponent();
}

void releaseComponent(Component *com)
{
	delete com;
}
#endif
#endif

