#ifndef __EXTERNAL_API_SYMBOL_VARIABLE_REQUESTER_H__
#define __EXTERNAL_API_SYMBOL_VARIABLE_REQUESTER_H__

#include <unordered_map>

#include "system/io/protocol/IoWorkerManager.h"
#include "system/os/OSNotify.h"

#include "protocol/IoSymbolProtocol.h"

class SymbolPacket;

class SymbolVariableRequester : public IoWorkerManager, public IoSymbolProtocol
{
public:
	SymbolVariableRequester();
	virtual ~SymbolVariableRequester();

public:
	virtual IoWorker* createWorker();		
	virtual bool OnReceived(IoConnection* pConnect, const SymbolPacket& packet);

	bool RequestSetVariable(const std::string& ip, int port, const std::string& componentName, const std::string& variableName, const std::string& input);
	bool RequestGetVariable(std::string& output, const std::string& ip, int port, const std::string& componentName, const std::string& variableName);

protected:
	virtual IoTranceiver* createTransceiver(IoConnection* pConnection);

private:
	std::string findConnectionId(const std::string& ip, int port, const std::string& compName, const std::string& protocol = "");
	void SetReceiveData(const std::string& id, std::shared_ptr<SymbolPacket>& pData);
	std::shared_ptr<SymbolPacket> GetReceiveData(const std::string& id, unsigned long timeout = 5000);

private:
	std::unordered_map<std::string, std::shared_ptr<SymbolPacket>> mReceiveDataMap;
	std::unordered_map<std::string, std::string> mNodeIdMap;
	OSNotify mReceiveValueNotify;
};

#endif