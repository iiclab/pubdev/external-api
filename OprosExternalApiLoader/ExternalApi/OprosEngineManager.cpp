#include "OprosEngineManager.h"

#include <array>

#include "system/config/tinyxml/TinyXmlConfig.h"
#include "system/Registry.h"
#include "system/io/IoManager.h"
#include "ArchiveManager.h"
#include "system/cm/ComponentManager.h"
#include "system/util/StringUtil.h"

#include "SymbolVariableRequester.h"

using opros::engine::Config;

OprosEngineManager::OprosEngineManager()
	: mIsEnabled(false)
{
	mpConfig = CreateConfig();	
	mpArchiveManaver = new ArchiveManager(DEFAULT_ARCHIVE_TYPE);
	mpIoMnager = new IoManager();
	mpComponentManager = new ComponentManager();

	// 매니저 이름지정
	mpArchiveManaver->setName(ARCHIVE_MANAGER);
	mpIoMnager->setName(IO_MGNAGER);
	mpComponentManager->setName(COMPONENT_MANAGER);

	// 매니저들에 대한 자식 Config 추가
	mpConfig->addChild(mpArchiveManaver->getName());
	mpConfig->addChild(mpIoMnager->getName());
	mpConfig->addChild(mpComponentManager->getName());
	
	// 매니저 등록
	Registry* pRegistry = Registry::getRegistry();

	pRegistry->setManager(mpComponentManager->getName(), mpComponentManager);	
	pRegistry->setManager(mpIoMnager->getName(), mpIoMnager);	
	pRegistry->setManager(mpArchiveManaver->getName(), mpArchiveManaver);

	mpSymbolVariableRequester = new SymbolVariableRequester();
}

OprosEngineManager::~OprosEngineManager()
{
	Disable();

	delete mpSymbolVariableRequester;

	Registry* pRegistry = Registry::getRegistry();
	
	pRegistry->removeManager(mpComponentManager->getName());	
	pRegistry->removeManager(mpIoMnager->getName());	
	pRegistry->removeManager(mpArchiveManaver->getName());

	delete mpComponentManager;
	delete mpIoMnager;
	delete mpArchiveManaver;
	delete mpConfig;
}

bool OprosEngineManager::Enable()
{
	if(mIsEnabled) return true;	

	std::array<Manager*, 3> managerList 
		= {mpArchiveManaver, mpIoMnager, mpComponentManager};

	for(auto itor = managerList.begin(), end = managerList.end()
		; itor != end; ++itor)
	{
		Manager* pManager = *itor;
		pManager->setConfig(mpConfig->getChild(pManager->getName()));
		if(!pManager->init())
		{
			//로그 직어주는 기능 추가해야함
			return false;
		}
	}

	for(auto itor = managerList.begin(), end = managerList.end()
		; itor != end; ++itor)
	{
		Manager* pManager = *itor;
		if(!pManager->start())
		{
			//로그 직어주는 기능 추가해야함
			return false;
		}
	}

	mIsEnabled = true;
	return true;
}

bool OprosEngineManager::Disable()
{
	if(!mIsEnabled) return true;

	std::array<Manager*, 3> managerList 
		= {mpArchiveManaver, mpIoMnager, mpComponentManager};

	for(auto itor = managerList.begin(), end = managerList.end()
		; itor != end; ++itor)
	{
		Manager* pManager = *itor;
		if(!pManager->stop())
		{
			//로그 직어주는 기능 추가해야함
		}
	}

	for(auto itor = managerList.begin(), end = managerList.end()
		; itor != end; ++itor)
	{
		Manager* pManager = *itor;
		if(!pManager->destroy())
		{
			//로그 직어주는 기능 추가해야함
		}
	}
	
	mIsEnabled = false;
	return false;
}

bool OprosEngineManager::AddProperty(const Property& property, const std::string& parent)
{
	if(mIsEnabled) return false;
	
	if(parent.empty())
	{
		mpConfig->addAttribute(property.first, property.second);
	}
	else
	{
		auto parentList = StringUtil::split(parent, '/');

		
		Config* pParentConfig = mpConfig;		
		for(auto itor = parentList.begin(), end = parentList.end()
			; itor != end; ++itor)
		{
			Config* pConfig = pParentConfig->getChild(*itor);
			if(pConfig == nullptr)	pConfig = pParentConfig->addChild(*itor);
			pParentConfig = pConfig;
		}
		
		pParentConfig->addAttribute(property.first, property.second);
	}

	return true;
}

Config* OprosEngineManager::CreateConfig()
{
	return new TinyXmlConfig();
}

bool OprosEngineManager::AddRemoteComponent(const std::string& component, const std::string& ip, int port)
{
	std::stringstream stream;
	stream << ip << ":" << port;
	std::string nodeName = stream.str();
	
	NodeLocator* pNodeLocator = mpComponentManager->getNodeLocator();

	if(pNodeLocator->getNodeById(nodeName) == nullptr)
	{
		NodeInfo* pNodeInfo = new NodeInfo();
		pNodeInfo->node_id = nodeName;
		pNodeInfo->io.protocol = "tcp";
		pNodeInfo->io.role = "client";
		pNodeInfo->io.enabled = true;
		pNodeInfo->io.props.setProperty("cnn.handler", "component_io");
		pNodeInfo->io.props.setProperty("evt.processor", "system");
		pNodeInfo->io.props.setProperty("ip.addr", ip);
		pNodeInfo->io.props.setProperty("ip.port", pNodeInfo->io.props.intToStr(port));
		pNodeLocator->addNode(pNodeInfo);
	}

	ComponentLocator* pComponentLocator = mpComponentManager->getComponentLocator();
	pComponentLocator->addComponentLocation(component, nodeName);

	return true;
}