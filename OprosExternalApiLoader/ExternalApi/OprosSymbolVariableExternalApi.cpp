#include "OprosSymbolVariableExternalApi.h"

#include "system/Registry.h"
#include "system/cm/ComponentManager.h"
#include "ArchiveManager.h"
#include "SymbolService.h"
#include "BinaryArchiveFactory.h"
#include "StringArchiveFactory.h"

#include "OprosServiceCaller.h"

OprosSymbolVariableExternalApi::OprosSymbolVariableExternalApi( const std::string& componentName
															   , const std::string& ip, int port
															   , SymbolVariableRequester& requester)
	: mComponentName(componentName), mIp(ip), mPort(port), mRequester(requester)
{	
	mpArchiveFactory = GetArchiveFactory(OPROS_SYMBOL_ENCODING_STR);
}

OprosSymbolVariableExternalApi::~OprosSymbolVariableExternalApi()
{
}

bool OprosSymbolVariableExternalApi::Load( const scenario::FunctionType& functionType )
{
	if(mSymbolVariableTypeMap.count(functionType.name) != 0)
		return false;
	
	mSymbolVariableTypeMap.insert(std::make_pair(functionType.name, functionType.returnType));

	return true;
}

bool OprosSymbolVariableExternalApi::Call( const std::string& functionName, scenario::Value& output, const std::vector<scenario::Value>& input )
{
	auto itor = mSymbolVariableTypeMap.find(functionName);
	if(itor == mSymbolVariableTypeMap.end())
		return false;

	try
	{
		if(input.size() == 0)
		{
			output.ConvertTo(itor->second);
			return GetSymbolVariable(output, functionName);
		}
		else
		{
			scenario::Value value = input[0];
			value.ConvertTo(itor->second);

			if(!SetSymbolVariable(functionName, value))
				return false;

			output = value;
			return true;
		}
	}
	catch(...)
	{
		return false;
	}
}

bool OprosSymbolVariableExternalApi::GetSymbolVariable( scenario::Value& output, const std::string& name )
{
	std::string outvalue;

	if(!mRequester.RequestGetVariable(outvalue, mIp, mPort, mComponentName, name))
		return false;

	std::stringstream outputStream(outvalue);
	opros::archive::iarchive* pIarchive = mpArchiveFactory->getInputArchive(&outputStream);

	output = OprosServiceCaller::Deserialize(*pIarchive, output);

	mpArchiveFactory->release(pIarchive);

	return true;
}

bool OprosSymbolVariableExternalApi::SetSymbolVariable( const std::string& name, const scenario::Value& input )
{
	std::stringstream inputStream;
	opros::archive::oarchive* pOarchive = mpArchiveFactory->getOutputArchive(&inputStream);
	OprosServiceCaller::Serialize(*pOarchive, input);

	mpArchiveFactory->release(pOarchive);

	return mRequester.RequestSetVariable(mIp, mPort, mComponentName, name, inputStream.str());
}

ArchiveFactory* OprosSymbolVariableExternalApi::GetArchiveFactory( SymbolEncodingRule rule )
{
	std::string factoryName;
	if(rule == OPROS_SYMBOL_ENCODING_STR)
		factoryName = "string";
	else
		factoryName = "binary";
	
	Registry* pRegistry = Registry::getRegistry();
	ArchiveManager* pArchiveManger = static_cast<ArchiveManager*>(pRegistry->getManager(ARCHIVE_MANAGER));
	auto pArchiveFactory = pArchiveManger->getArchiveFactory(factoryName);
	if(pArchiveFactory == nullptr)
	{
		if(rule == OPROS_SYMBOL_ENCODING_STR)
			pArchiveFactory = new StringArchiveFactory();
		else
			pArchiveFactory = new BinaryArchiveFactory();
		
		pArchiveManger->addArchiveFactory(factoryName, pArchiveFactory);
	}
	
	return pArchiveFactory;
}

