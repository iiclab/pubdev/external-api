#ifndef __EXTERNAL_API_OPROS_SERVICE_EXTERNAL_API_H__
#define __EXTERNAL_API_OPROS_SERVICE_EXTERNAL_API_H__

#include <unordered_map>

#include <Scenario/ExternalApi/ExternalApi.h>

#include "OprosServiceCaller.h"

class OprosServiceCaller;

class OprosServiceExternalApi : public scenario::ExternalApi
{
public:
    OprosServiceExternalApi(const std::string& componentName, const std::string& servicePortName);
    virtual ~OprosServiceExternalApi(void);

public:
    bool Load(const scenario::FunctionType& functionType);
    bool Call(const std::string& functionName, scenario::Value& output, const std::vector<scenario::Value>& input);

private:
    std::string mComponentName;
    std::string mServicePortName;
    std::unordered_map<std::string, scenario::FunctionType> mFunctionTypeMap;
	
	OprosServiceCaller mServiceCaller;
};

#endif