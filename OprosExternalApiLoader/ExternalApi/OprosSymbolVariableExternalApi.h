#ifndef __EXTERNAL_API_OPROS_SYMBOL_VARIABLE_EXTERNAL_API_H__
#define __EXTERNAL_API_OPROS_SYMBOL_VARIABLE_EXTERNAL_API_H__


#include <unordered_map>

#include <Scenario/ExternalApi/ExternalApi.h>

#include "SymbolVariableRequester.h"
#include "SymbolService.h"

class OprosServiceCaller;
class ArchiveFactory;
class ServicePortConnector;

class OprosSymbolVariableExternalApi : public scenario::ExternalApi
{
public:
	OprosSymbolVariableExternalApi(const std::string& componentName, const std::string& ip, int port, SymbolVariableRequester& requester);
	virtual ~OprosSymbolVariableExternalApi();

public:
	bool Load(const scenario::FunctionType& functionType);
	bool Call(const std::string& functionName, scenario::Value& output, const std::vector<scenario::Value>& input);
	
private:
	ArchiveFactory* GetArchiveFactory(SymbolEncodingRule rule);
	bool GetSymbolVariable(scenario::Value& output, const std::string& name);
	bool SetSymbolVariable(const std::string& name, const scenario::Value& input);


private:
	std::string mComponentName;
	std::unordered_map<std::string, scenario::Value> mSymbolVariableTypeMap;

	ArchiveFactory* mpArchiveFactory;
	SymbolVariableRequester& mRequester;
	const std::string mIp;
	const int mPort;
};

#endif