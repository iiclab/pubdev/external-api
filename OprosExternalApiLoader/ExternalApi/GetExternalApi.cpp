#include <Scenario/ExternalApi/Url.h>

#include "ArchiveManager.h"
#include "system/cm/ComponentManager.h"
#include "system/Registry.h"
#include "system/io/IoManager.h"
#include "system/os/OSMutex.h"
#include "system/os/OSGuard.h"

#include "OprosEngineManager.h"

#include "OprosServiceExternalApi.h"
#include "OprosSymbolVariableExternalApi.h"

#define EXTERANL_API_URI_SCHEME "opros"

static OSMutex gEngineManagerMutex;

extern "C"
{
	__declspec(dllexport) scenario::ExternalApi* GetExternalApi( const char* uri_ );
	__declspec(dllexport) void ReleaseExternalApi(scenario::ExternalApi* pExternalApi);
}

scenario::ExternalApi* GetExternalApi( const char* uri_ )
{
	scenario::Url uri(uri_);

	if (!uri.IsValid()
		|| _stricmp(uri.GetScheme().c_str(), EXTERANL_API_URI_SCHEME) != 0)
	{
		return nullptr;
	}

	std::string nodeName = uri.GetHostName();
	int portNumber = uri.GetPortNumber();
	auto path = uri.GetPath();

	if (path.size() < 2)
	{
		return nullptr;
	}

	OprosEngineManager& manager = OprosEngineManager::GetInstance();

	if (!manager.IsEnabled())
	{
		OSGuard guard(&gEngineManagerMutex);
		if(!manager.IsEnabled())
		{
			//manager.AddProperty(std::make_pair("cnn.handler", "component_io"), COMPONENT_MANAGER"/io_connector");
			//manager.AddProperty(std::make_pair("event_processor", "system"), COMPONENT_MANAGER"/io_connector");
			//manager.AddProperty(std::make_pair("workers", "10"), COMPONENT_MANAGER);

			manager.AddProperty(std::make_pair("id", "system"), IO_MGNAGER"/event_processor");
			manager.AddProperty(std::make_pair("type", "selector"), IO_MGNAGER"/event_processor");
			manager.AddProperty(std::make_pair("timeout", "0"), IO_MGNAGER"/event_processor");

			if(!manager.Enable())
			{
				//���� ����
				return nullptr;
			}
		}
	}

	if(path[1] == "service" && path.size() >= 3)
	{
		manager.AddRemoteComponent(path[0], nodeName, portNumber);
		return new OprosServiceExternalApi(path[0], path[2]);
	}
	else if(path[1] == "symbol")
	{
		return new OprosSymbolVariableExternalApi(path[0], nodeName, portNumber
			, manager.GetSymbolVariableReqeuster());
	}

	return nullptr;
}

void ReleaseExternalApi(scenario::ExternalApi* pExternalApi)
{
	delete pExternalApi;
}
