#ifndef __EXTERNAL_API_OPROS_SERVICE_CALLER_H__
#define __EXTERNAL_API_OPROS_SERVICE_CALLER_H__

#include <Scenario/ExternalApi/ExternalApi.h>

namespace opros
{
	namespace archive
	{
		class oarchive;
		class iarchive;
	}
}

class ArchiveFactory;
class ServicePortConnector;

class OprosServiceCaller
{
public:
	OprosServiceCaller(const std::string& componentName, const std::string& serviceName, ArchiveFactory* pArchiveFactory = nullptr);
	~OprosServiceCaller(void);

public:
	scenario::Value Call(const scenario::FunctionType& functionType, const std::vector<scenario::Value>& paramList);

public:
	static void Serialize(opros::archive::oarchive& archive, const scenario::Value& value);
	static scenario::Value Deserialize(opros::archive::iarchive& archive, const scenario::Value& type);
	
private:
	static std::string GetTypeName(const scenario::Value& value);

private:   
	ArchiveFactory* mpArchiveFactory;
	ServicePortConnector* mpServicePortConnector;

	std::string mComponentName;
	std::string mServiceName;
};

#endif