#include "SymbolPacket.h"

#include <sstream>

SymbolPacket::SymbolPacket( const std::string& command /*= ""*/, const std::string& payload_ /*= ""`*/ )
{
	SetVersion("1.0");
	SetTarget("symbol");
	if(command != "")
		SetCommand(command);
	SetPayload(payload_);
}

SymbolPacket::SymbolPacket(const SymbolPacket& packet)
{
	properties.copy(const_cast<SymbolPacket&>(packet).properties);
	mPayload = packet.mPayload;
}

SymbolPacket& SymbolPacket::operator=( const SymbolPacket& packet )
{
	if(this == &packet)
		return *this;

	properties.copy(const_cast<SymbolPacket&>(packet).properties);
	mPayload = packet.mPayload;

	return *this;
}

std::string SymbolPacket::ConvertToPakcet() const
{
	std::stringstream stream;

	std::vector<std::string> propertyName;
	const_cast<Properties&>(properties).getNames(propertyName);

	for (auto itor = propertyName.begin(); itor != propertyName.end()
		; ++itor)
	{
		stream << *itor << "=" << properties.getProperty(*itor, "") << ";";
	}

	stream << std::endl;
	stream << mPayload;

	return stream.str();
}

void SymbolPacket::Parse( const std::string input )
{
	properties.toProperties(input, ';');
	size_t payloadSize = properties.getInt("payloadSize", 0);
	mPayload.resize(payloadSize);
}
