#include "IoSymbolProtocol.h"

#include "system/Trace.h"
#include "system/Log.h"

#include "IoSymbolTranceiver.h"
#include "IoSymbolWorker.h"
#include "SymbolPacket.h"

IoSymbolProtocol::IoSymbolProtocol()
{
	setConnectionHandlerId("symbol_variable_io");
}

IoSymbolProtocol::~IoSymbolProtocol()
{
}

IoTranceiver* IoSymbolProtocol::createTransceiver( IoConnection* pConnection )
{
	return new IoSymbolTranceiver(nullptr, pConnection);
}

bool IoSymbolProtocol::Send( const std::string& connectionId, const SymbolPacket& packet )
{
	trace_enter();

	IoConnection* pConnection = findConnection(connectionId);
	if (pConnection == nullptr) 
	{
		log_error("IoConnection is null");
		return false;
	}

	IoSymbolTranceiver* pTranceiver 
		= static_cast<IoSymbolTranceiver*>(pConnection->getUserObject("transceiver"));
	if (pTranceiver == nullptr) 
	{
		log_error("IoSymbolTranceiver is NULL : cnn.id=" << pConnection->getId());
		return true;
	}

	if (pTranceiver->Send(packet) == false) 
	{
		log_error("Sending ERROR");
		return false;
	}

	return true;
}

bool IoSymbolProtocol::OnReceived( IoConnection* pConnection, const SymbolPacket& pakcet )
{
	trace_enter();

	if (pConnection == nullptr) 
	{
		log_error("IoConnection is NULL");
		return true;
	}

	if (m_workerMgr != nullptr) 
	{
		IoWorker* pWorker = m_workerMgr->getWorker();
		if (pWorker != nullptr) 
		{
			IoSymbolWorker* pIoSymbolWorker = dynamic_cast<IoSymbolWorker*>(pWorker);
			if (pIoSymbolWorker != nullptr) 
			{
				pIoSymbolWorker->SetData(pConnection, pakcet);
				pIoSymbolWorker->startWork(pConnection->getId());
			}
			else 
			{
				log_error("Not IoSymbolWorker");
			}
		}
		else 
		{
			log_error("No IoWorker");
		}
	}
	else
	{
		log_error("No WorkerManger");
	}

	return true;
}