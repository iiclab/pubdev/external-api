#ifndef __EXTERNAL_API_PROTOCOL_IO_SYMBOL_WORKER_H__
#define __EXTERNAL_API_PROTOCOL_IO_SYMBOL_WORKER_H__

#include "system/io/protocol/IoWorker.h"

class SymbolVariableRequester;
class SymbolPacket;

class IoSymbolWorker : public IoWorker
{
public:
	virtual ~IoSymbolWorker() {}

public:
	virtual void SetData(IoConnection* pConnection, const SymbolPacket& packet) = 0;
};

#endif