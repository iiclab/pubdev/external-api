#include "IoSymbolTranceiver.h"

#include <sstream>

#include "system/io/IoEventProcessor.h"
#include "system/Trace.h"
#include "system/Log.h"

#include "../SymbolVariableRequester.h"
#include "IoSymbolProtocol.h"
#include "SymbolPacket.h"

IoSymbolTranceiver::IoSymbolTranceiver( IoSymbolProtocol* pProtocol, IoConnection* pConnection )
	: mpProtocol(pProtocol), mpConnection(pConnection)
{
	trace_enter();
	
	mIsClosed = false;

	mWritePosition = 0;
	mReadPosition = mReadLength = 0;
}

IoSymbolTranceiver::~IoSymbolTranceiver()
{
	if (mIsClosed == false) onClose();
}

bool IoSymbolTranceiver::onSend()
{
	if (mIsClosed || mSendBuffer.size() == 0 || mpConnection->isOpen() == false) 
	{
		return false;
	}

	int wc = mpConnection->write(&mSendBuffer.at(mWritePosition), mSendBuffer.size() - mWritePosition);
	if (wc < 0) 
	{
		ErrorWrite();
		log_error("Connection write error : cnn.id=" << mpConnection->getId());
		return false;
	}

	mWritePosition += wc;
	if (mWritePosition < (int)mSendBuffer.size()) 
	{
		mpConnection->clearEvent(IoEventUtil::writeEvent());
		mpConnection->getEventProcessor()->registerEventTarget(mpConnection);
	}
	else
	{
		EndWrite();
	}

	return true;
}

void IoSymbolTranceiver::onClose()
{
	mIsClosed = true;
	mNotify.notifyAll();
}

void IoSymbolTranceiver::EndWrite()
{
	mNotify.lock();

	mSendBuffer.clear();

	mNotify.unlock();

	mNotify.notify();
}

void IoSymbolTranceiver::ErrorWrite()
{
	mNotify.lock();

	mSendBuffer.clear();

	mNotify.unlock();

	mNotify.notify();
}

bool IoSymbolTranceiver::onReceive()
{
	enum {INIT_STATE, HEADER_RECEIVE_STATE, PAYLOAD_RECEIVE_STATE} 
	state = INIT_STATE;

	trace_enter();

	SymbolPacket packet;
	int payloadSize = 0;
	bool readEnd = false;
	while (!readEnd) 
	{
		trace("cnn.id=" << mpConnection->getId() << ", readState=" << state);

		if (mIsClosed || mpConnection->isOpen() == false) 
		{
			return false;
		}

		switch (state) 
		{
		case INIT_STATE:
			{
				trace("[onReceive] INIT_STATE");
				state = HEADER_RECEIVE_STATE;
				if(mReadPosition == mReadLength)
				{
					mReadPosition = mReadLength = 0;
				}
				mReceivedData.clear();
			}
			break;

			// 메시지 헤더를 읽음
		case HEADER_RECEIVE_STATE:
			{
				trace("[onReceive] HEADER_RECEIVE_STATE: readPos=" << mReadPosition << ";readLen=" << mReadLength);
				
				int length = mpConnection->read(&mReceiveBuffer.at(mReadPosition), mReceiveBuffer.size() - mReadPosition);
				if (length < 0)
				{
					state = INIT_STATE;
					mReadPosition = mReadLength = 0;
					log_error("Connection read error : cnn.id=" << mpConnection->getId());
					return false;
				}
				mReadLength += length;

				bool isEOL = false;
				while (mReadPosition < mReadLength)
				{
					char ch = (char) mReceiveBuffer[mReadPosition++];

					trace("[onReceive] STR_RECV_CHECK_EOL: readPos=" << mReadPosition << ";ch=" << ch);

					if (ch == '\r' || ch == '\n') 
					{
						if(ch == '\r' && mReceiveBuffer[mReadPosition] == '\n')
							mReadPosition++;
						isEOL = true;
						break;
					}
					else
					{
						mReceivedData.push_back(ch);
					}
				}

				if (isEOL)
				{
					packet.Parse(mReceivedData);
					state = PAYLOAD_RECEIVE_STATE;
					payloadSize = packet.GetPayload().size();
					mReceivedData.clear(); 
				}
				else
				{
					mReadPosition = 0;
					mReadLength = 0;
				}
			}
			break;

			//메시지 페이로드를 읽음
		case PAYLOAD_RECEIVE_STATE:
			{
				while (payloadSize > 0 && mReadPosition < mReadLength)
				{
					char ch = (char) mReceiveBuffer[mReadPosition++];
					mReceivedData.push_back(ch);
					payloadSize--;
				}

				if (payloadSize > 0)
				{
					mReadPosition = 0;
					mReadLength = 0;

					//앞으로 수신해야할 데이터가 있는 경우
					int length = mpConnection->read(&mReceiveBuffer.at(mReadPosition) , mReceiveBuffer.size() - mReadPosition);
					if (length < 0)
					{
						state = INIT_STATE;
						mReadPosition = mReadLength = 0;
						log_error("Connection read error : cnn.id=" << mpConnection->getId());
						return false;
					}
					mReadLength += length;
				}
				else
				{
					// 수신한 데이터가 남거나 더이상 없는 경우
					packet.SetPayload(mReceivedData);

					//수신한 패킷 처리
					ProcessReceive(mpConnection, packet);

					readEnd = true;
				}
			}
			break;
		} // end of switch
	} // end of while

	return true;
}

bool IoSymbolTranceiver::Send( const SymbolPacket& packet )
{
	return sendString(packet.ConvertToPakcet());
}

bool IoSymbolTranceiver::sendString( const std::string& sendStr )
{
	trace_enter();

	if (sendStr.size() == 0)
		return true;

	mNotify.lock();

	while (mSendBuffer.size() != 0)
	{
		mNotify.wait();
		if (mIsClosed || mpConnection->isOpen() == false) 
		{
			mNotify.unlock();
			return false;
		}
	}
	mWritePosition = 0;

	mNotify.unlock();

	mSendBuffer.resize(sendStr.size());
	memcpy(&mSendBuffer.at(0), sendStr.c_str(), mSendBuffer.size());

	int wc = mpConnection->write(&mSendBuffer.at(mWritePosition), mSendBuffer.size() - mWritePosition);
	if (wc < 0) 
	{
		ErrorWrite();
		log_error("Connection write error : cnn.id=" << mpConnection->getId());
		return false;
	}

	mWritePosition += wc;
	if (mWritePosition < (int)mSendBuffer.size())
	{
		trace("Write CONTINUE------------");
		mpConnection->addEvent(IoEventUtil::writeEvent());
		mpConnection->getEventProcessor()->registerEventTarget(mpConnection);
	}
	else
	{
		trace("Write DONE------------");
		EndWrite();
	}

	return true;
}

void IoSymbolTranceiver::ProcessReceive(IoConnection* pConnection, const SymbolPacket& packet)
{
	trace_enter();
	mpProtocol->OnReceived(pConnection, packet);
}

