#ifndef __EXTERNAL_API_PROTOCOL_IO_SYMBOL_TRANCEIVER_H__
#define __EXTERNAL_API_PROTOCOL_IO_SYMBOL_TRANCEIVER_H__

#include <string>
#include <array>
#include <vector>
#include <stdint.h>

#include "system/io/protocol/IoTranceiver.h"
#include "system/os/OSNotify.h"

class IoSymbolProtocol;
class IoConnection;
class SymbolPacket;

class IoSymbolTranceiver : public IoTranceiver
{
public:
	IoSymbolTranceiver(IoSymbolProtocol* pProtocol, IoConnection* pConnection);
	virtual ~IoSymbolTranceiver();

public:
	virtual bool Send(const SymbolPacket& packet);
	
public:
	virtual bool onSend();
	virtual bool onReceive();
	virtual void onClose();

protected:
	bool sendString(const std::string& sendStr); 
	void ProcessReceive(IoConnection* pConnection, const SymbolPacket& packet);
	void EndWrite();
	void ErrorWrite();

protected:
	IoSymbolProtocol* mpProtocol;
	IoConnection* mpConnection;

	bool mIsClosed;
	OSNotify mNotify;

	// for sending
	std::vector<uint8_t> mSendBuffer;
	int mWritePosition;

	// for receiving
	std::string mReceivedData;	
	std::array<uint8_t, 128> mReceiveBuffer;
	int mReadPosition, mReadLength;
};

#endif