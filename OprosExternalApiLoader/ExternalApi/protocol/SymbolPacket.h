#ifndef __EXTERNAL_API_PROTOCOL_SYMBOL_PACKET_H__
#define __EXTERNAL_API_PROTOCOL_SYMBOL_PACKET_H__

#include <string>

#include "system/util/Properties.h"

class SymbolPacket
{
public:
	SymbolPacket(const std::string& command = "", const std::string& payload_ = "");
	SymbolPacket(const SymbolPacket& packet);

public:
	SymbolPacket& operator=(const SymbolPacket& packet);

public:
	inline void SetVersion(const std::string& version)
	{
		properties.setProperty("ver", version);
	}

	inline const std::string& GetVersion(const std::string& default = "")
	{
		return properties.getProperty("ver", default);
	}

	inline void SetTarget(const std::string& target)
	{
		properties.setProperty("target", target);
	}

	inline const std::string& GetTarget(const std::string& default = "")
	{
		return properties.getProperty("target", default);
	}

	inline void SetCommand(const std::string& command)
	{
		properties.setProperty("cmd", command);
	}

	inline const std::string& GetCommand(const std::string& default = "")
	{
		return properties.getProperty("cmd", default);
	}

	inline void SetPayload(const std::string& payload)
	{
		mPayload = payload;
		properties.setProperty("payloadSize", properties.intToStr(payload.size()));
	}

	inline const std::string& GetPayload()
	{
		return mPayload;
	}

	std::string ConvertToPakcet() const;
	void Parse(const std::string input);

public:
	Properties properties;

private:
	std::string mPayload;
};

#endif