#ifndef __EXTERNAL_API_PROTOCOL_IO_SYMBOL_PROTOCOL_H__
#define __EXTERNAL_API_PROTOCOL_IO_SYMBOL_PROTOCOL_H__

#include <string>

#include "system/io/protocol/IoProtocol.h"

class SymbolPacket;

class IoSymbolProtocol : public IoProtocol
{
public:
	IoSymbolProtocol();
	virtual ~IoSymbolProtocol();

public:
	virtual IoTranceiver* createTransceiver(IoConnection* pConnection);
	virtual bool Send(const std::string& connectionId, const SymbolPacket& packet);
	virtual bool OnReceived(IoConnection* pConnection, const SymbolPacket& pakcet);
};

#endif