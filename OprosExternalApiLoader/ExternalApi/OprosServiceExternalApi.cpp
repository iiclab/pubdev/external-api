#include "OprosServiceExternalApi.h"

#include "OprosServiceCaller.h"

using namespace scenario;

OprosServiceExternalApi::OprosServiceExternalApi( const std::string& componentName, const std::string& servicePortName )
    : mComponentName(componentName), mServicePortName(servicePortName)
	, mServiceCaller(componentName, servicePortName)
{
}

OprosServiceExternalApi::~OprosServiceExternalApi(void)
{
}

bool OprosServiceExternalApi::Load( const FunctionType& functionType )
{
    auto itor = mFunctionTypeMap.find(functionType.name);
	if(itor != mFunctionTypeMap.end())
		return false;

    mFunctionTypeMap.insert(std::make_pair(functionType.name, functionType));

    return true;
}

bool OprosServiceExternalApi::Call( const std::string& functionName, scenario::Value& output, const std::vector<scenario::Value>& input )
{
    auto itor = mFunctionTypeMap.find(functionName);    
    if(itor == mFunctionTypeMap.end())
		return false;
	
	try
	{
		output = mServiceCaller.Call(itor->second, input);
	}
	catch(...)
	{
		return false;
	}

    return true;
}
