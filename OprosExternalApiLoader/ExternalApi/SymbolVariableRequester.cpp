#include "SymbolVariableRequester.h"

#include <sstream>

#include "system/Registry.h"
#include "system/io/IoManager.h"
#include "system/monitor/SymbolVariable.h"

#include "protocol/IoSymbolTranceiver.h"
#include "protocol/IoSymbolWorker.h"
#include "protocol/SymbolPacket.h"

int parseSymbolGetRequest(char *p, long psize, std::vector<SymbolVariable> &out);

SymbolVariableRequester::SymbolVariableRequester()
{	
	setWorkerManager(this);

	Registry* pRegistry = Registry::getRegistry();
	IoManager* pIoManager = static_cast<IoManager*>(pRegistry->getManager(IO_MGNAGER));
	pIoManager->addConnectionHandler(this);
}

SymbolVariableRequester::~SymbolVariableRequester()
{
	Registry* pRegistry = Registry::getRegistry();
	IoManager* pIoManager = static_cast<IoManager*>(pRegistry->getManager(IO_MGNAGER));
	pIoManager->removeConnectionHandler(getConnectionHandlerId());
}

IoWorker* SymbolVariableRequester::createWorker()
{
	return nullptr;
}

IoTranceiver* SymbolVariableRequester::createTransceiver(IoConnection* pConnection)
{
	return new IoSymbolTranceiver(this, pConnection);
}

bool SymbolVariableRequester::RequestSetVariable( const std::string& ip, int port, const std::string& componentName, const std::string& variableName, const std::string& input )
{
	std::string connectionId = findConnectionId(ip, port, componentName);
	if(connectionId == "")
		return false;

	std::string variableId = componentName;
	variableId.append(".").append(variableName);

	std::string payload;
	payload.append("{");
	payload.append("var=");
	payload.append(variableId);
	payload.append(";");
	payload.append("valueformat=str;");
	payload.append("value=\"");
	payload.append(input);
	payload.append("\"");
	payload.append(";}");

	SymbolPacket packet;
	packet.SetVersion("1.0");
	packet.SetTarget("symbol");
	packet.SetCommand("set");
	packet.SetPayload(payload);
		
	if (!Send(connectionId, packet))
		return false;

	auto pResult = GetReceiveData(connectionId);
	if(pResult == nullptr)
		return false;

	Properties& properties = pResult->properties;

	if(properties.getProperty("target") != "symbol"
		|| properties.getProperty("cmd") != "set"
		|| properties.getProperty("success") != "ok")
		return false;
	
	return true;
}

bool SymbolVariableRequester::RequestGetVariable( std::string& output, const std::string& ip, int port, const std::string& componentName, const std::string& variableName)
{
	std::string connectionId = findConnectionId(ip, port, componentName);
	if(connectionId == "")
		return false;

	std::string variableId = componentName;
	variableId.append(".").append(variableName);

	std::string payload;
	payload.append("{");
	payload.append("var=");
	payload.append(variableId);
	payload.append(";");
	payload.append("valueformat=str");
	payload.append(";}");

	SymbolPacket packet;
	packet.SetVersion("1.0");
	packet.SetTarget("symbol");
	packet.SetCommand("get");
	packet.SetPayload(payload);
		
	if (!Send(connectionId, packet))
		return false;

	auto pResult = GetReceiveData(connectionId);
	if(pResult == nullptr)
		return false;

	Properties& properties = pResult->properties;
	
	if(properties.getProperty("target") != "symbol"
		|| properties.getProperty("cmd") != "get"
		|| properties.getProperty("success") != "ok")
		return false;
	
	std::vector<SymbolVariable> symbolVariables;
	const std::string& receivedPayload = pResult->GetPayload();
	parseSymbolGetRequest(const_cast<char*>(receivedPayload.c_str())
		, receivedPayload.size(), symbolVariables);

	for(auto itor = symbolVariables.begin()
		; itor != symbolVariables.end(); ++itor)
	{
		if(itor->getVarName() == variableId)
		{
			output = itor->getValue();
			return true;
		}
	}

	return false;
}

std::string SymbolVariableRequester::findConnectionId( const std::string& ip, int port, const std::string& compName, const std::string& protocol )
{	
	std::string nodeId = ip;
	nodeId.append(":").append(StringUtil::intToStr(port));
	nodeId.append("/").append(compName);

	OSGuard guard(&m_mutex);

	auto nodeIdItor = mNodeIdMap.find(nodeId);
	if(nodeIdItor != mNodeIdMap.end())
		return nodeIdItor->second;
			
	IoInfo ioInfo;
	ioInfo.enabled = true;
	ioInfo.protocol = protocol == "" ? "tcp" : protocol;
	ioInfo.role = "client";
	ioInfo.props.setProperty("cnn.handler", getConnectionHandlerId());
	ioInfo.props.setProperty("evt.processor", "system");
	ioInfo.props.setProperty("ip.addr", ip);
	ioInfo.props.setProperty("ip.port", ioInfo.props.intToStr(port));

	IoManager* pIoManager = static_cast<IoManager *>(Registry::getRegistry()->getManager(IO_MGNAGER));	
	
	if (pIoManager->registerIo(ioInfo) == false)
		return "";
	
	if (pIoManager->startIo(ioInfo.id) == false)
	{
		pIoManager->removeEventTarget(ioInfo.id);
		return "";
	}

	mNodeIdMap[nodeId] = ioInfo.id;

	return ioInfo.id;
}

void SymbolVariableRequester::SetReceiveData(const std::string& id, std::shared_ptr<SymbolPacket>& pData)
{
	mReceiveValueNotify.lock();

	mReceiveDataMap[id] = pData;
	
	mReceiveValueNotify.unlock();
	mReceiveValueNotify.notifyAll();
}

std::shared_ptr<SymbolPacket> SymbolVariableRequester::GetReceiveData(const std::string& id, unsigned long timeout)
{
	OSGuard guard(&mReceiveValueNotify.getLock());

	for(; mReceiveDataMap.count(id) == 0 || mReceiveDataMap[id] == nullptr; )
	{
		if(!mReceiveValueNotify.wait(timeout))
			return nullptr;
	}
	 
	auto pPacket = mReceiveDataMap[id];
	mReceiveDataMap.erase(id);

	return pPacket;
}

bool SymbolVariableRequester::OnReceived( IoConnection* pConnect, const SymbolPacket& packet )
{
	if(pConnect == nullptr)
		return false;

	SetReceiveData(pConnect->getId(), std::shared_ptr<SymbolPacket>(new SymbolPacket(packet)));
	return true;
}