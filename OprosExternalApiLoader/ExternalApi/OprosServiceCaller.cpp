#include "OprosServiceCaller.h"

#include <sstream>

#include "ArchiveManager.h"
#include "archive/opros_archive.h"
#include "system/cm/ComponentManager.h"
#include "system/Registry.h"
#include "ArchiveFactory.h"
#include "ServicePortConnectorImpl.h"

using scenario::Value;

OprosServiceCaller::OprosServiceCaller(const std::string& componentName, const std::string& serviceName, ArchiveFactory* pArchiveFactory /*= nullptr*/ )
	: mComponentName(componentName), mServiceName(serviceName), mpArchiveFactory(pArchiveFactory)
{
	Registry* pRegistry = Registry::getRegistry();

	ComponentManager* pComponentManager = static_cast<ComponentManager*>(pRegistry->getManager(COMPONENT_MANAGER));
	
	if(mpArchiveFactory == nullptr)
	{
		ArchiveManager* pArchiveManger = static_cast<ArchiveManager*>(pRegistry->getManager(ARCHIVE_MANAGER));    
		mpArchiveFactory = pArchiveManger->getDefaultArchiveFactory();
	}

	auto pServicePortConnector = new ServicePortConnectorImpl(pComponentManager->getComponentRequester());
	pServicePortConnector->setTargetComponentName(mComponentName);
	pServicePortConnector->setTargetPortName(mServiceName);
	mpServicePortConnector = pServicePortConnector;
}

OprosServiceCaller::~OprosServiceCaller(void)
{
	delete mpServicePortConnector;
}

scenario::Value OprosServiceCaller::Call( const scenario::FunctionType& functionType, const std::vector<scenario::Value>& paramList )
{
	if (paramList.size() != functionType.parameters.size())
		return Value();

	std::stringstream inputStream;
	opros::archive::oarchive* pOarchive = mpArchiveFactory->getOutputArchive(&inputStream);

	for (size_t i = 0; i < paramList.size(); i++)
	{
		Value value = paramList[i];
		const scenario::FunctionType::Parameter& param = functionType.parameters[i];
		Serialize(*pOarchive, value.ConvertTo(param.type));
	}

	std::string invalue = inputStream.str();
	std::string outvalue;

	mpServicePortConnector->requestService(
		const_cast<std::string&>(functionType.name), invalue, outvalue);

	std::stringstream outputStream(outvalue);
	opros::archive::iarchive* pIarchive = mpArchiveFactory->getInputArchive(&outputStream);

	Value result = Deserialize(*pIarchive, functionType.returnType);

	mpArchiveFactory->release(pIarchive);
	mpArchiveFactory->release(pOarchive);

	return result;
}

void OprosServiceCaller::Serialize( opros::archive::oarchive& archive, const scenario::Value& value )
{
    switch(value.GetType())
    {
    case Value::Int32: 
        archive << OPROS_SERIALIZATION_NVP(value.AsInt32());
        break;       

    case Value::Uint32: 
        archive << OPROS_SERIALIZATION_NVP(value.AsUint32());
        break;

    case Value::Float32: 
        archive << OPROS_SERIALIZATION_NVP(value.AsFloat32());
        break;

    case Value::Float64:
        archive << OPROS_SERIALIZATION_NVP(value.AsFloat64());
        break;

    case Value::String:
        archive << OPROS_SERIALIZATION_NVP(value.AsString());
        break;

    case Value::Int8:
        {
            char temp = value.AsInt8();
            archive << OPROS_SERIALIZATION_NVP(temp);
        }
        break;

    case Value::Uint8:
        archive << OPROS_SERIALIZATION_NVP(value.AsUint8());
        break;

    case Value::Bool: 
        archive << OPROS_SERIALIZATION_NVP(value.AsBool());
        break;

    case Value::Int16:
        archive << OPROS_SERIALIZATION_NVP(value.AsInt16());
        break;

    case Value::Uint16: 
        archive << OPROS_SERIALIZATION_NVP(value.AsUint16());
        break;

    case Value::Int64: 
		{
			// 현재 OPRoS의 long에 대한 정의가 불분명하여
			// External API에서의 long은 64비트이지만 32비트로 처리함
			long temp = static_cast<long>(value.AsUint64());
			archive << OPROS_SERIALIZATION_NVP(temp);
		}
		break;

    case Value::Uint64:
		{
			// 현재 OPRoS의 long에 대한 정의가 불분명하여
			// External API에서의 long은 64비트이지만 32비트로 처리함
			unsigned long temp = static_cast<unsigned long>(value.AsUint64());
			archive << OPROS_SERIALIZATION_NVP(temp);
		}
		break;

	case Value::Struct:
		{
			auto& structure = value.AsStructure();
			std::string typeName = GetTypeName(value);
		
			archive.write_class_head(typeName);

			for (auto itor = structure.begin(), end = structure.end()
				; itor != end; ++itor)
				Serialize(archive, itor->second);

			archive.write_class_end(typeName);
		}
		break;

	case Value::Array:
		{
			// std::vector 에 대해서만 배열로 인정함
			auto& array = value.AsArray();
			int arraySize = static_cast<int>(array.size());
			std::string typeName = GetTypeName(value);

			archive.write_class_head(typeName);

			archive << OPROS_SERIALIZATION_NVP(arraySize);

			for (auto itor = array.begin(), end = array.end()
				; itor != end; ++itor)
				Serialize(archive, *itor);				

			archive.write_class_end(typeName);
		}
		break;

	case Value::Void:
		break;

    default:
        assert(!"Not supported type");
    }
}

Value OprosServiceCaller::Deserialize( opros::archive::iarchive& archive, const Value& type)
{
    Value result(type);

    switch(type.GetType())
    {
    case Value::Int32: 
        archive >> OPROS_SERIALIZATION_NVP(result.AsInt32());
        break;       

    case Value::Uint32: 
        archive >> OPROS_SERIALIZATION_NVP(result.AsUint32());
        break;

    case Value::Float32: 
        archive >> OPROS_SERIALIZATION_NVP(result.AsFloat32());
        break;

    case Value::Float64:
        archive >> OPROS_SERIALIZATION_NVP(result.AsFloat64());
        break;

    case Value::String:
        archive >> OPROS_SERIALIZATION_NVP(result.AsString());
        break;

    case Value::Int8:
        {
            char temp;
            archive >> OPROS_SERIALIZATION_NVP(temp);
            result.AsInt8() = temp;
        }
        break;
        
    case Value::Uint8:
        archive >> OPROS_SERIALIZATION_NVP(result.AsUint8());
        break;

    case Value::Bool: 
        archive >> OPROS_SERIALIZATION_NVP(result.AsBool());
        break;

    case Value::Int16:
        archive >> OPROS_SERIALIZATION_NVP(result.AsInt16());
        break;

    case Value::Uint16: 
        archive >> OPROS_SERIALIZATION_NVP(result.AsUint16());
        break;

    case Value::Int64:
		{
			// 현재 OPRoS의 long에 대한 정의가 불분명하여
			// External API에서의 long은 64비트이지만 32비트로 처리함
			long temp;
			archive >> OPROS_SERIALIZATION_NVP(temp);
			result.AsInt64() = temp;
		}
		break;

    case Value::Uint64:
		{
			// 현재 OPRoS의 long에 대한 정의가 불분명하여
			// External API에서의 long은 64비트이지만 32비트로 처리함
			unsigned long temp;
			archive >> OPROS_SERIALIZATION_NVP(temp);
			result.AsUint64() = temp;
		}
		break;
        
	case Value::Struct:
		{
			auto& structure = result.AsStructure();
			std::string typeName = GetTypeName(result);

			archive.read_class_head(typeName);

			for (auto itor = structure.begin(), end = structure.end()
				; itor != end; ++itor)
				itor->second = Deserialize(archive, itor->second);

			archive.read_class_end(typeName);
		}
		break;

	case Value::Array:
		{
			// std::vector 에 대해서만 배열로 인정함
			auto& array = result.AsArray();
			int arraySize = 0;
			std::string typeName = GetTypeName(result);

			archive.read_class_head(typeName);

			archive >> OPROS_SERIALIZATION_NVP(arraySize);
			
			array.resize(arraySize);
			array.Normalize();

			for (auto itor = array.begin(), end = array.end()
				; itor != end; ++itor)
				*itor = Deserialize(archive, *itor);				

			archive.read_class_end(typeName);
		}
		break;
		
	case Value::Void:
		break;

    default:
        assert(!"Not supported type");
    }

    return result;
}

std::string OprosServiceCaller::GetTypeName( const scenario::Value& value )
{
	std::string result;

	switch (value.GetType())
	{
	case scenario::Value::Int8:
		{
			//std::remove_const<
			//	std::remove_reference<decltype(value.AsInt8())>::type>
			//	::type* p = nullptr;
			char* p = nullptr;
			result = typeName(p);
		}
		break;
	case scenario::Value::Uint8:
		{
			std::remove_const<
				std::remove_reference<
				decltype(value.AsUint8())>::type>::type* p = nullptr;
			result = typeName(p);
		}
		break;
	case scenario::Value::Int16:
		{
			std::remove_const<
				std::remove_reference<
				decltype(value.AsInt16())>::type>::type* p = nullptr;
			result = typeName(p);
		}
		break;
	case scenario::Value::Uint16:
		{
			std::remove_const<
				std::remove_reference<
				decltype(value.AsUint16())>::type>::type* p = nullptr;
			result = typeName(p);
		}
		break;
	case scenario::Value::Int32:
		{
			std::remove_const<
				std::remove_reference<
				decltype(value.AsInt32())>::type>::type* p = nullptr;
			result = typeName(p);
		}
		break;
	case scenario::Value::Uint32:
		{
			std::remove_const<
				std::remove_reference<
				decltype(value.AsUint32())>::type>::type* p = nullptr;
			result = typeName(p);
		}
		break;
	case scenario::Value::Int64:
		{
			// 현재 OPRoS의 long에 대한 정의가 불분명하여
			// External API에서의 long은 64비트이지만 32비트로 처리함
			//std::remove_const<
			//	std::remove_reference<
			//	decltype(value.AsInt64())>::type>::type* p = nullptr;
			long* p = nullptr;
			result = typeName(p);
		}
		break;
	case scenario::Value::Uint64:
		{
			// 현재 OPRoS의 long에 대한 정의가 불분명하여
			// External API에서의 long은 64비트이지만 32비트로 처리함
			//std::remove_const<
			//	std::remove_reference<
			//	decltype(value.AsUint64())>::type>::type* p = nullptr;
			unsigned long* p = nullptr;
			result = typeName(p);
		}
		break;
	case scenario::Value::Float32:
		{
			std::remove_const<
				std::remove_reference<
				decltype(value.AsFloat32())>::type>::type* p = nullptr;
			result = typeName(p);
		}
		break;
	case scenario::Value::Float64:
		{
			std::remove_const<
				std::remove_reference<
				decltype(value.AsFloat64())>::type>::type* p = nullptr;
			result = typeName(p);
		}
		break;
	case scenario::Value::Bool:
		{
			std::remove_const<
				std::remove_reference<
				decltype(value.AsBool())>::type>::type* p = nullptr;
			result = typeName(p);
		}
		break;
	case scenario::Value::Array:
		{
			result = "std::vector<";
			result.append(GetTypeName(value.AsArray().GetDefaultType()));
			result.append(">");
		}
		break;
	case scenario::Value::String:
		{
			std::remove_const<
				std::remove_reference<
				decltype(value.AsString())>::type>::type* p = nullptr;
			result = typeName(p);
		}
		break;
	case scenario::Value::Struct:
		result = value.AsStructure().GetName();
		break;
	
	default:
		break;
	}

	return result;
}
