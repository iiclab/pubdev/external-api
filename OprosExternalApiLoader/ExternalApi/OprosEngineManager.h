#ifndef __EXTERNAL_API_OPROS_ENGINE_MANAGER_H__
#define __EXTERNAL_API_OPROS_ENGINE_MANAGER_H__

#include <string>

namespace opros
{
	namespace engine
	{
		class Config;
	}
}

class IoManager;
class ComponentManager;
class ArchiveManager;
class SymbolVariableRequester;

class OprosEngineManager
{
public:
	typedef std::pair<std::string, std::string> Property;

private:
	OprosEngineManager();
	
	//for noncopyable
	OprosEngineManager(const OprosEngineManager&);
	const OprosEngineManager& operator=(const OprosEngineManager&);

public:
	~OprosEngineManager();

public:
	static inline OprosEngineManager& GetInstance()
	{
		static OprosEngineManager engineManger;
		return engineManger;
	}

public:
	bool Enable();
	bool Disable();

	bool AddProperty(const Property& property, const std::string& parent = "");
	bool AddRemoteComponent(const std::string& component, const std::string& ip, int port);

	inline bool IsEnabled() { return mIsEnabled; }
	inline SymbolVariableRequester& GetSymbolVariableReqeuster()
	{
		return *mpSymbolVariableRequester;
	}

private:
	opros::engine::Config* CreateConfig();

private:	
	ArchiveManager* mpArchiveManaver;
	IoManager* mpIoMnager;
	ComponentManager* mpComponentManager;
	SymbolVariableRequester* mpSymbolVariableRequester;
	opros::engine::Config* mpConfig;

	bool mIsEnabled;
};

#endif