#include <boost/asio.hpp>

#include <ros/ros.h>
#include <std_msgs/String.h>

#include "TestNode/Person.h"
#include "TestNode/Hello.h"
#include "TestNode/PrintPerson.h"
#include "TestNode/GetIpAddress.h"
#include "TestNode/Execute.h"

bool GetLocalIp(std::string& output, const std::string& masterIp, unsigned short masterPort);

void Echo(const std_msgs::String::ConstPtr& message);

bool PrintPerson(TestNode::PrintPerson::Request& request, TestNode::PrintPerson::Response& response);
bool Hello(TestNode::Hello::Request& request, TestNode::Hello::Response& response);
bool Execute(TestNode::Execute::Request& request, TestNode::Execute::Response& response);
bool GetIpAddress(TestNode::GetIpAddress::Request& request, TestNode::GetIpAddress::Response& response);

const std::string masterIp = "203.252.90.114";
const unsigned short masterPort = 11311;

int main()
{
	ros::M_string rosParameter;
	{		
		std::stringstream stream;
		stream << "http://" << masterIp << ":" << masterPort;

		rosParameter["__master"] = stream.str();
		if (!GetLocalIp(rosParameter["__ip"], masterIp, masterPort))
			return 0;
	}
	
	ros::init(rosParameter, "TestNode");
	ros::NodeHandle nodeHandle;

	auto echoSubscriber = nodeHandle.subscribe("TestNode/Echo", 100, &Echo);
	auto printPersonService = nodeHandle.advertiseService("TestNode/PrintPerson", &PrintPerson);
	auto helloService = nodeHandle.advertiseService("TestNode/Hello", &Hello);
	auto executeService = nodeHandle.advertiseService("TestNode/Execute", &Execute);
	auto getIpAddressService = nodeHandle.advertiseService("TestNode/GetIpAddress", &GetIpAddress);
	
	ros::spin();	
	return 0;
}

bool PrintPerson(TestNode::PrintPerson::Request& request, TestNode::PrintPerson::Response& response)
{
	std::cout << "[" << GetTickCount() << "]";
	std::cout << request.person << std::endl;
	response.result = request.person;
	return true;
}

bool Hello(TestNode::Hello::Request& request, TestNode::Hello::Response& response)
{
	std::cout << "[" << GetTickCount() << "]";
	for (auto itor = request.messages.begin(), end = request.messages.end()
		; itor != end; ++itor)
		std::cout << *itor << std::endl;
	response.echoes = request.messages;
	return true; 
}

void Echo(const std_msgs::String::ConstPtr& message)
{	
	std::cout << "[" << GetTickCount() << "]";
	std::cout << message->data << std::endl;
}

bool Execute(TestNode::Execute::Request& request, TestNode::Execute::Response& response)
{
	return WinExec(request.program.c_str(), SW_SHOWNORMAL) > 31;
}

bool GetIpAddress(TestNode::GetIpAddress::Request& request, TestNode::GetIpAddress::Response& response)
{
	return GetLocalIp(response.ip, masterIp, masterPort);
}

bool GetLocalIp(std::string& output, const std::string& masterIp, unsigned short masterPort)
{
	using namespace boost::asio::ip;
	typedef tcp Protocol;

	boost::system::error_code errorCdoe;
	boost::asio::io_service ioService;

	Protocol::resolver resolver(ioService);
	Protocol::resolver::query query(masterIp, "");
	auto endpointItor = resolver.resolve(query, errorCdoe);

	if (errorCdoe)
		return false;

	if (endpointItor == Protocol::resolver_iterator())
		return false;

	Protocol::socket socket(ioService);			
	socket.connect(Protocol::endpoint(endpointItor->endpoint().address(), masterPort), errorCdoe);

	if (errorCdoe)
		return false;

	auto localEnpoint = socket.local_endpoint(errorCdoe);

	if (errorCdoe)
		return false;

	output = localEnpoint.address().to_string();

	socket.close();

	return true;
}
