/* Auto-generated by genmsg_cpp for file /home/east/ros_workspace/TestNode/msg/Person.msg */
#ifndef TESTNODE_MESSAGE_PERSON_H
#define TESTNODE_MESSAGE_PERSON_H
#include <string>
#include <vector>
#include <map>
#include <ostream>
#include "ros/serialization.h"
#include "ros/builtin_message_traits.h"
#include "ros/message_operations.h"
#include "ros/time.h"

#include "ros/macros.h"

#include "ros/assert.h"


namespace TestNode
{
template <class ContainerAllocator>
struct Person_ {
  typedef Person_<ContainerAllocator> Type;

  Person_()
  : name()
  , age(0)
  , grade(0.0)
  {
  }

  Person_(const ContainerAllocator& _alloc)
  : name(_alloc)
  , age(0)
  , grade(0.0)
  {
  }

  typedef std::basic_string<char, std::char_traits<char>, typename ContainerAllocator::template rebind<char>::other >  _name_type;
  std::basic_string<char, std::char_traits<char>, typename ContainerAllocator::template rebind<char>::other >  name;

  typedef int32_t _age_type;
  int32_t age;

  typedef float _grade_type;
  float grade;


  typedef boost::shared_ptr< ::TestNode::Person_<ContainerAllocator> > Ptr;
  typedef boost::shared_ptr< ::TestNode::Person_<ContainerAllocator>  const> ConstPtr;
  boost::shared_ptr<std::map<std::string, std::string> > __connection_header;
}; // struct Person
typedef  ::TestNode::Person_<std::allocator<void> > Person;

typedef boost::shared_ptr< ::TestNode::Person> PersonPtr;
typedef boost::shared_ptr< ::TestNode::Person const> PersonConstPtr;


template<typename ContainerAllocator>
std::ostream& operator<<(std::ostream& s, const  ::TestNode::Person_<ContainerAllocator> & v)
{
  ros::message_operations::Printer< ::TestNode::Person_<ContainerAllocator> >::stream(s, "", v);
  return s;}

} // namespace TestNode

namespace ros
{
namespace message_traits
{
template<class ContainerAllocator> struct IsMessage< ::TestNode::Person_<ContainerAllocator> > : public TrueType {};
template<class ContainerAllocator> struct IsMessage< ::TestNode::Person_<ContainerAllocator>  const> : public TrueType {};
template<class ContainerAllocator>
struct MD5Sum< ::TestNode::Person_<ContainerAllocator> > {
  static const char* value() 
  {
    return "2a55331c0ec5695e0d8ea29647815bfa";
  }

  static const char* value(const  ::TestNode::Person_<ContainerAllocator> &) { return value(); } 
  static const uint64_t static_value1 = 0x2a55331c0ec5695eULL;
  static const uint64_t static_value2 = 0x0d8ea29647815bfaULL;
};

template<class ContainerAllocator>
struct DataType< ::TestNode::Person_<ContainerAllocator> > {
  static const char* value() 
  {
    return "TestNode/Person";
  }

  static const char* value(const  ::TestNode::Person_<ContainerAllocator> &) { return value(); } 
};

template<class ContainerAllocator>
struct Definition< ::TestNode::Person_<ContainerAllocator> > {
  static const char* value() 
  {
    return "string name\n\
int32 age\n\
float32 grade\n\
\n\
";
  }

  static const char* value(const  ::TestNode::Person_<ContainerAllocator> &) { return value(); } 
};

} // namespace message_traits
} // namespace ros

namespace ros
{
namespace serialization
{

template<class ContainerAllocator> struct Serializer< ::TestNode::Person_<ContainerAllocator> >
{
  template<typename Stream, typename T> inline static void allInOne(Stream& stream, T m)
  {
    stream.next(m.name);
    stream.next(m.age);
    stream.next(m.grade);
  }

  ROS_DECLARE_ALLINONE_SERIALIZER;
}; // struct Person_
} // namespace serialization
} // namespace ros

namespace ros
{
namespace message_operations
{

template<class ContainerAllocator>
struct Printer< ::TestNode::Person_<ContainerAllocator> >
{
  template<typename Stream> static void stream(Stream& s, const std::string& indent, const  ::TestNode::Person_<ContainerAllocator> & v) 
  {
    s << indent << "name: ";
    Printer<std::basic_string<char, std::char_traits<char>, typename ContainerAllocator::template rebind<char>::other > >::stream(s, indent + "  ", v.name);
    s << indent << "age: ";
    Printer<int32_t>::stream(s, indent + "  ", v.age);
    s << indent << "grade: ";
    Printer<float>::stream(s, indent + "  ", v.grade);
  }
};


} // namespace message_operations
} // namespace ros

#endif // TESTNODE_MESSAGE_PERSON_H

